package com.smartech.sistemacooperativa.bll.facade;

import com.smartech.sistemacooperativa.dal.DAOAporte;
import com.smartech.sistemacooperativa.dal.DAOAprobacion;
import com.smartech.sistemacooperativa.dal.DAOComprobante;
import com.smartech.sistemacooperativa.dal.DAOCuenta;
import com.smartech.sistemacooperativa.dal.DAOCuota;
import com.smartech.sistemacooperativa.dal.DAODetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dal.DAODetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dal.DAODetalleSolicitudPrestamoSocio;
import com.smartech.sistemacooperativa.dal.DAODetalleSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dal.DAOEmpleado;
import com.smartech.sistemacooperativa.dal.DAOFamiliar;
import com.smartech.sistemacooperativa.dal.DAOHabilitacionCuotas;
import com.smartech.sistemacooperativa.dal.DAOHistoricoCaja;
import com.smartech.sistemacooperativa.dal.DAOHistoricoCuenta;
import com.smartech.sistemacooperativa.dal.DAOHuella;
import com.smartech.sistemacooperativa.dal.DAOMoneda;
import com.smartech.sistemacooperativa.dal.DAOMovimiento;
import com.smartech.sistemacooperativa.dal.DAOMovimientoAdministrativo;
import com.smartech.sistemacooperativa.dal.DAOPago;
import com.smartech.sistemacooperativa.dal.DAOPrestamo;
import com.smartech.sistemacooperativa.dal.DAOSocio;
import com.smartech.sistemacooperativa.dal.DAOSolicitudIngreso;
import com.smartech.sistemacooperativa.dal.DAOSolicitudPrestamo;
import com.smartech.sistemacooperativa.dal.DAOSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dal.DAOTarjeta;
import com.smartech.sistemacooperativa.dal.DAOTasaInteres;
import com.smartech.sistemacooperativa.dal.DAOTasaInteresSolicitudPrestamo;
import com.smartech.sistemacooperativa.dal.DAOTipoInteres;
import com.smartech.sistemacooperativa.dal.DAOUsuario;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Aprobacion;
import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.DetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dominio.DetalleSolicitudPrestamoSocio;
import com.smartech.sistemacooperativa.dominio.DetalleSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.HistoricoCaja;
import com.smartech.sistemacooperativa.dominio.HistoricoCuenta;
import com.smartech.sistemacooperativa.dominio.Huella;
import com.smartech.sistemacooperativa.dominio.Moneda;
import com.smartech.sistemacooperativa.dominio.Movimiento;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.SolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TasaInteresSolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class InsertFacade {

    private static InsertFacade instance = null;

    private InsertFacade() {
    }

    public static InsertFacade getInstance() {
        if (instance == null) {
            instance = new InsertFacade();
        }

        return instance;
    }

    public boolean insertUsuario(Usuario objUsuario) {
        boolean result = DAOUsuario.getInstance().insert(objUsuario);

        if (result) {
            for (Huella objHuella : objUsuario.getLstHuellas()) {
                result = DAOHuella.getInstance().insert(objHuella, objUsuario);
                if (!result) {
                    break;
                }
            }
        }

        return result;
    }

    public boolean insertSocio(Socio objSocio) {
        return DAOSocio.getInstance().insert(objSocio);
    }

    public boolean insertFamiliaresSocio(List<DetalleFamiliarSocio> lstFamiliaresSocio) {
        boolean result = lstFamiliaresSocio.isEmpty();

        for (DetalleFamiliarSocio objDetalleFamiliarSocio : lstFamiliaresSocio) {
            result = objDetalleFamiliarSocio.getObjFamiliar().getId() != 0 ? true : DAOFamiliar.getInstance().insert(objDetalleFamiliarSocio.getObjFamiliar());
            if (result) {
                DAODetalleFamiliarSocio.getInstance().insert(objDetalleFamiliarSocio);
            }
        }

        return result;
    }

    public boolean insertSolicitudIngreso(SolicitudIngreso objSolicitudIngreso) {
        boolean result = false;

        try {
            result = this.insertSocio(objSolicitudIngreso.getObjSocio());

            if (result) {
                result = this.insertFamiliaresSocio(objSolicitudIngreso.getObjSocio().getLstFamiliaresSocio());

                if (result) {
                    result = DAOSolicitudIngreso.getInstance().insert(objSolicitudIngreso);
                    if (result) {
                        result = this.insertDetalleDocumentoSolicitud(objSolicitudIngreso.getLstDetalleDocumentoSolicitud());
                        if (result) {
                            result = this.insertAprobaciones(objSolicitudIngreso.getLstAprobaciones());
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertDetalleDocumentoSolicitud(List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud) {
        boolean result = true;

        for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : lstDetalleDocumentoSolicitud) {
            result = DAODetalleDocumentoSolicitud.getInstance().insert(objDetalleDocumentoSolicitud);
            if (!result) {
                break;
            }
        }

        return result;
    }

    public boolean insertAprobaciones(List<Aprobacion> lstAprobaciones) {
        boolean result = true;

        for (Aprobacion objAprobacion : lstAprobaciones) {
            result = DAOAprobacion.getInstance().insert(objAprobacion);
            if (!result) {
                break;
            }
        }

        return result;
    }

    public boolean insertCuentaSocios(Cuenta objCuenta) {
        boolean result = false;

        try {
            result = DAOCuenta.getInstance().insert(objCuenta);

            System.out.println("resultado nueva cuenta: " + result);
            if (result) {
                for (Socio objSocio : objCuenta.getLstSocios()) {
                    result = result && DAOCuenta.getInstance().insertDetalleCuentaSocio(objCuenta, objSocio);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertTarjeta(Tarjeta objTarjeta) {
        boolean result = false;
        try {
            Tarjeta objTarjetaBuscar = QueryFacade.getInstance().getTarjeta(objTarjeta.getObjSocio());
            if (objTarjetaBuscar == null) {
                result = DAOTarjeta.getInstance().insert(objTarjeta);
            } else {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertAporte(Aporte objAporte) {
        return DAOAporte.getInstance().insert(objAporte);
    }

    public boolean insertPago(Pago objPago) {
        return DAOPago.getInstance().insert(objPago);
    }

    public boolean insertDetallePagoAporte(Aporte objAporte, Pago objPago) {
        return DAOAporte.getInstance().insertDetallePagoAporte(objPago, objAporte);
    }

    public boolean insertPrestamo(Prestamo objPrestamo) {
        return DAOPrestamo.getInstance().insert(objPrestamo);
    }

    public boolean insertCuotas(List<Cuota> lstCuotas) {
        boolean result = false;

        try {
            for (Cuota objCuota : lstCuotas) {
                result = DAOCuota.getInstance().insert(objCuota);
                if (!result) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertMovimiento(Movimiento objMovimiento) {
        return DAOMovimiento.getInstance().insert(objMovimiento);
    }

    public boolean insertHistoricoCaja(HistoricoCaja objHistoricoCaja) {
        return DAOHistoricoCaja.getInstance().insert(objHistoricoCaja);
    }

    public boolean insertSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo) {
        boolean result = DAOSolicitudPrestamo.getInstance().insert(objSolicitudPrestamo);

        if (result) {
            result = this.insertDetalleSolicitudPrestamoSocio(objSolicitudPrestamo.getLstDetalleSolicitudPrestamoSocio());
            if (result) {
                result = this.insertDetalleDocumentoSolicitud(objSolicitudPrestamo.getLstDetalleDocumentoSolicitud());
                if (result) {
                    result = this.insertAprobaciones(objSolicitudPrestamo.getLstAprobaciones());
                }
            }
        }

        return result;
    }

    public boolean insertTasaInteresSolicitudPrestamo(TasaInteresSolicitudPrestamo objTasaInteresSolicitudPrestamo) {
        return DAOTasaInteresSolicitudPrestamo.getInstance().insert(objTasaInteresSolicitudPrestamo);
    }

    public boolean insertDetalleSolicitudPrestamoSocio(List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio) {
        boolean result = true;

        for (DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio : lstDetalleSolicitudPrestamoSocio) {
            result = DAODetalleSolicitudPrestamoSocio.getInstance().insert(objDetalleSolicitudPrestamoSocio);
            if (!result) {
                break;
            }
        }

        return result;
    }

    public boolean insertMoneda(Moneda objMoneda) {
        boolean result = false;

        try {
            result = DAOMoneda.getInstance().insert(objMoneda);
            if (result) {
                result = DAOMoneda.getInstance().insertTipoCambio(objMoneda);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertComprobante(Comprobante objComprobante) {
        boolean result = false;

        try {
            result = DAOComprobante.getInstance().insert(objComprobante);
            if (result) {
                result = DAOComprobante.getInstance().insertDetalleComprobantePago(objComprobante);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertHistoricoCuenta(HistoricoCuenta objHistoricoCuenta) {
        return DAOHistoricoCuenta.getInstance().insertHistoricoCuenta(objHistoricoCuenta);
    }

    public boolean insertSolicitudRetiroSocio(SolicitudRetiroSocio objSolicitudRetiroSocio) {
        boolean result = false;

        try {
            result = DAOSolicitudRetiroSocio.getInstance().insert(objSolicitudRetiroSocio);
            for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud()) {
                result = DAODetalleDocumentoSolicitud.getInstance().insert(objDetalleDocumentoSolicitud);
                if (!result) {
                    break;
                }
            }
            if (result) {
                for (Aprobacion objAprobacion : objSolicitudRetiroSocio.getLstAprobaciones()) {
                    result = DAOAprobacion.getInstance().insert(objAprobacion);
                    if (!result) {
                        break;
                    }
                }
                if (result) {
                    for (DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio : objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios()) {
                        result = DAODetalleSolicitudRetiroSocio.getInstance().insert(objSolicitudRetiroSocio, objDetalleSolicitudRetiroSocio);
                        if (!result) {
                            break;
                        }
                    }
                }
//                if (result) {
//                    if (objSolicitudRetiroSocio.isAprobado()) {
//                        objSolicitudRetiroSocio.getObjSocio().setActivo(false);
//                        result = UpdateFacade.getInstance().updateSocio(objSolicitudRetiroSocio.getObjSocio());
//                    }
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertDetalleHabilitacionCuotaPago(HabilitacionCuotas objHabilitacionCuotas, Pago objPago) {
        return DAOHabilitacionCuotas.getInstance().insertDetalleHabilitacionCuotaPago(objPago, objHabilitacionCuotas);
    }

    public boolean insertEmpleado(Empleado objEmpleado) {
        boolean result = false;

        try {
            result = this.insertUsuario(objEmpleado.getObjUsuario());
            if (result) {
                result = DAOEmpleado.getInstance().insert(objEmpleado);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertTasasInteres(List<TasaInteres> lstTasasInteres) {
        boolean result = false;

        try {
            if (lstTasasInteres.isEmpty()) {
                return true;
            }
            for (TasaInteres objTasaInteres : lstTasasInteres) {
                result = DAOTasaInteres.getInstance().insert(objTasaInteres);
                if (!result) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertTipoInteres(TipoInteres objTipoInteres) {
        boolean result = false;

        try {
            result = DAOTipoInteres.getInstance().insert(objTipoInteres);
            if (result) {
                this.insertTasasInteres(objTipoInteres.getLstTasasInteres());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertHabilitacionCuotas(HabilitacionCuotas objHabilitacionCuotas) {
        boolean result = false;

        try {
            result = DAOHabilitacionCuotas.getInstance().insert(objHabilitacionCuotas);
            if (result) {
                result = UpdateFacade.getInstance().updateCuotas(objHabilitacionCuotas.getLstCuotas());
                if (result) {
                    result = DAOHabilitacionCuotas.getInstance().insertDetalleHabilitacionCuota(objHabilitacionCuotas);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean insertMovimientoAdministrativo(MovimientoAdministrativo objMovimientoAdministrativo) {
        return DAOMovimientoAdministrativo.getInstance().insert(objMovimientoAdministrativo);
    }

    public boolean insertDetallePagoMovimientoAdministrativo(Pago objPago, MovimientoAdministrativo objMovimientoAdministrativo) {
        return DAOMovimientoAdministrativo.getInstance().insertDetallePagoMovimientoAdministrativo(objPago, objMovimientoAdministrativo);
    }

    public boolean insertDetallePagoSolicitudPrestamo(Pago objPago, SolicitudPrestamo objSolicitudPrestamo) {
        return DAOSolicitudPrestamo.getInstance().insertDetallePagoSolicitudPrestamo(objPago, objSolicitudPrestamo);
    }
}
