package com.smartech.sistemacooperativa.bll.facade;

import com.smartech.sistemacooperativa.bll.process.PrestamosProcessLogic;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.DetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.HistoricoCuenta;
import com.smartech.sistemacooperativa.dominio.Movimiento;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.ITransaccion;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import com.smartech.sistemacooperativa.dominio.TransferenciaCuenta;
import com.smartech.sistemacooperativa.util.generics.BigDecimalUtil;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.FileUtil;
import com.smartech.sistemacooperativa.util.reporting.ReportUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.pentaho.reporting.engine.classic.core.parameters.ParameterDefinitionEntry;

/**
 *
 * @author Smartech
 */
public class ReportFacade {

    private static ReportFacade instance = null;

    private ReportFacade() {

    }

    public static ReportFacade getInstance() {
        if (instance == null) {
            instance = new ReportFacade();
        }

        return instance;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getFichaSolicitudPrestamo(Prestamo objPrestamo) {
        String[] headersGarantes = {"nombre", "tipodoc", "dni"};
        String[] headersCuotas = {"vencimiento", "capital", "amortizacion", "interes", "cuota"};

        SolicitudPrestamo objSolicitudPrestamo = objPrestamo.getObjSolicitudPrestamo();
        List<Socio> lstSociosGarantes = objSolicitudPrestamo.getLstSociosGarantes();
        DefaultTableModel defaultTableModelGarantes = new DefaultTableModel(null, headersGarantes);
        if (!lstSociosGarantes.isEmpty()) {
            for (Socio objSocio : lstSociosGarantes) {
                defaultTableModelGarantes.addRow(new Object[]{
                    objSocio.getNombreCompleto().toUpperCase(),
                    objSocio.getObjTipoDocumentoIdentidad().getNombre(),
                    objSocio.getDocumentoIdentidad()
                });
            }
        } else {
            defaultTableModelGarantes.addRow(new Object[]{
                "NO TIENE GARANTES",
                "---",
                "---"
            });
        }

        List<Cuota> lstCuotas = objPrestamo.getLstCuotas();
        DefaultTableModel defaultTableModelCuotas = new DefaultTableModel(null, headersCuotas);
        for (Cuota objCuota : lstCuotas) {
            defaultTableModelCuotas.addRow(new Object[]{
                DateUtil.getRegularDate(DateUtil.getDate(objCuota.getFechaVencimiento())),
                objCuota.getDeudaPendiente().add(objCuota.getAmortizacion()),
                objCuota.getAmortizacion(),
                objCuota.getInteres(),
                objCuota.getMonto()
            });
        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("tblCuotas", defaultTableModelCuotas);
        data.put("tblGarantes", defaultTableModelGarantes);

        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        parameters.add(ReportUtil.newParameter("codigosolicitud", objSolicitudPrestamo.getCodigo()));
        parameters.add(ReportUtil.newParameter("nombresocio", objSolicitudPrestamo.getObjSocioPrestatario().getNombreCompleto()));
        parameters.add(ReportUtil.newParameter("codigosocio", objSolicitudPrestamo.getObjSocioPrestatario().getCodigo()));
        parameters.add(ReportUtil.newParameter("tipodocsocio", objSolicitudPrestamo.getObjSocioPrestatario().getObjTipoDocumentoIdentidad().getNombre()));
        parameters.add(ReportUtil.newParameter("dnisocio", objSolicitudPrestamo.getObjSocioPrestatario().getDocumentoIdentidad()));
        parameters.add(ReportUtil.newParameter("direccionsocio", objSolicitudPrestamo.getObjSocioPrestatario().getDireccion()));
        parameters.add(ReportUtil.newParameter("tipoInteres", objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().getNombre()));
        parameters.add(ReportUtil.newParameter("tasaInteres", objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().isPlazoFijo() ? ("Plazo Fijo - S/. " + BigDecimalUtil.round(objSolicitudPrestamo.getObjTasaInteres().getMontoPlazoFijo()).toPlainString()) : (BigDecimalUtil.round(objSolicitudPrestamo.getObjTasaInteres().getTea()).toPlainString() + " %")));
        parameters.add(ReportUtil.newParameter("fechaDesembolso", "-"));
        parameters.add(ReportUtil.newParameter("cuotas", String.valueOf(objSolicitudPrestamo.getCuotas())));
        parameters.add(ReportUtil.newParameter("montoAprobado", BigDecimalUtil.round(objSolicitudPrestamo.getMontoAprobado()).toPlainString()));
        parameters.add(ReportUtil.newParameter("motivoSolicitud", objSolicitudPrestamo.getMotivo()));
        parameters.add(ReportUtil.newParameter("motivoDesaprobacion", objSolicitudPrestamo.getMotivoDesaprobacion().isEmpty() ? " - " : objSolicitudPrestamo.getMotivoDesaprobacion()));
        parameters.add(ReportUtil.newParameter("otrosIngresos", BigDecimalUtil.round(objSolicitudPrestamo.getOtrosIngresos()).toPlainString()));
        parameters.add(ReportUtil.newParameter("nivelGastos", BigDecimalUtil.round(objSolicitudPrestamo.getNivelGastos()).toPlainString()));
        parameters.add(ReportUtil.newParameter("capital", BigDecimalUtil.round(objPrestamo.getMonto()).toPlainString()));
        parameters.add(ReportUtil.newParameter("capitalFinal", BigDecimalUtil.round(objPrestamo.getMonto()).toPlainString()));

        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getFichaInscripcion(SolicitudIngreso objSolicitudIngreso, Establecimiento objEstablecimiento) {
        String[] headers = {"nombrebeneficiario", "tipodnibeneficiario", "numerobeneficiario", "parentesco", "beneficio"};

        Socio objSocio = objSolicitudIngreso.getObjSocio();
        Empleado objEmpleadoPresidente = QueryFacade.getInstance().getEmpleado(TipoUsuario.TIPO_PRESIDENTE_CONSEJO_ADMINISTRACION);
        Empleado objEmpleadoGerente = QueryFacade.getInstance().getEmpleado(TipoUsuario.TIPO_GERENTE_COOPERATIVA);

        DefaultTableModel objDefaultTableModel = new DefaultTableModel(null, headers);

        if (!objSocio.getLstDetalleFamiliarSocio().isEmpty()) {
            for (DetalleFamiliarSocio objFamiliar : objSocio.getLstDetalleFamiliarSocio()) {
                objDefaultTableModel.addRow(new Object[]{
                    objFamiliar.getObjFamiliar().getNombreCompleto(),
                    objFamiliar.getObjFamiliar().getObjTipoDocumentoIdentidad().getNombre(),
                    objFamiliar.getObjFamiliar().getDocumentoIdentidad(),
                    objFamiliar.getObjParentesco().getNombre(),
                    BigDecimalUtil.round(objFamiliar.getPorcentajeBeneficio())
                });
            }
        } else {
            objDefaultTableModel.addRow(new Object[]{
                "NO TIENE BENEFICIARIO",
                "---",
                "---",
                "---",
                "---",
                0
            });
        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("beneficiarios", objDefaultTableModel);

        /*DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        simbolos.setGroupingSeparator(',');
        DecimalFormat decf = new DecimalFormat("###,###.##", simbolos);
        
        BigDecimal ingresoMensual = new BigDecimal(decf.format(objSocio.getIngresoMensual()));*/
        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        //parameters.add(ReportUtil.newParameter("codigo", objSocio.getCodigo()));
        parameters.add(ReportUtil.newParameter("foto", "image." + FileUtil.getExtension(objSocio.getRutaFoto())));
        parameters.add(ReportUtil.newParameter("nombre", objSocio.getNombre()));
        parameters.add(ReportUtil.newParameter("apellidopaterno", objSocio.getApellidoPaterno()));
        parameters.add(ReportUtil.newParameter("apellidomaterno", objSocio.getApellidoMaterno()));
        parameters.add(ReportUtil.newParameter("fechanacimiento", DateUtil.getRegularDate(DateUtil.getDate(objSocio.getFechaNacimiento()))));
        parameters.add(ReportUtil.newParameter("natural", objSocio.getOrigen()));
        parameters.add(ReportUtil.newParameter("distrito", objSocio.getObjDistrito().getNombre()));
        parameters.add(ReportUtil.newParameter("provincia", objSocio.getObjDistrito().getObjProvincia().getNombre()));
        parameters.add(ReportUtil.newParameter("departamento", objSocio.getObjDistrito().getObjProvincia().getObjDepartamento().getNombre()));
        parameters.add(ReportUtil.newParameter("ocupacion", objSocio.getOcupacion()));
        parameters.add(ReportUtil.newParameter("gradoinstruccion", objSocio.getObjGradoInstruccion().getNombre()));
        parameters.add(ReportUtil.newParameter("estadocivil", objSocio.getObjEstadoCivil().getNombre()));
        parameters.add(ReportUtil.newParameter("dni", objSocio.getDocumentoIdentidad()));
        parameters.add(ReportUtil.newParameter("tipodni", objSocio.getObjTipoDocumentoIdentidad().getNombre()));
        parameters.add(ReportUtil.newParameter("direccion", objSocio.getDireccion()));
        parameters.add(ReportUtil.newParameter("tipotrabajador", objSocio.getObjTipoTrabajador().getNombre()));
        parameters.add(ReportUtil.newParameter("direccionlaboral", objSocio.getDireccionLaboral()));
        parameters.add(ReportUtil.newParameter("ingresomensual", BigDecimalUtil.round(objSocio.getIngresoMensual()).toPlainString()));
        //parameters.add(ReportUtil.newParameter("ingresomensual", BigDecimalUtil.formatMiles(objSocio.getIngresoMensual())));
        parameters.add(ReportUtil.newParameter("importe", Aporte.PAGO_APORTE_SOLES.toPlainString()));
        parameters.add(ReportUtil.newParameter("fullnamesocio", objSocio.getNombreCompleto()));
        parameters.add(ReportUtil.newParameter("fullnamepresidente", objEmpleadoPresidente.getNombreCompleto()));
        parameters.add(ReportUtil.newParameter("dnipresidente", objEmpleadoPresidente.getDocumentoIdentidad()));
        parameters.add(ReportUtil.newParameter("fullnamegerente", objEmpleadoGerente.getNombreCompleto()));
        parameters.add(ReportUtil.newParameter("dnigerente", objEmpleadoGerente.getDocumentoIdentidad()));
        parameters.add(ReportUtil.newParameter("establecimiento", objEstablecimiento.getNombre()));

        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getAllSociosDetallado() {
        String[] headers = {"nombre", "dni", "fechaingreso", "codigo", "monto", "tipodoc",
            "nombrebeneficiario", "parentesco", "beneficio", "tipodocbeneficiario", "dnibeneficiario", "montobeneficiario"};

        DefaultTableModel objDefaultTableModel = new DefaultTableModel(null, headers);

        List<Socio> lstSocios = QueryFacade.getInstance().getAllSociosActivos(false);

        for (Socio objSocio : lstSocios) {
            BigDecimal montoSocio = QueryFacade.getInstance().getSaldoAportesSocio(objSocio);
            if (!objSocio.getLstDetalleFamiliarSocio().isEmpty()) {
                for (DetalleFamiliarSocio objFamiliar : objSocio.getLstDetalleFamiliarSocio()) {
                    objDefaultTableModel.addRow(new Object[]{
                        objSocio.getNombreCompleto(),
                        objSocio.getDocumentoIdentidad(),
                        DateUtil.getRegularDate(DateUtil.getDate(objSocio.getFechaRegistro())),
                        objSocio.getCodigo(),
                        BigDecimalUtil.round(montoSocio),
                        objSocio.getObjTipoDocumentoIdentidad().getNombre(),
                        objFamiliar.getObjFamiliar().getNombreCompleto(),
                        objFamiliar.getObjParentesco().getNombre(),
                        BigDecimalUtil.round(objFamiliar.getPorcentajeBeneficio()),
                        objFamiliar.getObjFamiliar().getObjTipoDocumentoIdentidad().getNombre(),
                        objFamiliar.getObjFamiliar().getDocumentoIdentidad(),
                        BigDecimalUtil.round(objFamiliar.getMontoBeneficiario(montoSocio))
                    });
                }
            } else {
                objDefaultTableModel.addRow(new Object[]{
                    objSocio.getNombreCompleto(),
                    objSocio.getDocumentoIdentidad(),
                    DateUtil.getRegularDate(DateUtil.getDate(objSocio.getFechaRegistro())),
                    objSocio.getCodigo(),
                    BigDecimalUtil.round(montoSocio),
                    objSocio.getObjTipoDocumentoIdentidad().getNombre(),
                    "NO TIENE BENEFICIARIO",
                    "---",
                    "---",
                    "---",
                    "---",
                    "---"
                });
            }

        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("allsocios", objDefaultTableModel);

        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        //parameters.add(ReportUtil.newParameter("", ));
        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getAllSociosNoDetallado() {
        String[] headers = {"nombre", "dni", "fechaingreso", "codigo", "monto", "tipo"};

        DefaultTableModel objDefaultTableModel = new DefaultTableModel(null, headers);

        List<Socio> lstSocios = QueryFacade.getInstance().getAllSociosActivos(false);

        for (Socio objSocio : lstSocios) {
            BigDecimal montoSocio = QueryFacade.getInstance().getSaldoAportesSocio(objSocio);
            objDefaultTableModel.addRow(new Object[]{
                objSocio.getNombreCompleto(),
                objSocio.getDocumentoIdentidad(),
                DateUtil.getRegularDate(DateUtil.getDate(objSocio.getFechaRegistro())),
                objSocio.getCodigo(),
                BigDecimalUtil.round(montoSocio),
                objSocio.getObjTipoDocumentoIdentidad().getNombre()
            });
        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("allsocios", objDefaultTableModel);

        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        //parameters.add(ReportUtil.newParameter("", ));
        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getOneSocioDetallado(Socio objSocio) {
        String[] headersBeneficiarios = {"nombrebeneficiario", "tipodocumento", "numero", "parentesco", "beneficio", "montocorres"};
        String[] headersAportes = {"montoaporte", "fechaaporte", "horaaporte", "caja", "establecimiento"};

        DefaultTableModel dtmBeneficiarios = new DefaultTableModel(null, headersBeneficiarios);
        DefaultTableModel dtmAportes = new DefaultTableModel(null, headersAportes);

        BigDecimal montoSocio = QueryFacade.getInstance().getSaldoAportesSocio(objSocio);

        if (!objSocio.getLstDetalleFamiliarSocio().isEmpty()) {
            for (DetalleFamiliarSocio objFamiliar : objSocio.getLstDetalleFamiliarSocio()) {
                dtmBeneficiarios.addRow(new Object[]{
                    objFamiliar.getObjFamiliar().getNombreCompleto(),
                    objFamiliar.getObjFamiliar().getObjTipoDocumentoIdentidad().getNombre(),
                    objFamiliar.getObjFamiliar().getDocumentoIdentidad(),
                    objFamiliar.getObjParentesco().getNombre(),
                    BigDecimalUtil.round(objFamiliar.getPorcentajeBeneficio()),
                    BigDecimalUtil.round(objFamiliar.getMontoBeneficiario(montoSocio))
                });
            }
        } else {
            dtmBeneficiarios.addRow(new Object[]{
                "NO TIENE BENEFICIARIO",
                "---",
                "---",
                "---",
                "---",
                0
            });
        }

        List<Aporte> lstAportes = QueryFacade.getInstance().getAllAportesPagados(objSocio, DateUtil.getDate(objSocio.getFechaRegistro()), DateUtil.currentTimestamp());

        if (!lstAportes.isEmpty()) {
            for (Aporte objAporte : lstAportes) {
                for (Pago objPago : objAporte.getLstPagos()) {
                    Movimiento objMovimiento = QueryFacade.getInstance().getMovimiento(objPago);
                    dtmAportes.addRow(new Object[]{
                        BigDecimalUtil.round(objAporte.getMonto()),
                        DateUtil.getRegularDate(DateUtil.getDate(objMovimiento.getFecha())), //DateUtil.getRegularDate(DateUtil.getDate(objAporte.getFechaRegistro())),
                        DateUtil.getRegularTime(DateUtil.getDate(objMovimiento.getFecha())), //DateUtil.getRegularTime(DateUtil.getDate(objAporte.getFechaRegistro())),
                        objMovimiento.getObjCaja().getCodigo(),
                        objMovimiento.getObjCaja().getObjEstablecimiento().getNombre()

                    });
                }

            }
        } else {
            dtmAportes.addRow(new Object[]{
                "NO TIENE APORTES",
                "NO TIENE APORTES",
                "---",
                "---",
                "---"
            });
        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("allbeneficiarios", dtmBeneficiarios);
        data.put("aportes", dtmAportes);

        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        parameters.add(ReportUtil.newParameter("codigo", objSocio.getCodigo()));
        parameters.add(ReportUtil.newParameter("fecha", DateUtil.getRegularDate(DateUtil.getDate(objSocio.getFechaRegistro()))));
        parameters.add(ReportUtil.newParameter("nombrecompleto", objSocio.getNombreCompleto()));
        parameters.add(ReportUtil.newParameter("dni", objSocio.getDocumentoIdentidad()));
        parameters.add(ReportUtil.newParameter("tipo", objSocio.getObjTipoDocumentoIdentidad().getNombre()));
        parameters.add(ReportUtil.newParameter("montoaporte", BigDecimalUtil.round(montoSocio).toPlainString()));

        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getReporteCrediticio(Socio objSocio) {
        String[] headers = {"monto", "fecha", "cuotasTotales", "cuotasPendientes", "cuotasRetrasadas", "cuotasVencidas", "deudaPendiente", "rol", "prestatario"};

        List<Prestamo> lstPrestamos = QueryFacade.getInstance().getAllPrestamos(objSocio, false);
        lstPrestamos.addAll(QueryFacade.getInstance().getAllPrestamosAsGarante(objSocio, false));

        DefaultTableModel tableModel = new DefaultTableModel(null, headers);

        for (Prestamo objPrestamo : lstPrestamos) {
            tableModel.addRow(new Object[]{
                BigDecimalUtil.round(objPrestamo.getMonto()),
                DateUtil.getRegularDate(DateUtil.getDate(objPrestamo.getFechaRegistro())),
                objPrestamo.getLstCuotas().size(),
                objPrestamo.getCuotasPendientes().size(),
                objPrestamo.getCuotasRetrasadas(new Date()).size(),
                objPrestamo.getCuotasVencidas().size(),
                BigDecimalUtil.round(objPrestamo.calcularDeudaPendiente(new Date())),
                objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getId() == objSocio.getId() ? "Prestatario" : "Fiador",
                objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getId() == objSocio.getId() ? "-" : objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getNombreCompleto()
            });
        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("prestamosQuery", tableModel);

        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        parameters.add(ReportUtil.newParameter("nombreSocio", objSocio.getNombreCompleto()));
        parameters.add(ReportUtil.newParameter("documentoIdentidadSocio", objSocio.getDocumentoIdentidad()));
        parameters.add(ReportUtil.newParameter("tipoDocumentoIdentidad", objSocio.getObjTipoDocumentoIdentidad().getNombre()));
        parameters.add(ReportUtil.newParameter("codigoSocio", objSocio.getCodigo()));
        parameters.add(ReportUtil.newParameter("direccionSocio", objSocio.getDireccion()));

        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getReporteCrediticioDetallado(Socio objSocio) {
        String[] headers = {"rol", "codigoPrestamo", "acreedor", "montoPrestamo", "fechaPrestamo", "estadoCuota", "montoCuota", "interesCuota", "fechaVencimientoCuota", "diasRetraso", "moraCuota", "deudaPendienteCuota"};

        List<Prestamo> lstPrestamos = QueryFacade.getInstance().getAllPrestamos(objSocio, false);
        lstPrestamos.addAll(QueryFacade.getInstance().getAllPrestamosAsGarante(objSocio, false));

        DefaultTableModel tableModel = new DefaultTableModel(null, headers);

        for (Prestamo objPrestamo : lstPrestamos) {
            for (Cuota objCuota : objPrestamo.getLstCuotas()) {
                objCuota.updateMontos(new Date());
                tableModel.addRow(new Object[]{
                    objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getId() == objSocio.getId() ? "Prestatario" : "Garante",
                    objPrestamo.getObjSolicitudPrestamo().getCodigo(),
                    objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getId() == objSocio.getId() ? "-" : objPrestamo.getObjSolicitudPrestamo().getObjSocioPrestatario().getNombreCompleto(),
                    objPrestamo.getMonto(),
                    DateUtil.getRegularDate(DateUtil.getDate(objPrestamo.getFechaRegistro())),
                    objCuota.getEstado(new Date()),
                    objCuota.getMonto(),
                    objCuota.getInteres(),
                    DateUtil.getRegularDate(DateUtil.getDate(objCuota.getFechaVencimiento())),
                    objCuota.getDiasRetraso(new Date()),
                    objCuota.getMora(),
                    objCuota.calcularMonto(new Date())
                });
            }
        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("prestamosQuery", tableModel);

        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        parameters.add(ReportUtil.newParameter("nombreSocio", objSocio.getNombreCompleto()));
        parameters.add(ReportUtil.newParameter("documentoIdentidadSocio", objSocio.getDocumentoIdentidad()));
        parameters.add(ReportUtil.newParameter("tipoDocumentoIdentidad", objSocio.getObjTipoDocumentoIdentidad().getNombre()));
        parameters.add(ReportUtil.newParameter("codigoSocio", objSocio.getCodigo()));
        parameters.add(ReportUtil.newParameter("direccionSocio", objSocio.getDireccion()));

        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getSimulacionCrediticia(Prestamo objPrestamo, TasaInteres objTasaInteres, Date fechaDesembolso) {
        String[] headers = {"numero", "vencimiento", "capital", "amortizacion", "interes", "cuota"};

        DefaultTableModel tableModel = new DefaultTableModel(null, headers);

        BigDecimal capital = new BigDecimal(BigInteger.ZERO);
        capital = capital.add(objPrestamo.getMonto());

        int numero = 1;
        for (Cuota objCuota : objPrestamo.getLstCuotas()) {
            tableModel.addRow(new Object[]{
                numero++,
                DateUtil.getRegularDate(DateUtil.getDate(objCuota.getFechaVencimiento())),
                capital,
                objCuota.getAmortizacion(),
                objCuota.getInteres(),
                objCuota.getMonto()
            });

            capital = capital.subtract(objCuota.getAmortizacion());
        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("tblCuotas", tableModel);

        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        parameters.add(ReportUtil.newParameter("tasaInteres", BigDecimalUtil.round(objTasaInteres.getTea()).toPlainString() + " %"));
        parameters.add(ReportUtil.newParameter("fechaDesembolso", DateUtil.getRegularDate(fechaDesembolso)));
        parameters.add(ReportUtil.newParameter("cuotas", String.valueOf(objPrestamo.getCuotas())));
        parameters.add(ReportUtil.newParameter("capital", objPrestamo.getMonto().toPlainString()));
        parameters.add(ReportUtil.newParameter("capitalFinal", objPrestamo.getMonto().toPlainString()));

        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> imprimirComprobante(ITransaccion objTransaccion, Establecimiento objEstablecimiento) {
        Pago objPago = null;

        if (objTransaccion instanceof Aporte) {
            objPago = QueryFacade.getInstance().getAllPagos((Aporte) objTransaccion).get(0);
        } else if (objTransaccion instanceof Deposito) {
            objPago = QueryFacade.getInstance().getAllPagos((Deposito) objTransaccion).get(0);
        } else if (objTransaccion instanceof Retiro) {
            objPago = QueryFacade.getInstance().getAllPagos((Retiro) objTransaccion).get(0);
        } else if (objTransaccion instanceof Transferencia) {
            objPago = QueryFacade.getInstance().getAllPagos((Transferencia) objTransaccion).get(0);
        } else if (objTransaccion instanceof HabilitacionCuotas) {
            objPago = QueryFacade.getInstance().getAllPagos((HabilitacionCuotas) objTransaccion).get(0);
        } else if (objTransaccion instanceof MovimientoAdministrativo) {
            objPago = QueryFacade.getInstance().getAllPagos((MovimientoAdministrativo) objTransaccion).get(0);
        } else if(objTransaccion instanceof SolicitudPrestamo){
            objPago = QueryFacade.getInstance().getAllPagos((SolicitudPrestamo) objTransaccion).get(0);
        }

        return objPago == null ? null : this.imprimirComprobante(objPago, objEstablecimiento);
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> imprimirComprobante(Pago objPago, Establecimiento objEstablecimiento) {
        Comprobante objComprobante = QueryFacade.getInstance().getComprobante(objPago);
        ITransaccion objTransaccion = QueryFacade.getInstance().getTransaccion(objPago);
        Movimiento objMovimiento = QueryFacade.getInstance().getMovimiento(objPago);

        DefaultTableModel tableModel = new DefaultTableModel();
        List<ParameterDefinitionEntry> parameters = new ArrayList<>();

        HashMap<String, TableModel> data = new HashMap<>();

        if (objTransaccion instanceof Aporte) {
            Aporte objAporte = (Aporte) objTransaccion;

            parameters.add(ReportUtil.newParameter("establecimiento", objComprobante.getObjEstablecimiento().getNombre()));
            parameters.add(ReportUtil.newParameter("caja", objMovimiento.getObjCaja().getCodigo()));
            parameters.add(ReportUtil.newParameter("cajero", objMovimiento.getObjEmpleado().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigo", objComprobante.getCodigo()));
            parameters.add(ReportUtil.newParameter("socio", objAporte.getObjTarjeta().getObjSocio().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigoSocio", objAporte.getObjTarjeta().getObjSocio().getCodigo()));
            parameters.add(ReportUtil.newParameter("fecha", DateUtil.getRegularDate(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("hora", DateUtil.getRegularTime(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("importe", objPago.getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objAporte.getMonto()).toPlainString()));
        } else if (objTransaccion instanceof Deposito) {
            Deposito objDeposito = (Deposito) objTransaccion;

            parameters.add(ReportUtil.newParameter("establecimiento", objComprobante.getObjEstablecimiento().getNombre()));
            parameters.add(ReportUtil.newParameter("caja", objMovimiento.getObjCaja().getCodigo()));
            parameters.add(ReportUtil.newParameter("cajero", objMovimiento.getObjEmpleado().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigo", objComprobante.getCodigo()));
            parameters.add(ReportUtil.newParameter("cuenta", objDeposito.getObjCuenta().getCodigo()));
            parameters.add(ReportUtil.newParameter("fecha", DateUtil.getRegularDate(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("hora", DateUtil.getRegularTime(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("importe", objPago.getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objDeposito.getMonto()).toPlainString()));
            parameters.add(ReportUtil.newParameter("tipoCuenta", objDeposito.getObjCuenta().getObjTipoCuenta().getNombre()));
            parameters.add(ReportUtil.newParameter("nombre", objDeposito.getObjCuenta().getNombreCliente()));
        } else if (objTransaccion instanceof Retiro) {
            Retiro objRetiro = (Retiro) objTransaccion;

            parameters.add(ReportUtil.newParameter("establecimiento", objComprobante.getObjEstablecimiento().getNombre()));
            parameters.add(ReportUtil.newParameter("caja", objMovimiento.getObjCaja().getCodigo()));
            parameters.add(ReportUtil.newParameter("cajero", objMovimiento.getObjEmpleado().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigo", objComprobante.getCodigo()));
            parameters.add(ReportUtil.newParameter("cuenta", objRetiro.getObjCuenta().getCodigo()));
            parameters.add(ReportUtil.newParameter("fecha", DateUtil.getRegularDate(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("hora", DateUtil.getRegularTime(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("importe", objPago.getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objRetiro.getMonto()).toPlainString()));
            parameters.add(ReportUtil.newParameter("tipoCuenta", objRetiro.getObjCuenta().getObjTipoCuenta().getNombre()));
            parameters.add(ReportUtil.newParameter("saldoActual", objRetiro.getObjCuenta().getObjTipoCuenta().getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objRetiro.getObjCuenta().getSaldo()).toPlainString()));
            parameters.add(ReportUtil.newParameter("nombre", objRetiro.getObjCuenta().getNombreCliente()));
        } else if (objTransaccion instanceof Transferencia) {
            Transferencia objTransferencia = (Transferencia) objTransaccion;

            if (objTransferencia instanceof TransferenciaCuenta) {
                TransferenciaCuenta objTransferenciaCuenta = (TransferenciaCuenta) objTransferencia;

                parameters.add(ReportUtil.newParameter("establecimiento", objComprobante.getObjEstablecimiento().getNombre()));
                parameters.add(ReportUtil.newParameter("caja", objMovimiento.getObjCaja().getCodigo()));
                parameters.add(ReportUtil.newParameter("cajero", objMovimiento.getObjEmpleado().getNombreCompleto()));
                parameters.add(ReportUtil.newParameter("codigo", objComprobante.getCodigo()));
                parameters.add(ReportUtil.newParameter("cuentaOrigen", objTransferenciaCuenta.getObjCuentaOrigen().getCodigo()));
                parameters.add(ReportUtil.newParameter("cuentaDestino", objTransferenciaCuenta.getObjCuentaDestino().getCodigo()));
                parameters.add(ReportUtil.newParameter("fecha", DateUtil.getRegularDate(DateUtil.getDate(objComprobante.getFecha()))));
                parameters.add(ReportUtil.newParameter("hora", DateUtil.getRegularTime(DateUtil.getDate(objComprobante.getFecha()))));
                parameters.add(ReportUtil.newParameter("importe", objPago.getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objTransferenciaCuenta.getMonto()).toPlainString()));
                parameters.add(ReportUtil.newParameter("saldoActual", objTransferenciaCuenta.getObjCuentaOrigen().getObjTipoCuenta().getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objTransferenciaCuenta.getObjCuentaOrigen().getSaldo()).toPlainString()));
                parameters.add(ReportUtil.newParameter("nombre", objTransferenciaCuenta.getObjCuentaDestino().getNombreCliente()));
            }
        } else if (objTransaccion instanceof HabilitacionCuotas) {
            HabilitacionCuotas objHabilitacionCuotas = (HabilitacionCuotas) objTransaccion;

            String[] headers = {"numero", "amortizacion", "interes", "mora", "monto"};

            tableModel = new DefaultTableModel(null, headers);

            for (Cuota objCuota : objHabilitacionCuotas.getLstCuotas()) {
                tableModel.addRow(new Object[]{
                    String.valueOf(objCuota.getNumero()),
                    objCuota.getAmortizacion(),
                    objCuota.getInteres(),
                    objCuota.getMora(),
                    objCuota.getMonto()
                });
            }

            data.put("tblQuery", tableModel);

            parameters.add(ReportUtil.newParameter("establecimiento", objComprobante.getObjEstablecimiento().getNombre()));
            parameters.add(ReportUtil.newParameter("caja", objMovimiento.getObjCaja().getCodigo()));
            parameters.add(ReportUtil.newParameter("cajero", objMovimiento.getObjEmpleado().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigo", objComprobante.getCodigo()));
            parameters.add(ReportUtil.newParameter("socio", objHabilitacionCuotas.getLstCuotas().get(0).getObjPrestamo().getObjSolicitudPrestamo().getObjSocioPrestatario().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigoSocio", objHabilitacionCuotas.getLstCuotas().get(0).getObjPrestamo().getObjSolicitudPrestamo().getObjSocioPrestatario().getCodigo()));
            parameters.add(ReportUtil.newParameter("fecha", DateUtil.getRegularDate(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("hora", DateUtil.getRegularTime(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("importe", objPago.getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objHabilitacionCuotas.getMonto()).toPlainString()));
        } else if (objTransaccion instanceof MovimientoAdministrativo) {
            MovimientoAdministrativo objMovimientoAdministrativo = (MovimientoAdministrativo) objTransaccion;

            parameters.add(ReportUtil.newParameter("establecimiento", objComprobante.getObjEstablecimiento().getNombre()));
            parameters.add(ReportUtil.newParameter("caja", objMovimiento.getObjCaja().getCodigo()));
            parameters.add(ReportUtil.newParameter("cajero", objMovimiento.getObjEmpleado().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigo", objComprobante.getCodigo()));
            parameters.add(ReportUtil.newParameter("empleado", objMovimientoAdministrativo.getObjEmpleado().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("tipoMovimiento", objMovimientoAdministrativo.isIngreso() ? "RECIBO DE INGRESO" : "RECIBO DE EGRESO"));
            parameters.add(ReportUtil.newParameter("descripcion", objMovimientoAdministrativo.getDescripcion()));
            parameters.add(ReportUtil.newParameter("fecha", DateUtil.getRegularDate(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("hora", DateUtil.getRegularTime(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("importe", objPago.getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objMovimientoAdministrativo.getMonto()).toPlainString()));
        } else if(objTransaccion instanceof SolicitudPrestamo){
            SolicitudPrestamo objSolicitudPrestamo = (SolicitudPrestamo) objTransaccion;

            parameters.add(ReportUtil.newParameter("establecimiento", objComprobante.getObjEstablecimiento().getNombre()));
            parameters.add(ReportUtil.newParameter("caja", objMovimiento.getObjCaja().getCodigo()));
            parameters.add(ReportUtil.newParameter("cajero", objMovimiento.getObjEmpleado().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigo", objComprobante.getCodigo()));
            parameters.add(ReportUtil.newParameter("socio", objSolicitudPrestamo.getObjSocioPrestatario().getNombreCompleto()));
            parameters.add(ReportUtil.newParameter("codigoSocio", objSolicitudPrestamo.getObjSocioPrestatario().getCodigo()));
            parameters.add(ReportUtil.newParameter("fecha", DateUtil.getRegularDate(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("hora", DateUtil.getRegularTime(DateUtil.getDate(objComprobante.getFecha()))));
            parameters.add(ReportUtil.newParameter("importe", objPago.getObjMoneda().getSimbolo() + " " + BigDecimalUtil.round(objSolicitudPrestamo.getMonto()).toPlainString()));
        }

        parameters.add(ReportUtil.newParameter("direccion", objEstablecimiento.getDireccion()));
        parameters.add(ReportUtil.newParameter("ubigeo", ConfigFacade.getConfig("ubigeo").isEmpty() ? objEstablecimiento.getObjDistrito().getUbigeo() : ConfigFacade.getConfig("ubigeo")));
        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }

    public Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> getEstadoCuentaData(Cuenta objCuenta, Date fechaDesde, Date fechaHasta) {
        List<HistoricoCuenta> lstHistoricoCuenta = QueryFacade.getInstance().getAllHistoricoCuenta(objCuenta, fechaDesde, fechaHasta);

        String[] headers = {"fecha", "descripcion", "hora", "monto", "saldo"};

        DefaultTableModel tableModel = new DefaultTableModel(null, headers);

        for (HistoricoCuenta objHistoricoCuenta : lstHistoricoCuenta) {
            tableModel.addRow(new Object[]{
                DateUtil.getRegularDate(DateUtil.getDate(objHistoricoCuenta.getFecha())),
                objHistoricoCuenta.getObjComprobante().getDescripcion(),
                DateUtil.getRegularTime(DateUtil.getDate(objHistoricoCuenta.getFecha())),
                BigDecimalUtil.round(objHistoricoCuenta.getObjComprobante().getMonto()).toPlainString(),
                BigDecimalUtil.round(objHistoricoCuenta.getSaldo()).toPlainString()
            });
        }

        HashMap<String, TableModel> data = new HashMap<>();
        data.put("tableQuery", tableModel);

        Calendar fechaAnterior = Calendar.getInstance();
        fechaAnterior.setTime(fechaDesde);
        fechaAnterior.add(Calendar.DAY_OF_MONTH, -1);

        List<HistoricoCuenta> lstHistoricoCuentaAnterior = QueryFacade.getInstance().getAllHistoricoCuenta(objCuenta, fechaAnterior.getTime(), fechaDesde);
        BigDecimal saldoContableInicial = lstHistoricoCuentaAnterior.isEmpty() ? BigDecimal.ZERO : lstHistoricoCuentaAnterior.get(lstHistoricoCuentaAnterior.size() - 1).getSaldo();
        BigDecimal saldoContableFinal = lstHistoricoCuenta.isEmpty() ? BigDecimal.ZERO : lstHistoricoCuenta.get(lstHistoricoCuenta.size() - 1).getSaldo();

        List<ParameterDefinitionEntry> parameters = new ArrayList<>();
        parameters.add(ReportUtil.newParameter("saldoContableInicial", BigDecimalUtil.round(saldoContableInicial).toPlainString()));
        parameters.add(ReportUtil.newParameter("saldoContableFinal", BigDecimalUtil.round(saldoContableFinal).toPlainString()));
        parameters.add(ReportUtil.newParameter("depositoEfectivo", BigDecimalUtil.round(HistoricoCuenta.getTotalDepositosEfectivo(lstHistoricoCuenta)).toPlainString()));
        parameters.add(ReportUtil.newParameter("depositoOtros", BigDecimalUtil.round(HistoricoCuenta.getTotalDepositosOtros(lstHistoricoCuenta)).toPlainString()));
        parameters.add(ReportUtil.newParameter("retiros", BigDecimalUtil.round(HistoricoCuenta.getTotalRetiros(lstHistoricoCuenta)).toPlainString()));
        parameters.add(ReportUtil.newParameter("moneda", objCuenta.getObjTipoCuenta().getObjMoneda().getNombre()));
        parameters.add(ReportUtil.newParameter("codigoCuenta", objCuenta.getCodigo()));
        parameters.add(ReportUtil.newParameter("nombreCliente", objCuenta.getNombreCliente()));
        parameters.add(ReportUtil.newParameter("codigoSocio", objCuenta.getCodigoCliente()));
        parameters.add(ReportUtil.newParameter("direccionCliente", objCuenta.getDireccionCliente()));
        parameters.add(ReportUtil.newParameter("fechaInicio", DateUtil.getRegularDate(fechaDesde)));
        parameters.add(ReportUtil.newParameter("fechaFin", DateUtil.getRegularDate(fechaHasta)));
        parameters.add(ReportUtil.newParameter("intereses", HistoricoCuenta.getTotalIntereses(lstHistoricoCuenta).toPlainString()));
        parameters.add(ReportUtil.newParameter("saldoPromedio", BigDecimalUtil.round((saldoContableFinal.add(saldoContableInicial)).divide(BigDecimal.valueOf(2), 4, RoundingMode.HALF_UP)).toPlainString()));

        Map<HashMap<String, TableModel>, List<ParameterDefinitionEntry>> map = new HashMap<>();
        map.put(data, parameters);

        return map;
    }
}
