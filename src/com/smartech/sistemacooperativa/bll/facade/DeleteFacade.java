package com.smartech.sistemacooperativa.bll.facade;

import com.smartech.sistemacooperativa.dal.DAOAporte;
import com.smartech.sistemacooperativa.dal.DAOEmpleado;
import com.smartech.sistemacooperativa.dal.DAOMovimientoAdministrativo;
import com.smartech.sistemacooperativa.dal.DAOSolicitudIngreso;
import com.smartech.sistemacooperativa.dal.DAOSolicitudPrestamo;
import com.smartech.sistemacooperativa.dal.DAOTarjeta;
import com.smartech.sistemacooperativa.dal.DAOTasaInteres;
import com.smartech.sistemacooperativa.dal.DAOTipoInteres;
import com.smartech.sistemacooperativa.dal.DAOUsuario;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Solicitud;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DeleteFacade {

    private static DeleteFacade instance = null;

    private DeleteFacade() {
    }

    public static DeleteFacade getInstance() {
        if (instance == null) {
            instance = new DeleteFacade();
        }

        return instance;
    }

    public boolean deleteSolicitud(Solicitud objSolicitud) {
        boolean result = false;

        try {
            objSolicitud.setEstado(false);
            if (objSolicitud instanceof SolicitudIngreso) {
                result = DAOSolicitudIngreso.getInstance().update((SolicitudIngreso) objSolicitud);
            }

            if (objSolicitud instanceof SolicitudPrestamo) {
                result = DAOSolicitudPrestamo.getInstance().update((SolicitudPrestamo) objSolicitud);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean deleteEmpleado(Empleado objEmpleado) {
        boolean result = false;

        try {
            result = DAOEmpleado.getInstance().delete(objEmpleado);
            if (result) {
                result = DAOUsuario.getInstance().delete(objEmpleado.getObjUsuario());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean deleteTipoInteres(TipoInteres objTipoInteres) {
        boolean result = false;

        try {
            result = DAOTipoInteres.getInstance().delete(objTipoInteres);
            if (result) {
                result = this.deleteTasasInteres(objTipoInteres.getLstTasasInteres());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean deleteTasasInteres(List<TasaInteres> lstTasasInteres) {
        boolean result = false;

        try {
            if (lstTasasInteres.isEmpty()) {
                return true;
            }
            for (TasaInteres objTasaInteres : lstTasasInteres) {
                result = DAOTasaInteres.getInstance().delete(objTasaInteres);
                if (!result) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
    
    public boolean deleteAporte(Aporte objAporte){
        return DAOAporte.getInstance().delete(objAporte);
    }
    
    public boolean deleteMovimientoAdministrativo(MovimientoAdministrativo objMovimientoAdministrativo){
        return DAOMovimientoAdministrativo.getInstance().delete(objMovimientoAdministrativo);
    }
    
    public boolean deleteTarjeta(Tarjeta objTarjeta){
        return DAOTarjeta.getInstance().physicalDelete(objTarjeta);
    }
    
    public boolean deleteAportePhysical(Aporte objAporte){
        return DAOAporte.getInstance().physicalDelete(objAporte);
    }
}
