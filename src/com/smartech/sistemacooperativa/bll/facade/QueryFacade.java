package com.smartech.sistemacooperativa.bll.facade;

import com.smartech.sistemacooperativa.dal.*;
import com.smartech.sistemacooperativa.dominio.*;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.FileUtil;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class QueryFacade {

    private static QueryFacade instance = null;

    private QueryFacade() {
    }

    public static QueryFacade getInstance() {
        if (instance == null) {
            instance = new QueryFacade();
        }

        return instance;
    }

    public Empleado getEmpleado(int idTipoUsuario) {
        return DAOEmpleado.getInstance().getEmpleado(idTipoUsuario);
    }

    public Permiso getPermiso(int id) {
        return DAOPermiso.getInstance().getPermiso(id);
    }

    public List<Establecimiento> getAllEstablecimientos() {
        return DAOEstablecimiento.getInstance().getAllEstablecimientos();
    }

    public List<Caja> getAllCajas(Establecimiento objEstablecimiento) {
        return DAOCaja.getInstance().getAllCajas(objEstablecimiento);
    }

    public List<Caja> getAllCajasAbiertas(Establecimiento objEstablecimiento) {
        return DAOCaja.getInstance().getAllCajasAbiertas(objEstablecimiento);
    }

    public List<Caja> getAllCajasCerradas(Establecimiento objEstablecimiento) {
        return DAOCaja.getInstance().getAllCajasCerradas(objEstablecimiento);
    }

    public List<Departamento> getAllDepartamentos() {
        return DAODepartamento.getInstance().getAllDepartamentos();
    }

    public List<TipoTrabajador> getAllTiposTrabajador() {
        return DAOTipoTrabajador.getInstance().getAllTiposTrabajador();
    }

    public Socio getSocioByDocumentoIdentidad(String documentoIdentidad, boolean cargarFoto) {
        FileUtil.connect();

        Socio objSocio = DAOSocio.getInstance().getSocioByDocumentoIdentidad(documentoIdentidad, cargarFoto);
        if (objSocio != null) {
            if (objSocio.isEstado()) {
                int size = objSocio.getLstCuentas().size();
                for (int i = 0; i < size; i++) {
                    if (!objSocio.getLstCuentas().get(i).isEstado()) {
                        objSocio.getLstCuentas().remove(i);
                        size--;
                        i--;
                    }
                }
            } else {
                objSocio = null;
            }
        }

        FileUtil.disconnect();
        return objSocio;
    }

    public Socio getSocioByRUC(String ruc, boolean cargarFoto) {
        FileUtil.connect();

        Socio objSocio = DAOSocio.getInstance().getSocioByRUC(ruc, cargarFoto);

        FileUtil.disconnect();

        return objSocio;
    }

    public Socio getSocioByCodigo(String codigo, boolean cargarFoto) {
        FileUtil.connect();

        Socio objSocio = DAOSocio.getInstance().getSocioByCodigo(codigo, cargarFoto);

        FileUtil.disconnect();

        return objSocio;
    }

    public List<Deposito> getAllDepositos(Date fechaDesde, Date fechaHasta) {
        List<Deposito> lstDepositos = DAODeposito.getInstance().getAllDepositos(DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
        for (int i = 0; i < lstDepositos.size(); i++) {
            if (lstDepositos.get(i).getObjCuenta().getObjTipoCuenta().getId() == TipoCuenta.TIPO_FONDO_MORTUORIO) {
                lstDepositos.remove(i);
                i--;
            }
        }
        return lstDepositos;
    }

    public List<Deposito> getAllDepositosFondoMortuorio(Date fechaDesde, Date fechaHasta) {
        List<Deposito> lstDepositos = DAODeposito.getInstance().getAllDepositos(DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
        for (int i = 0; i < lstDepositos.size(); i++) {
            if (lstDepositos.get(i).getObjCuenta().getObjTipoCuenta().getId() != TipoCuenta.TIPO_FONDO_MORTUORIO) {
                lstDepositos.remove(i);
                i--;
            }
        }
        return lstDepositos;
    }

    public List<Retiro> getAllRetiros(Date fechaDesde, Date fechaHasta) {
        return DAORetiro.getInstance().getAllRetiros(DateUtil.getTimestamp(fechaDesde), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
    }

    public List<Cuenta> getAllCuentas() {
        return DAOCuenta.getInstance().getAllCuentas();
    }

    public List<Cuenta> getAllCuentasHabilitadas() {
        return DAOCuenta.getInstance().getAllCuentasHabilitadas();
    }

    public List<Deposito> getAllDepositos(Cuenta objCuenta) {
        return DAODeposito.getInstance().getAllDepositos(objCuenta);
    }

    public Cuenta getCuenta(String numeroCuenta) {
        return DAOCuenta.getInstance().getCuenta(numeroCuenta);
    }

    public Cuenta getCuentaHabilitada(String numeroCuenta) {
        Cuenta objCuenta = DAOCuenta.getInstance().getCuenta(numeroCuenta);
        if (objCuenta != null && objCuenta.isEstado()) {
            return objCuenta;
        }
        return null;
    }

    public List<Cuenta> getAllCuentas(Socio objSocio) {
        return DAOCuenta.getInstance().getAllCuentas(objSocio);
    }

    public List<Cuenta> getCuentasSocios(TipoCuenta objTipoCuenta, List<Socio> lstSocios) {
        return DAOCuenta.getInstance().getCuentasSocios(objTipoCuenta, lstSocios);
    }

    public List<Socio> getAllSocios(boolean cargarFoto) {
        FileUtil.connect();

        List<Socio> lstSocios = DAOSocio.getInstance().getAllSocios(cargarFoto);

        FileUtil.disconnect();

        return lstSocios;
    }

    public List<Socio> getAllSocios(Cuenta objCuenta, boolean cargarFoto) {
        FileUtil.connect();

        List<Socio> lstSocios = DAOSocio.getInstance().getAllSocios(objCuenta, cargarFoto);

        FileUtil.disconnect();

        return lstSocios;
    }

    public List<Socio> getAllSociosOnly(Cuenta objCuenta, boolean cargarFoto) {
        FileUtil.connect();

        List<Socio> lstSocios = DAOSocio.getInstance().getAllSociosOnly(objCuenta, cargarFoto);

        FileUtil.disconnect();

        return lstSocios;
    }

    public List<Parentesco> getAllParentescos() {
        return DAOParentesco.getInstance().getAllParentescos();
    }

    public List<TipoSolicitud> getAllTiposSolicitud(Establecimiento objEstablecimiento) {
        return DAOTipoSolicitud.getInstance().getAllTiposSolicitud(objEstablecimiento);
    }

    public TipoSolicitud getTipoSolicitud(int id, Establecimiento objEstablecimiento) {
        return DAOTipoSolicitud.getInstance().getTipoSolicitud(id, objEstablecimiento);
    }

    public List<TipoPago> getAllTipoPago() {
        return DAOTipoPago.getInstance().getAllTipoPago();
    }

    public Moneda getMonedaByAbreviatura(String abreviatura) {
        return DAOMoneda.getInstance().getMonedaByAbreviatura(abreviatura);
    }

    public List<Moneda> getAllMonedas() {
        return DAOMoneda.getInstance().getAllMonedas();
    }

    public List<Moneda> getAllMonedasCambio() {
        return DAOMoneda.getInstance().getAllMonedasCambio();
    }

    public Moneda getMonedaBase() {
        return DAOMoneda.getInstance().getMonedaBase();
    }

    public List<Cuota> getAllCuotasPorPagar(Socio objSocio) {
        return DAOCuota.getInstance().getAllCuotasPorPagar(objSocio);
    }

    public List<SolicitudIngreso> getAllSolicitudesIngresoPorPagar(Socio objSocio) {
        FileUtil.connect();

        List<SolicitudIngreso> lstSolicitudesIngreso = DAOSolicitudIngreso.getInstance().getAllSolicitudesIngresoPorPagar(objSocio);

        FileUtil.disconnect();

        return lstSolicitudesIngreso;
    }

    public List<SolicitudIngreso> getAllSolicitudesIngresoPorAprobarDocumentos(Socio objSocio) {
        FileUtil.connect();

        List<SolicitudIngreso> lstSolicitudesIngreso = DAOSolicitudIngreso.getInstance().getAllSolicitudesIngresoPorAprobarDocumentos(objSocio);

        FileUtil.disconnect();

        return lstSolicitudesIngreso;
    }

    public List<SolicitudIngreso> getAllSolicitudesIngresoPorPagar() {
        FileUtil.connect();

        List<SolicitudIngreso> lstSolicitudesIngreso = DAOSolicitudIngreso.getInstance().getAllSolicitudesIngresoPorPagar();

        FileUtil.disconnect();

        return lstSolicitudesIngreso;
    }

    public List<SolicitudIngreso> getAllSolicitudesIngresoPorAprobarDocumentos() {
        FileUtil.connect();

        List<SolicitudIngreso> lstSolicitudesIngreso = DAOSolicitudIngreso.getInstance().getAllSolicitudesIngresoPorAprobarDocumentos();

        FileUtil.disconnect();

        return lstSolicitudesIngreso;
    }

    public List<SolicitudIngreso> getAllSolicitudesIngreso(Socio objSocio) {
        FileUtil.connect();

        List<SolicitudIngreso> lstSolicitudesIngreso = DAOSolicitudIngreso.getInstance().getAllSolicitudesIngreso(objSocio);

        FileUtil.disconnect();

        return lstSolicitudesIngreso;
    }

    public SolicitudIngreso getLastSolicitudIngreso(Socio objSocio) {
        FileUtil.connect();

        List<SolicitudIngreso> lstSolicitudesIngreso = DAOSolicitudIngreso.getInstance().getAllSolicitudesIngreso(objSocio); // order by id asc

        FileUtil.disconnect();
        int size = lstSolicitudesIngreso.size();

        SolicitudIngreso objSolicitudIngreso;
        for (int i = size - 1; i >= 0; i--) {
            objSolicitudIngreso = lstSolicitudesIngreso.get(i);
            if (objSolicitudIngreso.isEstado() && objSolicitudIngreso.isAprobado() && objSolicitudIngreso.isPagado()) {
                return objSolicitudIngreso;
            }
        }
        return null;
    }

    public List<SolicitudPrestamo> getAllSolicitudesPrestamosPorAprobar() {
        FileUtil.connect();

        List<SolicitudPrestamo> lstSolicitudesPrestamo = DAOSolicitudPrestamo.getInstance().getAllSolicitudesPrestamoPorAprobar();

        FileUtil.disconnect();

        return lstSolicitudesPrestamo;
    }

    public List<Socio> getAllSociosActivos(boolean cargarFoto) {
        FileUtil.connect();

        List<Socio> lstSocios = DAOSocio.getInstance().getAllSociosActivos(cargarFoto);

        FileUtil.disconnect();

        return lstSocios;
    }

    public List<Socio> getAllSociosPasivos(Date fechaLimite, boolean cargarFoto) {
        FileUtil.connect();

        List<Socio> lstSocios = DAOSocio.getInstance().getAllSociosPasivos(DateUtil.getTimestamp(fechaLimite), cargarFoto);

        FileUtil.disconnect();

        return lstSocios;
    }

    public List<TipoCuenta> getAllTiposCuenta(boolean manual) {
        List<TipoCuenta> lstTiposCuenta = DAOTipoCuenta.getInstance().getAllTiposCuenta();
        if (manual) {
            for (int i = 0; i < lstTiposCuenta.size(); i++) {
                if (!lstTiposCuenta.get(i).isRegistroManual()) {
                    lstTiposCuenta.remove(i);
                    i--;
                }
            }
        }
        return lstTiposCuenta;
    }

    public List<TipoInteres> getAllTiposInteres() {
        return DAOTipoInteres.getInstance().getAllTiposInteres();
    }

    public List<TipoInteres> getAllTiposInteres(TipoCuenta objTipoCuenta) {
        return DAOTipoInteres.getInstance().getAllTiposInteres(objTipoCuenta);
    }

    public TasaInteres getTasaInteres(TipoInteres objTipoInteres) {
        return DAOTasaInteres.getInstance().getTasaInteres(objTipoInteres);
    }

    public TipoUsuario getTipoUsuario(int id) {
        return DAOTipoUsuario.getInstance().getTipoUsuario(id);
    }

    public TipoTarjeta getTipoTarjeta(int id) {
        return DAOTipoTarjeta.getInstance().getTipoTarjeta(id);
    }

    public List<Concepto> getAllConceptos() {
        return DAOConcepto.getInstance().getAllConceptos();
    }

    public List<Concepto> getAllConceptosOnly() {
        return DAOConcepto.getInstance().getAllConceptosOnly();
    }

    public List<Pago> getAllPagos(Date fechaDesde, Date fechaHasta) {
        return DAOPago.getInstance().getAllPagos(DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
    }

    public List<Pago> getAllPagos(Date fechaDesde, Date fechaHasta, Concepto objConcepto) {
        return DAOPago.getInstance().getAllPagos(objConcepto, DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
    }

    public List<Pago> getAllPagosRetiros(Aporte objAporte, Date fechaDesde, Date fechaHasta) {
        return DAOPago.getInstance().getAllPagos(objAporte, DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
    }

    public Aporte getAporteIngresoImpago(Socio objSocio) {
        return DAOAporte.getInstance().getAporteIngresoImpago(objSocio);
    }

    public List<Aporte> getAllAportesHabilitados(Date fechaDesde, Date fechaHasta) {
        return DAOAporte.getInstance().getAllAportesHabilitados(DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
    }

    public List<Aporte> getAllAportesPagados(Date fechaDesde, Date fechaHasta) {
        return DAOAporte.getInstance().getAllAportesPagados(DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
    }

    public List<Aporte> getAllAportesPagados(Socio objSocio, Date fechaDesde, Date fechaHasta) {
        return DAOAporte.getInstance().getAllAportesPagados(objSocio, DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
    }

    public Aporte getLastAporte(Socio objSocio) {
        return DAOAporte.getInstance().getLastAporte(objSocio);
    }

    public Concepto getConcepto(int id) {
        return DAOConcepto.getInstance().getConcepto(id);
    }

    public Concepto getConcepto(String nombre) {
        return DAOConcepto.getInstance().getConcepto(nombre);
    }

    public Tarjeta getTarjeta(String numeroTarjeta) {
        Tarjeta objTarjeta = DAOTarjeta.getInstance().getTarjeta(numeroTarjeta);

        if (objTarjeta != null) {
            int size = objTarjeta.getObjSocio().getLstCuentas().size();
            for (int i = 0; i < size; i++) {
                if (!objTarjeta.getObjSocio().getLstCuentas().get(i).isEstado()) {
                    objTarjeta.getObjSocio().getLstCuentas().remove(i);
                    size--;
                    i--;
                }
            }
            if (objTarjeta.getFechaBloqueoIntentos() == null) {
                return objTarjeta;
            } else if (DateUtil.getMinutesDifference(objTarjeta.getFechaBloqueoIntentos(), DateUtil.currentTimestamp()) >= Tarjeta.MINUTOS_BLOQUEO_TARJETA) {
                return objTarjeta;
            } else {
                return new Tarjeta(0, "", "", null, true, 3, null, null, null, false, null, null);
            }
        }

        return objTarjeta;
    }

    public List<EstadoCivil> getAllEstadosCiviles() {
        return DAOEstadoCivil.getInstance().getAllEstadosCiviles();
    }

    public EstadoCivil getEstadoCivil(int id) {
        return DAOEstadoCivil.getInstance().getEstadoCivil(id);
    }

    public List<GradoInstruccion> getAllGradosInstruccion() {
        return DAOGradoInstruccion.getInstance().getAllGradosInstruccion();
    }

    public GradoInstruccion getGradoInstruccion(int id) {
        return DAOGradoInstruccion.getInstance().getGradoInstruccion(id);
    }

    public SolicitudIngreso getSolicitudIngresoVigente(String dniSocio) {
        return DAOSolicitudIngreso.getInstance().getSolicitudIngresoVigente(dniSocio);
    }

    public long getUltimoCodigoSolicitudIngreso(int year) {
        return DAOSolicitudIngreso.getInstance().getUltimoCodigo(year);
    }

    public long getSiguienteCodigoSolicitudIngreso(int year) {
        return getUltimoCodigoSolicitudIngreso(year) + 1;
    }

    public long getUltimoCodigoSolicitudPrestamo(int year) {
        return DAOSolicitudPrestamo.getInstance().getUltimoCodigo(year);
    }

    public long getSiguienteCodigoSolicitudPrestamo(int year) {
        return getUltimoCodigoSolicitudPrestamo(year);
    }
    
    public long getUltimoCodigoSocio(){
        return DAOSocio.getInstance().getUltimoCodigo();
    }
    
    public long getSiguienteCodigoSocio(){
        return getUltimoCodigoSocio() + 1;
    }

    public String getUltimoCodigoTarjeta() {
        return DAOTarjeta.getInstance().getUltimoCodigoTarjeta();
    }

    public List<ITransaccion> getAllTransacciones(Concepto objConcepto, Date fechaDesde, Date fechaHasta, int modo) {
        return DAOTransaccion.getInstance().getAllTransacciones(objConcepto, DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)), modo);
    }

    public List<ITransaccion> getAllTransacciones(Cuenta objCuenta, Date fechaDesde, Date fechaHasta, int modo) {
        return DAOTransaccion.getInstance().getAllTransacciones(objCuenta, DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)), modo);
    }

    public List<Prestamo> getAllPrestamos(Socio objSocio, boolean pagado) {
        return DAOPrestamo.getInstance().getAllPrestamos(objSocio, pagado);
    }

    public List<Prestamo> getAllPrestamos(Socio objSocio, Date fechaDesde, Date fechaHasta, boolean pagado) {
        return DAOPrestamo.getInstance().getAllPrestamos(objSocio, DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)), pagado);
    }

    public List<Prestamo> getAllPrestamosAsGarante(Socio objSocio, boolean pagado) {
        return DAOPrestamo.getInstance().getAllPrestamosAsGarante(objSocio, pagado);
    }

    public List<Prestamo> getAllPrestamosAsGarante(Socio objSocio, Date fechaDesde, Date fechaHasta, boolean pagado) {
        return DAOPrestamo.getInstance().getAllPrestamosAsGarante(objSocio, DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)), pagado);
    }

    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPorAprobarAsGarante(Socio objSocio) {
        return DAOSolicitudPrestamo.getInstance().getAllSolicitudesPrestamosPorAprobarAsFiador(objSocio);
    }

    public List<Aporte> getAllAportes(Socio objSocio) {
        return DAOAporte.getInstance().getAllAportes(objSocio);
    }

    public SolicitudPrestamo getSolicitudPrestamoVigente(Socio objSocio) {
        return DAOSolicitudPrestamo.getInstance().getSolicitudPrestamoVigente(objSocio);
    }

    public List<TipoPrestamo> getAllTiposPrestamo() {
        return DAOTipoPrestamo.getInstance().getAllTiposPrestamo();
    }

    public HistoricoCaja getHistoricoCaja(Caja objCaja) {
        return DAOHistoricoCaja.getInstance().getLastHistoricoCaja(objCaja);
    }

    public BigDecimal getSaldoAhorroSocio(Socio objSocio) {
        return DAOCuenta.getInstance().getSaldoCuentas(objSocio);
    }

    public BigDecimal getSaldoAportesSocio(Socio objSocio) {
        return DAOAporte.getInstance().getSaldoAportes(objSocio);
    }

    public List<HistoricoCuenta> getAllHistoricoCuenta(Cuenta objCuenta, Date fechaDesde, Date fechaHasta) {
        return DAOHistoricoCuenta.getInstance().getAllHistoricoCuenta(objCuenta, DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)));
    }

    public List<DetalleTipoSolicitudDocumento> getAllDetalleTipoSolicitudDocumento(TipoSolicitud objTipoSolicitud, Establecimiento objEstablecimiento) {
        return DAODetalleTipoSolicitudDocumento.getInstance().getAllDetalleTipoSolicitudDocumento(objTipoSolicitud, objEstablecimiento);
    }

    public SolicitudRetiroSocio getLastSolicitudRetiroSocio(Socio objSocio) {
        FileUtil.connect();
        SolicitudRetiroSocio objSolicitudRetiroSocio = DAOSolicitudRetiroSocio.getInstance().getLastSolicitudRetiroSocio(objSocio);
        FileUtil.disconnect();
        return objSolicitudRetiroSocio;
    }

    public List<Usuario> getAllUsuarios(TipoUsuario objTipoUsuario) {
        return DAOUsuario.getInstance().getAllUsuarios(objTipoUsuario);
    }

    public TipoPago getTipoPago(int id) {
        return DAOTipoPago.getInstance().getTipoPago(id);
    }

    public Prestamo getPrestamo(long id) {
        return DAOPrestamo.getInstance().getPrestamo(id);
    }

    public List<Empleado> getAllEmpleadosDebajoNivel(Usuario objUsuario, Establecimiento objEstablecimiento) {
        return DAOEmpleado.getInstance().getAllEmpleadosDebajoNivel(objUsuario, objEstablecimiento);
    }

    public List<TipoUsuario> getAllTiposUsuarioEmpleadosDebajoNivel(Usuario objUsuario) {
        return DAOTipoUsuario.getInstance().getAllTiposUsuarioEmpleadosDebajoNivel(objUsuario);
    }

    public String getLastCodigoCuenta() {
        return DAOCuenta.getInstance().getLastCodigoCuenta();
    }

    public HistoricoCaja getLastHistoricoCaja(Empleado objEmpleado) {
        return DAOHistoricoCaja.getInstance().getLastHistoricoCaja(objEmpleado);
    }

    public List<TipoDocumentoIdentidad> getAllTiposDocumentoIdentidad() {
        return DAOTipoDocumentoIdentidad.getInstance().getAllTiposDocumentoIdentidad();
    }

    public Socio getSocioByDocumentoIdentidad(String documentoIdentidad, TipoDocumentoIdentidad objTipoDocumentoIdentidad, boolean cargarFoto) {
        FileUtil.connect();
        Socio objSocio = DAOSocio.getInstance().getSocioByDocumentoIdentidad(documentoIdentidad, objTipoDocumentoIdentidad, cargarFoto);
        if (objSocio != null) {
            if (objSocio.isEstado()) {
                int size = objSocio.getLstCuentas().size();
                for (int i = 0; i < size; i++) {
                    if (!objSocio.getLstCuentas().get(i).isEstado()) {
                        objSocio.getLstCuentas().remove(i);
                        size--;
                        i--;
                    }
                }
            } else {
                objSocio = null;
            }
        }
        FileUtil.disconnect();
        return objSocio;
    }

    public Movimiento getMovimiento(Pago objPago) {
        return DAOMovimiento.getInstance().getMovimiento(objPago);
    }

    public AccesoSistema getLastAccesoSistema(Usuario objUsuario) {
        return DAOAccesoSistema.getInstance().getLastAccesoSistema(objUsuario);
    }

    public AccesoSistema getLastAccesoSistema() {
        return DAOAccesoSistema.getInstance().getLastAccesoSistema();
    }

    public Comprobante getComprobante(Pago objPago) {
        return DAOComprobante.getInstance().getComprobante(objPago);
    }

    public ITransaccion getTransaccion(Pago objPago) {
        return DAOTransaccion.getInstance().getTransaccion(objPago);
    }

    public List<Pago> getAllPagos(Aporte objAporte) {
        return DAOPago.getInstance().getAllPagos(objAporte);
    }

    public List<Pago> getAllPagos(Retiro objRetiro) {
        return DAOPago.getInstance().getAllPagos(objRetiro);
    }

    public List<Pago> getAllPagos(Deposito objDeposito) {
        return DAOPago.getInstance().getAllPagos(objDeposito);
    }

    public List<Pago> getAllPagos(Transferencia objTransferencia) {
        return DAOPago.getInstance().getAllPagos(objTransferencia);
    }

    public List<Pago> getAllPagos(HabilitacionCuotas objHabilitacionCuotas) {
        return DAOPago.getInstance().getAllPagos(objHabilitacionCuotas);
    }

    public List<Pago> getAllPagos(MovimientoAdministrativo objMovimientoAdministrativo) {
        return DAOPago.getInstance().getAllPagos(objMovimientoAdministrativo);
    }

    public List<Pago> getAllPagos(SolicitudPrestamo objSolicitudPrestamo) {
        return DAOPago.getInstance().getAllPagos(objSolicitudPrestamo);
    }

    public Tarjeta getTarjeta(Socio objSocio) {
        return DAOTarjeta.getInstance().getTarjeta(objSocio);
    }

    public BigDecimal getMontoDisponibleAsGarante(Socio objSocio) {
        List<Prestamo> lstPrestamos = QueryFacade.getInstance().getAllPrestamosAsGarante(objSocio, false);
        //lstPrestamos.addAll(QueryFacade.getInstance().getAllPrestamosAsGarante(objSocio, false));
        List<SolicitudPrestamo> lstSolicitudesPrestamo = QueryFacade.getInstance().getAllSolicitudesPrestamoPorAprobarAsGarante(objSocio);

        BigDecimal montoAportes = QueryFacade.getInstance().getSaldoAportesSocio(objSocio);
        BigDecimal montoGarantias = BigDecimal.ZERO;
        for (Prestamo objPrestamo : lstPrestamos) {
            BigDecimal montoPrestamo = objPrestamo.getMonto();
            montoPrestamo = montoPrestamo.divide(BigDecimal.valueOf(objPrestamo.getObjSolicitudPrestamo().getLstSociosGarantes().size() + 1), 4, RoundingMode.HALF_UP);
            montoGarantias = montoGarantias.add(montoPrestamo);
        }

        for (SolicitudPrestamo objSolicitudPrestamo : lstSolicitudesPrestamo) {
            BigDecimal montoPrestamo = objSolicitudPrestamo.getMontoSolicitado();
            montoPrestamo = montoPrestamo.divide(BigDecimal.valueOf(objSolicitudPrestamo.getLstSociosGarantes().size() + 1), 4, RoundingMode.HALF_UP);
            montoGarantias = montoGarantias.add(montoPrestamo);
        }

        if (montoAportes.compareTo(montoGarantias) >= 0) {
            return montoAportes.subtract(montoGarantias);
        } else {
            return BigDecimal.ZERO;
        }
    }

    public List<Empleado> getAllEmpleadosSobreNivel(int nivel, Establecimiento objEstablecimiento) {
        return DAOEmpleado.getInstance().getAllEmpleadosSobreNivel(nivel, objEstablecimiento);
    }

    public List<MovimientoAdministrativo> getAllMovimientosAdministrativosHabilitados(Date fechaDesde, Date fechaHasta, boolean ingreso) {
        return DAOMovimientoAdministrativo.getInstance().getAllMovimientosAdministrativosHabilitados(DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)), ingreso);
    }

    public List<MovimientoAdministrativo> getAllMovimientosAdministrativosPagados(Date fechaDesde, Date fechaHasta, boolean ingreso) {
        return DAOMovimientoAdministrativo.getInstance().getAllMovimientosAdministrativosPagados(DateUtil.getTimestamp(DateUtil.zeroTime(fechaDesde)), DateUtil.getTimestamp(DateUtil.lastTime(fechaHasta)), ingreso);
    }

    public SolicitudIngreso getSolicitudIngresoVigente(Socio objSocio){
        return DAOSolicitudIngreso.getInstance().getSolicitudIngresoVigente(objSocio);
    }
}
