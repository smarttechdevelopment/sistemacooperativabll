package com.smartech.sistemacooperativa.bll.facade;

import com.smartech.sistemacooperativa.dal.DAOMoneda;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import java.math.BigDecimal;

/**
 *
 * @author Smartech
 */
public class ConfigFacade {
    
    private ConfigFacade() {
    }

    public static String getConfig(String key) {
        String retorno = null;
        try {
            retorno = java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/bll/config/CustomConfig").getString(key);
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            if (retorno == null) {
                retorno = java.util.ResourceBundle.getBundle("com/smartech/sistemacooperativa/bll/config/DefaultConfig").getString(key);
            }
        }
        return retorno;
    }
    
    public static void autoConfig(){
        Tarjeta.MINUTOS_BLOQUEO_TARJETA = Integer.parseInt(ConfigFacade.getConfig("minutosBloqueoTarjeta"));
        Socio.MESES_LIMITE_INACTIVIDAD = Integer.parseInt(ConfigFacade.getConfig("mesesInactividad"));
        Socio.PERIODO_MESES_FONDO_MORTUORIO = Integer.parseInt(ConfigFacade.getConfig("periodomesesfondomortuorio"));
        Aporte.MONEDA_BASE = DAOMoneda.getInstance().getMonedaByAbreviatura(ConfigFacade.getConfig("monedaBase"));
        Aporte.PAGO_APORTE_SOLES = new BigDecimal(ConfigFacade.getConfig("montoAportes"));
        Aporte.PAGO_APORTE_FONDO_MORTUORIO_SOLES = new BigDecimal(ConfigFacade.getConfig("montofondomortuorio"));
    }
}
