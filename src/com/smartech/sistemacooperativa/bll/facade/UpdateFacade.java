package com.smartech.sistemacooperativa.bll.facade;

import com.smartech.sistemacooperativa.dal.DAOAporte;
import com.smartech.sistemacooperativa.dal.DAOAprobacion;
import com.smartech.sistemacooperativa.dal.DAOCaja;
import com.smartech.sistemacooperativa.dal.DAOCuenta;
import com.smartech.sistemacooperativa.dal.DAOCuota;
import com.smartech.sistemacooperativa.dal.DAODetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dal.DAODetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dal.DAODetalleSolicitudPrestamoSocio;
import com.smartech.sistemacooperativa.dal.DAODetalleSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dal.DAOEmpleado;
import com.smartech.sistemacooperativa.dal.DAOHabilitacionCuotas;
import com.smartech.sistemacooperativa.dal.DAOHistoricoCaja;
import com.smartech.sistemacooperativa.dal.DAOHuella;
import com.smartech.sistemacooperativa.dal.DAOMoneda;
import com.smartech.sistemacooperativa.dal.DAOMovimientoAdministrativo;
import com.smartech.sistemacooperativa.dal.DAOPrestamo;
import com.smartech.sistemacooperativa.dal.DAOSocio;
import com.smartech.sistemacooperativa.dal.DAOSolicitudIngreso;
import com.smartech.sistemacooperativa.dal.DAOSolicitudPrestamo;
import com.smartech.sistemacooperativa.dal.DAOSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dal.DAOTarjeta;
import com.smartech.sistemacooperativa.dal.DAOTasaInteres;
import com.smartech.sistemacooperativa.dal.DAOTasaInteresSolicitudPrestamo;
import com.smartech.sistemacooperativa.dal.DAOTipoInteres;
import com.smartech.sistemacooperativa.dal.DAOUsuario;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Aprobacion;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.DetalleSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.HistoricoCaja;
import com.smartech.sistemacooperativa.dominio.Huella;
import com.smartech.sistemacooperativa.dominio.Moneda;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.SolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TasaInteresSolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class UpdateFacade {

    private static UpdateFacade instance = null;

    private UpdateFacade() {
    }

    public static UpdateFacade getInstance() {
        if (instance == null) {
            instance = new UpdateFacade();
        }

        return instance;
    }

    public boolean updateUsuario(Usuario objUsuario) {
        boolean result = false;

        try {
            result = DAOUsuario.getInstance().update(objUsuario);
            if (result) {
                result = DAOHuella.getInstance().physicalDelete(objUsuario);
                if (result) {
                    for (Huella objHuella : objUsuario.getLstHuellas()) {
                        result = DAOHuella.getInstance().insert(objHuella, objUsuario);
                        if (!result) {
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateSocio(Socio objSocio) {
        boolean result = false;

        try {
            result = DAOSocio.getInstance().update(objSocio);
            if (result) {
                this.updateUsuario(objSocio.getObjUsuario());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo) {
        boolean result = false;

        try {
            result = DAOSolicitudPrestamo.getInstance().update(objSolicitudPrestamo);

            if (result) {
                result = DAODetalleSolicitudPrestamoSocio.getInstance().physicalDelete(objSolicitudPrestamo);
                if (result) {
                    result = InsertFacade.getInstance().insertDetalleSolicitudPrestamoSocio(objSolicitudPrestamo.getLstDetalleSolicitudPrestamoSocio());
                    if (result) {
                        for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitudPrestamo : objSolicitudPrestamo.getLstDetalleDocumentoSolicitud()) {
                            result = DAODetalleDocumentoSolicitud.getInstance().update(objDetalleDocumentoSolicitudPrestamo);
                            if (!result) {
                                return false;
                            }
                        }
                        if (result) {
                            for (Aprobacion objAprobacion : objSolicitudPrestamo.getLstAprobaciones()) {
                                result = DAOAprobacion.getInstance().update(objAprobacion);
                                if (!result) {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateSolicitudIngreso(SolicitudIngreso objSolicitudIngreso) {
        boolean result = false;

        try {
            result = DAOSolicitudIngreso.getInstance().update(objSolicitudIngreso);
            if (result) {
                result = DAODetalleFamiliarSocio.getInstance().physicalDelete(objSolicitudIngreso.getObjSocio());
                if (result) {
                    result = InsertFacade.getInstance().insertFamiliaresSocio(objSolicitudIngreso.getObjSocio().getLstFamiliaresSocio());
                    if (result) {
                        result = this.updateUsuario(objSolicitudIngreso.getObjSocio().getObjUsuario());
                        if (result) {
                            for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitudIngreso : objSolicitudIngreso.getLstDetalleDocumentoSolicitud()) {
                                result = DAODetalleDocumentoSolicitud.getInstance().update(objDetalleDocumentoSolicitudIngreso);
                                if (!result) {
                                    return false;
                                }
                            }
                            if (result) {
                                for (Aprobacion objAprobacion : objSolicitudIngreso.getLstAprobaciones()) {
                                    result = DAOAprobacion.getInstance().update(objAprobacion);
                                    if (!result) {
                                        return false;
                                    }
                                }
                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean discardAllDetalleDocumentoSolicitudIngreso(SolicitudIngreso objSolicitudIngreso) {
        boolean result = false;

        try {
            result = DAODetalleDocumentoSolicitud.getInstance().discardAllDetalleDocumentoSolicitud(objSolicitudIngreso);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateAporte(Aporte objAporte) {
        boolean result = false;

        try {
            result = DAOAporte.getInstance().update(objAporte);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateTarjeta(Tarjeta objTarjeta) {
        boolean result = false;

        try {
            result = DAOTarjeta.getInstance().update(objTarjeta);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateHistoricoCaja(HistoricoCaja objHistoricoCaja) {
        return DAOHistoricoCaja.getInstance().update(objHistoricoCaja);
    }

    public boolean update(Caja objCaja) {
        return DAOCaja.getInstance().update(objCaja);
    }

    public boolean updateMoneda(Moneda objMoneda) {
        boolean result = false;

        try {
            result = DAOMoneda.getInstance().update(objMoneda);
            if (result) {
                result = DAOMoneda.getInstance().insertTipoCambio(objMoneda);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateSolicituRetiroSocio(SolicitudRetiroSocio objSolicitudRetiroSocio) {
        boolean result = false;

        try {
            result = DAOSolicitudRetiroSocio.getInstance().update(objSolicitudRetiroSocio);
            for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud()) {
                result = DAODetalleDocumentoSolicitud.getInstance().update(objDetalleDocumentoSolicitud);
                if (!result) {
                    break;
                }
            }
//            for (Aprobacion objAprobacion : objSolicitudRetiroSocio.getLstAprobaciones()) {
//                result &= DAOAprobacion.getInstance().update(objAprobacion);
//            }
            if (result) {
                for (DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio : objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios()) {
                    result = DAODetalleSolicitudRetiroSocio.getInstance().update(objSolicitudRetiroSocio, objDetalleSolicitudRetiroSocio);
                    if (!result) {
                        break;
                    }
                }

//                if (result) {
//                    if (objSolicitudRetiroSocio.isAprobado()) {
//                        objSolicitudRetiroSocio.getObjSocio().setActivo(false);
//                        result = UpdateFacade.getInstance().updateSocio(objSolicitudRetiroSocio.getObjSocio());
//                    }
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateCuotas(List<Cuota> lstCuotas) {
        boolean result = false;

        try {
            for (Cuota objCuota : lstCuotas) {
                result = this.updateCuota(objCuota);
                if (!result) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateCuota(Cuota objCuota) {
        return DAOCuota.getInstance().update(objCuota);
    }

    public boolean updatePrestamo(Prestamo objPrestamo) {
        return DAOPrestamo.getInstance().update(objPrestamo);
    }

    public boolean updateEmpleado(Empleado objEmpleado) {
        boolean result = false;

        try {
            result = DAOEmpleado.getInstance().update(objEmpleado);
            if (result) {
                this.updateUsuario(objEmpleado.getObjUsuario());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateTasasInteres(List<TasaInteres> lstTasasInteres) {
        boolean result = false;

        try {
            if (lstTasasInteres.isEmpty()) {
                return true;
            }
            for (TasaInteres objTasaInteres : lstTasasInteres) {
                int oldTasaInteresId = objTasaInteres.getId();
                result = DAOTasaInteres.getInstance().update(objTasaInteres);
                if (result) {
                    if (result) {
                        result = DAOCuenta.getInstance().updateTasasInteres(oldTasaInteresId, objTasaInteres.getId());
                        if (result) {
                            result = DAOTasaInteresSolicitudPrestamo.getInstance().updateTasasIntereses(oldTasaInteresId, objTasaInteres.getId());
                            if (result) {
                                TasaInteres objOldTasaInteres = new TasaInteres(oldTasaInteresId, "", false, BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN, BigDecimal.ZERO, null, null, null);
                                result = DAOTasaInteres.getInstance().delete(objOldTasaInteres);
                                if (!result) {
                                    break;
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean updateTipoInteres(TipoInteres objTipoInteres) {
        return DAOTipoInteres.getInstance().update(objTipoInteres);
    }

    public boolean updateHabilitacionCuotas(HabilitacionCuotas objHabilitacionCuotas) {
        return DAOHabilitacionCuotas.getInstance().update(objHabilitacionCuotas);
    }

    public boolean updateMovimientoAdministrativo(MovimientoAdministrativo objMovimientoAdministrativo) {
        return DAOMovimientoAdministrativo.getInstance().update(objMovimientoAdministrativo);
    }
}
