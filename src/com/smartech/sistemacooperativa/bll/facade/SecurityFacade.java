package com.smartech.sistemacooperativa.bll.facade;

import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAOAccesoSistema;
import com.smartech.sistemacooperativa.dal.DAODepartamento;
import com.smartech.sistemacooperativa.dal.DAODistrito;
import com.smartech.sistemacooperativa.dal.DAOProvincia;
import com.smartech.sistemacooperativa.dal.DAOTarjeta;
import com.smartech.sistemacooperativa.dal.DAOUsuario;
import com.smartech.sistemacooperativa.dominio.AccesoSistema;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.Persona;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;

import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class SecurityFacade {

    private static SecurityFacade instance = null;
    public static final int MAX_INTENTOS_TARJETA = 3;

    private SecurityFacade() {
    }

    public static SecurityFacade getInstance() {
        if (instance == null) {
            instance = new SecurityFacade();
        }

        return instance;
    }

    public List<Usuario> verifyAccess(String username, String password) {
        List<Usuario> lstUsuarios = DAOUsuario.getInstance().getAllUsuarios(username, password);

        if (!lstUsuarios.isEmpty()) {
            DAODepartamento.getInstance().getAllDepartamentos();
            DAOProvincia.getInstance().getAllProvincias();
            DAODistrito.getInstance().getAllDistritos();
        }

        return lstUsuarios;
    }

    public boolean verifyCard(Tarjeta objTarjeta, String clave) {
        boolean result = DAOTarjeta.getInstance().verificar(objTarjeta, clave);

        if (!result) {
            objTarjeta.setIntentos(objTarjeta.getIntentos() + 1);
            if (objTarjeta.getIntentos() == MAX_INTENTOS_TARJETA) {
                objTarjeta.setFechaBloqueoIntentos(DateUtil.currentTimestamp());
            }

            DAOTarjeta.getInstance().update(objTarjeta);
        } else {
            objTarjeta.setIntentos(0);
            objTarjeta.setFechaBloqueoIntentos(null);

            DAOTarjeta.getInstance().update(objTarjeta);
        }

        return result;
    }

    public Persona verifyEstablecimiento(List<Persona> lstPersonas, Establecimiento objEstablecimiento) {
        if (!lstPersonas.isEmpty()) {
            if (lstPersonas.get(0) instanceof Empleado) {
                List<Empleado> lstEmpleados = new ArrayList<>((Collection) lstPersonas);
                for (Empleado objEmpleado : lstEmpleados) {
                    if (objEmpleado.getObjEstablecimiento().getId() == objEstablecimiento.getId()) {
                        return objEmpleado;
                    }
                }
            }else{
                return (Socio) lstPersonas.get(0);
            }
        }

        return null;
    }

    public boolean verifyActivo(List<Usuario> lstUsuarios) {
        for (Usuario objUsuario : lstUsuarios) {
            if (objUsuario.isActivo()) {
                return true;
            }
        }

        return false;
    }
    
    public boolean registrarAccesoSistema(Usuario objUsuario){
        boolean result = false;
        if(objUsuario.getObjTipoUsuario().getId() != TipoUsuario.TIPO_SOCIO && objUsuario.isActivo()){
            ConnectionManager.beginTransaction();
            try {
                result = DAOAccesoSistema.getInstance().insert(new AccesoSistema(0, DateUtil.currentTimestamp(), objUsuario));
            } catch (Exception e) {
                result = false;
                e.printStackTrace();
            }

            if(result){
                ConnectionManager.commitTransaction();
            }else{
                ConnectionManager.rollbackTransaction();
            }
        }
        return result;
    }
}
