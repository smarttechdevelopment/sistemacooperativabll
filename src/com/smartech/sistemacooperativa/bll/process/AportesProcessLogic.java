package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.DeleteFacade;
import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Socio;
import java.sql.Timestamp;

/**
 *
 * @author Smartech
 */
public class AportesProcessLogic {

    private static AportesProcessLogic instance = null;

    private AportesProcessLogic() {
    }

    public static AportesProcessLogic getInstance() {
        if (instance == null) {
            instance = new AportesProcessLogic();
        }

        return instance;
    }

    public boolean registrarAporte(Aporte objAporte) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            if (objAporte.getObjTarjeta().getObjSocio().isEstado()) {
                result = InsertFacade.getInstance().insertAporte(objAporte);
            }
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }

    public boolean eliminarAporte(Aporte objAporte) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            result = DeleteFacade.getInstance().deleteAporte(objAporte);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }
    
    public Timestamp getFechaUltimoAporte(Socio objSocio) {
        Aporte objAporte = QueryFacade.getInstance().getLastAporte(objSocio);
        if (objAporte.isPagado()) {
            return objAporte.getFechaRegistro();
        }
        return null;
    }
}
