/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAOAporte;
import com.smartech.sistemacooperativa.dal.DAOCuenta;
import com.smartech.sistemacooperativa.dominio.AccesoSistema;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class CuentasProcessLogic {

    private static CuentasProcessLogic instance = null;

    private Map<Socio, Boolean> mapAprobacionSocioAnulacionCuenta = new HashMap<>();

    private CuentasProcessLogic() {
    }

    public static CuentasProcessLogic getInstance() {
        if (instance == null) {
            instance = new CuentasProcessLogic();
        }

        return instance;
    }

    public Map<Socio, Boolean> getMapAprobacionSocioAnulacionCuenta() {
        return mapAprobacionSocioAnulacionCuenta;
    }

    public String generarCuenta(Empleado objEmpleado, TasaInteres objTasaInteres, TipoCuenta objTipoCuenta, List<Socio> lstSocios, Usuario objUsuario) {
        String message = "";
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            Cuenta objCuenta = new Cuenta(
                    0,
                    this.generarCodigoCuenta(),
                    BigDecimal.ZERO,
                    DateUtil.currentTimestamp(),
                    null,
                    true,
                    true,
                    objEmpleado,
                    objTasaInteres,
                    objTipoCuenta,
                    objUsuario,
                    lstSocios,
                    false,
                    objTipoCuenta.getId() == TipoCuenta.TIPO_FONDO_MORTUORIO
            );

            if (objTipoCuenta.isMancomunada()) {
                if (lstSocios.size() > 1) {

                    List<Cuenta> lstCuentas = QueryFacade.getInstance().getCuentasSocios(objTipoCuenta, lstSocios);

                    result = lstCuentas.size() <= 0;
                    if (result) {
                        result = InsertFacade.getInstance().insertCuentaSocios(objCuenta);
                    } else {
                        message = "Error: No es posible agregar una cuenta de este tipo.";
                    }
                } else {
                    message = "Error: Seleccione mínimo 2 socios";
                }
            } else {
                System.out.println("lstsocios.size: " + lstSocios.size());
                if (lstSocios.size() == 1) {
                    // verificar cuentas del socio
                    result = true;
                    if (lstSocios.get(0).getLstCuentas().size() > 0) {
                        for (Cuenta objCuentaSocio : lstSocios.get(0).getLstCuentas()) {
                            if (objCuentaSocio.isEstado() || objCuentaSocio.isActivo()) {
                                if (objCuentaSocio.getObjTipoCuenta().getId() == objTipoCuenta.getId()) {
                                    result = false;
                                    break;
                                }
                            }
                        }
                    }

                    if (result) {
                        result = InsertFacade.getInstance().insertCuentaSocios(objCuenta);
                    } else {
                        message = "Error: No es posible agregar una cuenta de este tipo.";
                    }

                } else {
                    message = "Error: Seleccione socio";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            message = "Cuenta registrada correctamente.";
            ConnectionManager.commitTransaction();
        } else {
            if (message.isEmpty()) {
                message = "Error en el registro";
            }
            ConnectionManager.rollbackTransaction();
        }

        return message;
    }

    public String actualizarInteresesCuentas(Usuario objUsuario) {
        String message = "";
        boolean result = false;
        Calendar ultimoAccesoCalendar;

        ConnectionManager.beginTransaction();

        try {
//            if (objUsuario.getObjTipoUsuario().getId() == TipoUsuario.TIPO_ADMINISTRADOR || objUsuario.getObjTipoUsuario().getId() == TipoUsuario.TIPO_PRESIDENTE_CONSEJO_ADMINISTRACION) {
//                AccesoSistema objAccesoSistema = QueryFacade.getInstance().getLastAccesoSistema(objUsuario);
//            } else {
//
//            }
            if (objUsuario.getObjTipoUsuario().getId() != TipoUsuario.TIPO_SOCIO) {
                AccesoSistema objAccesoSistema = QueryFacade.getInstance().getLastAccesoSistema();
                if (objAccesoSistema != null) {
                    ultimoAccesoCalendar = DateUtil.getCalendar(objAccesoSistema.getFecha());
                    int mesesSinAcceso = DateUtil.getMonthsDifference(objAccesoSistema.getFecha(), DateUtil.currentTimestamp());
                    if (mesesSinAcceso > 0) {
                        for (int i = 0; i < mesesSinAcceso; i++) {
                            ultimoAccesoCalendar.add(Calendar.MONTH, 1);
                            ultimoAccesoCalendar.set(Calendar.DAY_OF_MONTH, 1);
                            result = DAOCuenta.getInstance().updateIntereses(DateUtil.getTimestamp(ultimoAccesoCalendar));
                            if (!result) {
                                break;
                            }
                        }
                    }
                } else {
                    ultimoAccesoCalendar = DateUtil.getCalendar(DateUtil.currentTimestamp());
                    ultimoAccesoCalendar.set(Calendar.DAY_OF_MONTH, 1);
                    result = DAOCuenta.getInstance().updateIntereses(DateUtil.getTimestamp(ultimoAccesoCalendar));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return message;
    }

    public String generarCodigoCuenta() {
        return StringUtil.getStringCode(Long.parseLong(QueryFacade.getInstance().getLastCodigoCuenta()) + 1, Cuenta.DIGITOS_CODIGO);
    }

    public boolean verificarSociosCuenta(Cuenta objCuenta) {
        objCuenta.setLstSocios(QueryFacade.getInstance().getAllSocios(objCuenta, false));
        boolean result = false;
        if (objCuenta.getObjTipoCuenta().verificacionUnanime()) {
            for (Socio objSocio : objCuenta.getLstSocios()) {
                result = objSocio.isEstado() && objSocio.isActivo();
                if (!result) {
                    break;
                } else if (!RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(objSocio).equals(Socio.ESTADO_ACTIVIDAD_ACTIVO)) {
                    result = false;
                    break;
                }
            }
        } else {
            for (Socio objSocio : objCuenta.getLstSocios()) {
                result = objSocio.isEstado() && objSocio.isActivo();
                if (result) {
                    if (RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(objSocio).equals(Socio.ESTADO_ACTIVIDAD_ACTIVO)) {
                        break;
                    }
                }
            }
        }
        return result;
    }

    public boolean sociosVerificados(Cuenta objCuenta) {
        boolean result = false;
        if (objCuenta.getObjTipoCuenta().verificacionUnanime()) {
            for (Socio objSocio : objCuenta.getLstSocios()) {
                result = objSocio.isEstado() && objSocio.isActivo();
                if (!result) {
                    break;
                }
            }
        } else {
            for (Socio objSocio : objCuenta.getLstSocios()) {
                result = objSocio.isEstado() && objSocio.isActivo();
                if (result) {
                    break;
                }
            }
        }
        return result;
    }

    public boolean comprobarEstadoActividadSocio(Socio objSocio) {
        boolean result;
        String estadoActividad;
        result = objSocio.isEstado() && objSocio.isActivo();
        if (result) {
            estadoActividad = RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(objSocio);
            result = estadoActividad.equals(Socio.ESTADO_ACTIVIDAD_ACTIVO);
        }
        return result;
    }

    public void updateSociosCuenta(Cuenta objCuenta) {
        if (objCuenta.getObjTipoCuenta().isMancomunada()) {
            for (Socio objSocio : objCuenta.getLstSocios()) {
                //Si esta inactivo o pasivo o estado = false
                if (!objSocio.isEstado() || !objSocio.isActivo() || RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(objSocio).equals(Socio.ESTADO_ACTIVIDAD_PASIVO)) {
                    objSocio.setActivo(false);
                }
            }
        }
    }

    public void generarAprobacionSocios(Cuenta objCuenta) {
        for (Socio objSocio : objCuenta.getLstSocios()) {
            this.mapAprobacionSocioAnulacionCuenta.put(objSocio, Boolean.FALSE);
        }
    }

    public void updateAprobacionSocioAnulacionCuenta(Socio objSocio, boolean aprobacion) {
        for (Map.Entry<Socio, Boolean> entry : this.mapAprobacionSocioAnulacionCuenta.entrySet()) {
            Socio key = entry.getKey();
            if (key.getId() == objSocio.getId()) {
                entry.setValue(aprobacion);
                return;
            }

        }
    }

    public boolean aprobacionAnulacionCuentaCompletada() {
        for (Map.Entry<Socio, Boolean> entry : this.mapAprobacionSocioAnulacionCuenta.entrySet()) {
            Socio key = entry.getKey();
            Boolean value = entry.getValue();
            if (!value) {
                return false;
            }
        }
        return true;
    }

    public boolean anularCuenta(Cuenta objCuenta) {
        boolean result = false;

        objCuenta = DAOCuenta.getInstance().getCuenta(objCuenta.getId());
        if (objCuenta.getSaldo().compareTo(BigDecimal.ZERO) == 0) {
            objCuenta.setActivo(false);
            objCuenta.setEstado(false);
            objCuenta.setFechamodificacion(DateUtil.currentTimestamp());
            result = DAOCuenta.getInstance().update(objCuenta);
        }

        return result;
    }

    public Cuenta getCuentaFondoMortuorio(Socio objSocio) {
        for (Cuenta objCuenta : objSocio.getLstCuentas()) {
            if (objCuenta.getObjTipoCuenta().getId() == TipoCuenta.TIPO_FONDO_MORTUORIO) {
                if (objCuenta.isEstado() && objCuenta.isActivo()) {
                    if (objCuenta.isBloqueadoSalida()) {
                        return objCuenta;
                    }
                }
            }
        }
        return null;
    }
}
