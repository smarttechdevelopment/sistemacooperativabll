/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.DeleteFacade;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAORetiro;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Moneda;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Permiso;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class RetirosProcessLogic {

    private Permiso objCurrentPermiso;
    private static final List<Moneda> lstMonedasAceptadas = new ArrayList<>();
    private Map<Socio, Boolean> mapSocios = new HashMap<>();
    private Cuenta objCuentaSeleccionada = null;

    public Permiso getObjCurrentPermiso() {
        return objCurrentPermiso;
    }

    public void setObjCurrentPermiso(Permiso objCurrentPermiso) {
        this.objCurrentPermiso = objCurrentPermiso;
    }

    public Map<Socio, Boolean> getMapSocios() {
        return mapSocios;
    }

    public Cuenta getObjCuentaSeleccionada() {
        return objCuentaSeleccionada;
    }

    public static List<Moneda> getLstMonedasAceptadas() {
        return lstMonedasAceptadas;
    }

    private static RetirosProcessLogic instance = null;

    private RetirosProcessLogic() {
    }

    public static RetirosProcessLogic getInstance() {

        if (instance == null) {
            instance = new RetirosProcessLogic();
            RetirosProcessLogic.lstMonedasAceptadas.add(QueryFacade.getInstance().getMonedaByAbreviatura("PEN"));
            Retiro.objMonedaBase = QueryFacade.getInstance().getMonedaBase();
        }
        return instance;
    }

    public void generarSocios(Cuenta objCuenta) {
        this.objCuentaSeleccionada = objCuenta;
        this.mapSocios.clear();
        for (Socio objSocio : this.getObjCuentaSeleccionada().getLstSocios()) {
            this.mapSocios.put(objSocio, Boolean.FALSE);
        }
    }

    public void actualizarAprobacionSocio(Socio objSocio, boolean aprobado) {
        for (Map.Entry<Socio, Boolean> entry : this.mapSocios.entrySet()) {
            Socio key = entry.getKey();
            //Boolean value = entry.getValue();
            if (key.getId() == objSocio.getId()) {
                entry.setValue(aprobado);
                break;
            }
            System.out.println("Socio: " + key.getId());
        }
    }

    public void cancelarAprobacionSocios() {
        for (Map.Entry<Socio, Boolean> entry : this.mapSocios.entrySet()) {
            entry.setValue(Boolean.FALSE);
        }
    }

    public boolean verificarAprobacionSocio(Socio objSocio) {
        boolean result = false;

        if (CuentasProcessLogic.getInstance().comprobarEstadoActividadSocio(objSocio)) {
            for (Map.Entry<Socio, Boolean> entry : this.mapSocios.entrySet()) {
                Boolean value = entry.getValue();
                if (entry.getKey().getId() == objSocio.getId()) {
                    return value;
                }
            }
        }

        return result;
    }

    public boolean verificarAprobacionSocios() {
        boolean result = false;

        if (this.objCuentaSeleccionada != null) {
            for (Map.Entry<Socio, Boolean> entry : this.mapSocios.entrySet()) {
                Boolean value = entry.getValue();
                if (this.objCuentaSeleccionada.getObjTipoCuenta().verificacionUnanime()) {
                    if (entry.getKey().isEstado() && entry.getKey().isActivo()) {
                        if (RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(entry.getKey()).equals(Socio.ESTADO_ACTIVIDAD_ACTIVO)) {
                            result = value;
                            if (!result) {
                                break;
                            }
                        }
                    }
                } else if (value) {
                    if (entry.getKey().isEstado() && entry.getKey().isActivo()) {
                        if (RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(entry.getKey()).equals(Socio.ESTADO_ACTIVIDAD_ACTIVO)) {
                            result = value;
                            break;
                        }
                    }
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    public boolean permiteRealizarPagoRetiro(Usuario objUsuarioOperador, Retiro objRetiro) {

        boolean result;
        //Retiro.establecerMontoAdvertencia(objUsuarioOperador);

        Retiro objRetiroComprobar = DAORetiro.getInstance().get(objRetiro.getId());
        result = objRetiroComprobar.isEstado() && objRetiroComprobar.isHabilitado() && !objRetiroComprobar.isPagado();

        if (result) {
            if (objRetiroComprobar.getObjCuenta().isEstado() && objRetiroComprobar.getObjCuenta().isActivo()) {
                if (objRetiro.verificarMontoRetiroYPagos()) { //&& objRetiro.evaluarMontoRetiroYCuenta()
                    for (Pago objPago : objRetiro.getLstPagos()) {
                        //result &= !Retiro.advertirMontoRetiro(objPago.getMonto(), objPago.getObjMoneda());
                        for (Moneda objMoneda : RetirosProcessLogic.lstMonedasAceptadas) {
                            result = objMoneda.equalsTo(objPago.getObjMoneda());
                            if(!result){
                                break;
                            }
                        }
                    }
                }
            }
        }

        return result;

    }

    public boolean permiteRealizarRetiro(Retiro objRetiro, Usuario objUsuarioOperador) {

        return !Retiro.advertirMontoRetiro(objRetiro.getMonto(), objRetiro.getObjCuenta().getObjTipoCuenta().getObjMoneda(), objUsuarioOperador.getObjTipoUsuario());

    }

    public boolean eliminarRetiro(Retiro objRetiro, Empleado objEmpleado) {
        boolean result = false;
        this.setObjCurrentPermiso(QueryFacade.getInstance().getPermiso(Permiso.PERMISO_RETIROS_ID));
        if (objEmpleado.getObjUsuario().getObjTipoUsuario().verificarPermiso(this.getObjCurrentPermiso().getNombre(), Permiso.NIVEL_ELIMINACION)) {
            result = DAORetiro.getInstance().logicalDeleteRetiro(objRetiro);
        }
        return result;
    }

    public String registrarRetiro(Retiro objRetiro, Empleado objEmpleado) {

        String mensaje = "";
        boolean result = false, permiteRegistrarRetiro;

        ConnectionManager.beginTransaction();
        try {
            this.setObjCurrentPermiso(QueryFacade.getInstance().getPermiso(Permiso.PERMISO_RETIROS_ID));

            if (CuentasProcessLogic.getInstance().verificarSociosCuenta(objRetiro.getObjCuenta())) {
                if (objRetiro.getObjCuenta().isEstado()) {
                    if (objRetiro.getObjCuenta().isActivo()) {
                        if (objRetiro.getObjCuenta().getSaldo().compareTo(objRetiro.getMonto()) >= 0) {
                            if (objRetiro.isHabilitado()) {
                                if (!objRetiro.isPagado()) {
                                    if (objRetiro.evaluarMontoRetiroYCuenta() && !Retiro.advertirMontoRetiro(objRetiro.getMonto(), objRetiro.getObjCuenta().getObjTipoCuenta().getObjMoneda(), objRetiro.getObjUsuario().getObjTipoUsuario())) {

                                        permiteRegistrarRetiro = true;
                                        //                                for(Pago objPago : objRetiro.getLstPagos()){
                                        //
                                        //                                    permiteRegistrarRetiro &= this.permiteRealizarRetiro(objEmpleado.getObjUsuario(), objPago);
                                        //                                    for(Moneda objMoneda : RetirosProcessLogic.lstMonedasAceptadas){
                                        //                                        permiteRegistrarRetiro &= objMoneda.equalsTo(objPago.getObjMoneda());
                                        //                                    }
                                        //                                }
                                        // 1. Verificar permiso de usuario para 
                                        // Nivel minimo: ESCRITURA
                                        result = permiteRegistrarRetiro && objEmpleado.getObjUsuario().getObjTipoUsuario().verificarPermiso(this.getObjCurrentPermiso().getNombre(), Permiso.NIVEL_ESCRITURA);

                                        if (result) {

                                            // 2. Verificar acreedor ?
                                            //                            objDeposito.getAcreedor();
                                            // 3. Verificar TIPO PAGO ?
                                            //                            lstPagos = objDeposito.getLstPagos();
                                            //                            for(Pago objPago : lstPagos) {
                                            //                                objPago.getTipopago();
                                            //                            }
                                            // 4. Registrar Retiro
                                            result = DAORetiro.getInstance().insert(objRetiro);

                                            if (result) {
                                                mensaje = "Retiro registrado correctamente";
                                                this.cancelarAprobacionSocios();
                                            } else {
                                                mensaje = "Error en registro";
                                            }

                                        } else {
                                            mensaje = "No tiene permiso para registrar Retiro";
                                        }
                                    } else {
                                        mensaje = "No es posible registrar ese monto";
                                    }
                                } else {
                                    mensaje = "No es posible registrar";
                                }
                            } else {
                                mensaje = "No está habilitado para realizar el retiro";
                            }
                        } else {
                            mensaje = "No cuenta con saldo suficiente";
                        }
                    } else {
                        mensaje = "La cuenta no está habilitada";
                    }
                } else {
                    mensaje = "La cuenta ya no existe";
                }
            }

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return mensaje;
    }
}
