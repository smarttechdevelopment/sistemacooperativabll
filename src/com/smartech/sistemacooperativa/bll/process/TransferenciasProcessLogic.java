/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAOTransaccion;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoPago;
import com.smartech.sistemacooperativa.dominio.TransferenciaCuenta;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class TransferenciasProcessLogic {
    
    private static TransferenciasProcessLogic instance = null;

    private TransferenciasProcessLogic() {
    }
    
    public static TransferenciasProcessLogic getInstance(){
        if(instance == null){
            instance = new TransferenciasProcessLogic();
        }
        return instance;
    }
    
    private Map<Socio, Boolean> mapSocios = new HashMap<>();

    public Map<Socio, Boolean> getMapSocios() {
        return mapSocios;
    }
    
    public void generarSocios(Cuenta objCuenta){
        this.mapSocios.clear();
        for(Socio objSocio : objCuenta.getLstSocios()){
            this.mapSocios.put(objSocio, Boolean.FALSE);
        }
    }
    
    public void actualizarAprobacionSocio(Socio objSocio, boolean aprobado){
        for (Map.Entry<Socio, Boolean> entry : this.mapSocios.entrySet()) {
            Socio key = entry.getKey();
            //Boolean value = entry.getValue();
            if(key.getId() == objSocio.getId()){
                entry.setValue(aprobado);
            }

        }
    }
    
    public void cancelarAprobacionSocios(){
        for (Map.Entry<Socio, Boolean> entry : this.mapSocios.entrySet()) {
            entry.setValue(Boolean.FALSE);
        }
    }
    
    public boolean verificarCuentas(BigDecimal monto, Cuenta objCuentaOrigen, Cuenta objCuentaDestino){
        boolean result;
        result = objCuentaOrigen.getObjTipoCuenta().getObjMoneda().equalsTo(objCuentaDestino.getObjTipoCuenta().getObjMoneda());
        if(result){
            result = objCuentaOrigen.isEstado() && objCuentaOrigen.isActivo();
            if(result){
                result = objCuentaDestino.isEstado() && objCuentaDestino.isActivo();
                if(result){
                    result = objCuentaOrigen.getSaldo().compareTo(monto) >= 0;
                    if(result){
                        result = objCuentaOrigen.getId() != objCuentaDestino.getId();
                    }
                }
            }
        }
        return result;
    }
    
    public boolean verificarAprobacionSocios(Cuenta objCuentaOrigen){
        boolean result = false;
        for (Map.Entry<Socio, Boolean> entry : this.mapSocios.entrySet()) {
                Boolean value = entry.getValue();
            if (objCuentaOrigen.getObjTipoCuenta().verificacionUnanime()) {
                if (entry.getKey().isEstado() && entry.getKey().isActivo()) {
                    if (RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(entry.getKey()).equals(Socio.ESTADO_ACTIVIDAD_ACTIVO)) {
                        result = value;
                        if (!result) {
                            break;
                        }
                    }
                }
            } else if (value) {
                if (entry.getKey().isEstado() && entry.getKey().isActivo()) {
                    if (RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(entry.getKey()).equals(Socio.ESTADO_ACTIVIDAD_ACTIVO)) {
                        result = true;
                        break;
                    }
                }
            }
        }
        return result;
    }
    
    public String registrarTransferenciaCuentas(BigDecimal monto, Cuenta objCuentaOrigen, Cuenta objCuentaDestino, Usuario objUsuario, Caja objCaja, Empleado objEmpleado){
        String mensaje = "Error";
        boolean result = false;
        
        if(objCaja != null && objCaja.getMonto().compareTo(monto) >= 0){
            ConnectionManager.beginTransaction();
            try {
                if(this.verificarCuentas(monto, objCuentaOrigen, objCuentaDestino)){
                    if(this.verificarAprobacionSocios(objCuentaOrigen)){
                        TransferenciaCuenta objTransferencia = new TransferenciaCuenta(0, DateUtil.currentTimestamp(), monto, result, DateUtil.currentTimestamp(), objCuentaOrigen.getObjTipoCuenta().getObjMoneda(), objUsuario, objCuentaOrigen, objCuentaDestino);
                        result = DAOTransaccion.getInstance().insert(objTransferencia);
                        if(result){
                            TipoPago objTipoPago = QueryFacade.getInstance().getTipoPago(1);
                            mensaje = PagosProcessLogic.getInstance().generarPago(objTransferencia, objTipoPago, objUsuario, objCaja, objEmpleado, objCaja.getObjEstablecimiento());
                            mensaje += ". Transferencia exitosa";
                        }else{
                            mensaje = "Error en registro de transferencia";
                        }
                    }else{
                        mensaje = "Error en aprobación de socios";
                    }
                }else{
                    mensaje = "Error en verificación de cuentas";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(result){
                ConnectionManager.commitTransaction();
            }else{
                ConnectionManager.rollbackTransaction();
            }            
        }else{
            mensaje = "Error en registro de montos en caja";
        }
        
        return mensaje;
    }
}
