package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Moneda;

/**
 *
 * @author Smartech
 */
public class ConfiguracionGeneralProcessLogic {

    private static ConfiguracionGeneralProcessLogic instance = null;

    private ConfiguracionGeneralProcessLogic() {
    }

    public static ConfiguracionGeneralProcessLogic getInstance() {
        if (instance == null) {
            instance = new ConfiguracionGeneralProcessLogic();
        }

        return instance;
    }

    public boolean registrarMoneda(Moneda objMoneda) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            result = InsertFacade.getInstance().insertMoneda(objMoneda);
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }

    public boolean actualizarMoneda(Moneda objMoneda) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            result = UpdateFacade.getInstance().updateMoneda(objMoneda);
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }
}
