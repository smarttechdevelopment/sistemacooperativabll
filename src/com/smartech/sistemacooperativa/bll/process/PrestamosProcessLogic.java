package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.DeleteFacade;
import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Aprobacion;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.DetalleSolicitudPrestamoSocio;
import com.smartech.sistemacooperativa.dominio.Documento;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TasaInteresSolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.TipoPago;
import com.smartech.sistemacooperativa.dominio.TipoPrestamo;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.FileUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.table.TableModel;

/**
 *
 * @author Smartech
 */
public class PrestamosProcessLogic {

    private static PrestamosProcessLogic instance = null;

    private PrestamosProcessLogic() {
    }

    public static PrestamosProcessLogic getInstance() {
        if (instance == null) {
            instance = new PrestamosProcessLogic();
        }

        return instance;
    }

    public Prestamo generarPrestamo(TasaInteres objTasaInteres, BigDecimal monto, int cuotas, Date fechaCalculo) {
        Prestamo objPrestamo = null;

        try {
            objPrestamo = new Prestamo(
                    0,
                    cuotas,
                    monto,
                    null,
                    null,
                    true,
                    false,
                    null,
                    false);
            objPrestamo.setLstCuotas(objPrestamo.generarCuotas(Prestamo.METODO_ALEMAN, objTasaInteres, fechaCalculo));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objPrestamo;
    }

    public Prestamo generarPrestamo(SolicitudPrestamo objSolicitudPrestamo) {
        Prestamo objPrestamo = null;

        try {
            objPrestamo = new Prestamo(
                    0,
                    objSolicitudPrestamo.getCuotas(),
                    objSolicitudPrestamo.getMontoAprobado(),
                    DateUtil.currentTimestamp(),
                    null,
                    true,
                    false,
                    objSolicitudPrestamo,
                    true);

            List<Cuota> lstCuotas = objPrestamo.generarCuotas(Prestamo.METODO_ALEMAN);
            boolean result = InsertFacade.getInstance().insertPrestamo(objPrestamo);

            if (result) {
                result = InsertFacade.getInstance().insertCuotas(lstCuotas);
                if (result) {
                    objPrestamo.setLstCuotas(lstCuotas);
                    objSolicitudPrestamo.setAprobado(true);
                    result = UpdateFacade.getInstance().updateSolicitudPrestamo(objSolicitudPrestamo);
                    if (!result) {
                        objPrestamo = null;
                    }
                } else {
                    objPrestamo = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objPrestamo;
    }

    public String eliminarSolicitud(SolicitudPrestamo objSolicitudPrestamo) {
        String message = "";
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            result = DeleteFacade.getInstance().deleteSolicitud(objSolicitudPrestamo);
            if (result) {
                message = "Solicitud eliminada correctamente.";
            } else {
                message = "Error en la eliminacion.";
            }
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return message;
    }

    public Map<String, Prestamo> actualizarSolicitud(SolicitudPrestamo objSolicitudPrestamo, int cuotas, String motivo, String motivoDesaprobacion, BigDecimal otrosIngresos, BigDecimal nivelGastos, BigDecimal montoDisponible, BigDecimal montoSolicitado, BigDecimal montoAprobado, Usuario objUsuario, TipoSolicitud objTipoSolicitud, TasaInteres objTasaInteres, TipoPrestamo objTipoPrestamo, List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio, List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud, List<Aprobacion> lstAprobaciones, Map<Documento, File> mapArchivos) {
        String message = "";
        Prestamo objPrestamo = null;
        boolean result = false;

        ConnectionManager.beginTransaction();
        FileUtil.connect();
        try {
            if (montoDisponible.compareTo(montoSolicitado) >= 0) {
                objSolicitudPrestamo.setOtrosIngresos(otrosIngresos);
                objSolicitudPrestamo.setNivelGastos(nivelGastos);
                objSolicitudPrestamo.setCuotas(cuotas);
                objSolicitudPrestamo.setMotivo(motivo);
                objSolicitudPrestamo.setMotivoDesaprobacion(motivoDesaprobacion);
                objSolicitudPrestamo.setMontoSolicitado(montoSolicitado);
                objSolicitudPrestamo.setMontoAprobado(montoAprobado);
                objSolicitudPrestamo.setFechaModificacion(DateUtil.currentTimestamp());
                objSolicitudPrestamo.setObjUsuario(objUsuario);
                objSolicitudPrestamo.setObjTipoSolicitud(objTipoSolicitud);
                objSolicitudPrestamo.setObjTipoPrestamo(objTipoPrestamo);
                objSolicitudPrestamo.setObjTasaInteres(objTasaInteres);
                objSolicitudPrestamo.setLstDetalleDocumentoSolicitud(lstDetalleDocumentoSolicitud);
                objSolicitudPrestamo.setLstDetalleSolicitudPrestamoSocio(lstDetalleSolicitudPrestamoSocio);
                objSolicitudPrestamo.setLstAprobaciones(lstAprobaciones);

                String destinationPath = "/SolicitudesPrestamo/" + objSolicitudPrestamo.getObjSocioPrestatario().getDocumentoIdentidad() + "/" + objSolicitudPrestamo.getCodigo();
                objSolicitudPrestamo.updateRutas(mapArchivos, destinationPath);
                result = UpdateFacade.getInstance().updateSolicitudPrestamo(objSolicitudPrestamo);

                if (result) {
                    message = "Solicitud actualizada correctamente.";
                    objSolicitudPrestamo.updateStatus();
                    result = FileUtil.createDirectoryFTP(destinationPath);
                    if (result) {
                        List<File> lstArchivosDocumentos = new ArrayList<>(mapArchivos.values());
                        result = FileUtil.saveFilesFTP(lstArchivosDocumentos, destinationPath);
                        if (result) {
                            if (objSolicitudPrestamo.isAprobado()) {
                                objPrestamo = this.generarPrestamo(objSolicitudPrestamo);
                                if (objPrestamo != null) {
                                    message = message.concat(" Prestamo generado correctamente.");
                                } else {
                                    result = false;
                                    message = "Error en el registro.";
                                }
                            } else {
                                objPrestamo = this.generarPrestamo(objTasaInteres, montoSolicitado, cuotas, new Date());
                            }
                            if (objPrestamo != null) {
                                objPrestamo.setObjSolicitud(objSolicitudPrestamo);
                            }
                        } else {
                            message = "Error en el registro.";
                        }
                    } else {
                        message = "Error en el registro.";
                    }
                }
            } else {
                message = "Error: El monto solicitado excede al monto disponible.";
            }
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }
        FileUtil.disconnect();

        Map map = new HashMap<>();
        map.put(message, objPrestamo);

        return map;
    }

    public Map<String, Prestamo> generarSolicitud(String codigo, int cuotas, String motivo, BigDecimal otrosIngresos, BigDecimal nivelGastos, BigDecimal montoDisponible, BigDecimal montoSolicitado, BigDecimal montoAprobado, Socio objSocio, Usuario objUsuario, TipoSolicitud objTipoSolicitud, TasaInteres objTasaInteres, TipoPrestamo objTipoPrestamo, List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio, List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud, List<Aprobacion> lstAprobaciones, Map<Documento, File> mapArchivos) {
        String message = "";
        Prestamo objPrestamo = null;
        boolean result = false;

        ConnectionManager.beginTransaction();
        FileUtil.connect();
        try {
            SolicitudPrestamo objSolicitudPrestamoExistente = QueryFacade.getInstance().getSolicitudPrestamoVigente(objSocio);
            if (objSolicitudPrestamoExistente == null) {
                if (montoDisponible.compareTo(montoSolicitado) >= 0) {
                    SolicitudPrestamo objSolicitudPrestamo = new SolicitudPrestamo(
                            0,
                            codigo,
                            otrosIngresos,
                            nivelGastos,
                            cuotas,
                            motivo,
                            "",
                            montoSolicitado,
                            montoAprobado,
                            DateUtil.add(DateUtil.currentTimestamp(), Calendar.DAY_OF_MONTH, 30),
                            DateUtil.currentTimestamp(),
                            null,
                            false,
                            true,
                            objUsuario,
                            objTipoSolicitud,
                            objTasaInteres,
                            objTipoPrestamo,
                            lstDetalleSolicitudPrestamoSocio,
                            lstDetalleDocumentoSolicitud,
                            lstAprobaciones,
                            new ArrayList<>(),
                            true,
                            false);

                    String destinationPath = "/SolicitudesPrestamo/" + objSocio.getDocumentoIdentidad() + "/" + objSolicitudPrestamo.getCodigo();
                    objSolicitudPrestamo.updateRutas(mapArchivos, destinationPath);
                    result = InsertFacade.getInstance().insertSolicitudPrestamo(objSolicitudPrestamo);

                    TasaInteresSolicitudPrestamo objTasaInteresSolicitudPrestamo = new TasaInteresSolicitudPrestamo(
                            0,
                            DateUtil.currentTimestamp(),
                            null,
                            true,
                            objSolicitudPrestamo,
                            objTasaInteres);

                    if (result) {
                        result = InsertFacade.getInstance().insertTasaInteresSolicitudPrestamo(objTasaInteresSolicitudPrestamo);
                    }

                    if (result) {
                        message = "Solicitud registrada.";
                        objSolicitudPrestamo.updateStatus();
                        result = FileUtil.createDirectoryFTP(destinationPath);
                        if (result) {
                            List<File> lstArchivosDocumentos = new ArrayList<>(mapArchivos.values());
                            result = FileUtil.saveFilesFTP(lstArchivosDocumentos, destinationPath);
                            if (result) {
                                if (result) {
                                    if (objSolicitudPrestamo.isAprobado()) {
                                        objPrestamo = this.generarPrestamo(objSolicitudPrestamo);
                                        if (objPrestamo != null) {
                                            message = message.concat(" Prestamo generado correctamente.");
                                        } else {
                                            message = "Error en el registro.";
                                        }
                                    } else {
                                        objPrestamo = this.generarPrestamo(objTasaInteres, montoSolicitado, cuotas, new Date());
                                    }
                                    if (objPrestamo != null) {
                                        objPrestamo.setObjSolicitud(objSolicitudPrestamo);
                                    }
                                } else {
                                    message = "Error en el registro.";
                                }
                            } else {
                                message = "Error en el registro.";
                            }
                        } else {
                            message = "Error en el registro.";
                        }
                    } else {
                        message = "Error en el registro.";
                    }
                } else {
                    message = "Error: El monto solicitado excede al monto disponible.";
                }
            } else {
                message = "Error: Ya existe una Solicitud o un Prestamo Activo vigente.";
            }

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }
        FileUtil.disconnect();

        Map map = new HashMap<>();
        map.put(message, objPrestamo);

        return map;
    }

    public BigDecimal getMontoTotalPago(List<Cuota> lstCuotas) {
        BigDecimal montoTotal = new BigDecimal(BigInteger.ZERO);

        for (Cuota objCuota : lstCuotas) {
            montoTotal = montoTotal.add(objCuota.getMonto());
        }

        return montoTotal;
    }

    public void actualizarInteresCuotas(int metodo, List<Cuota> lstCuotas, BigDecimal tasaInteres) {
        switch (metodo) {
            case Prestamo.METODO_FRANCES:

                break;

            case Prestamo.METODO_ALEMAN:
                this.actualizarInteresCuotasMetodoAleman(lstCuotas, tasaInteres);
                break;
        }
    }

    private void actualizarInteresCuotasMetodoAleman(List<Cuota> lstCuotas, BigDecimal tasaInteres) {
        BigDecimal saldo = lstCuotas.get(0).getDeudaPendiente().add(lstCuotas.get(0).getAmortizacion());
        BigDecimal cuota = new BigDecimal(BigInteger.ZERO);
        BigDecimal interes = new BigDecimal(BigInteger.ZERO);
        BigDecimal amortizacion = lstCuotas.get(0).getAmortizacion();

        for (Cuota objCuota : lstCuotas) {
            interes = saldo.multiply(tasaInteres);
            cuota = amortizacion.add(interes);
            saldo = saldo.subtract(amortizacion);

            objCuota.setInteres(interes);
            objCuota.setMonto(cuota);
        }
    }

    public boolean verificarSolicitud(SolicitudPrestamo objSolicitudPrestamo) {
        boolean result = false;

        return result;
    }

    public BigDecimal calcularDeudaSocio(Socio objSocio) {
        BigDecimal deuda = new BigDecimal(BigInteger.ZERO);

        List<Cuota> lstCuotas = QueryFacade.getInstance().getAllCuotasPorPagar(objSocio);

        for (Cuota objCuota : lstCuotas) {
            deuda = deuda.add(objCuota.calcularMonto(new Date()));
        }

        return deuda;
    }

    public BigDecimal calcularSaldoAportes(Socio objSocio) {
        BigDecimal saldo = new BigDecimal(BigInteger.ZERO);

        List<Aporte> lstAportes = QueryFacade.getInstance().getAllAportes(objSocio);

        for (Aporte objAporte : lstAportes) {
            saldo = saldo.add(objAporte.getMonto());
        }

        return saldo;
    }

    public void updateMontoCuota(List<Cuota> lstCuotas, Date fechaCalculo) {
        for (Cuota objCuota : lstCuotas) {
            objCuota.updateMontos(fechaCalculo);
        }
    }

    public BigDecimal calcularDeudaAportes(Socio objSocio) {
        BigDecimal deuda;
//        Aporte objAporte = QueryFacade.getInstance().getLastAporte(objSocio);
//        if (objAporte != null) {
//            int meses = DateUtil.getMonthsDifference(objAporte.getFechaRegistro(), DateUtil.currentTimestamp());
//            deuda = Aporte.PAGO_APORTE_SOLES.multiply(new BigDecimal(meses));
//        }
//        return Aporte.PAGO_APORTE_SOLES;

        // desde el ingreso de socio
        SolicitudIngreso objSolicitudIngreso = QueryFacade.getInstance().getLastSolicitudIngreso(objSocio);
        int meses = DateUtil.getMonthsDifference(objSolicitudIngreso.getFechaRegistro(), DateUtil.currentTimestamp()); // meses desde el ingreso del socio

        // Nota: no considera si el monto de pago para los aportes cambia en el tiempo
        BigDecimal montoEstimadoAportes = Aporte.PAGO_APORTE_SOLES.multiply(new BigDecimal(meses));
        BigDecimal saldoAportes = this.calcularSaldoAportes(objSocio);
        deuda = montoEstimadoAportes.subtract(saldoAportes);
        if (deuda.compareTo(BigDecimal.ZERO) > 0) {
            return deuda;
        } else {
            return BigDecimal.ZERO;
        }
    }

    public String generarCodigoSolicitudPrestamo() {
        int year = new GregorianCalendar().get(Calendar.YEAR);
        return StringUtil.getStringCode(QueryFacade.getInstance().getSiguienteCodigoSolicitudPrestamo(year), 4) + "-" + String.valueOf(year);
    }

    public boolean debeAportes(Socio objSocio) {
        return this.calcularDeudaAportes(objSocio).compareTo(BigDecimal.ZERO) > 0;
    }

    public String verificarDisponibilidadGarante(Socio objSocio) {
        List<Prestamo> lstPrestamos = QueryFacade.getInstance().getAllPrestamosAsGarante(objSocio, false);
        //lstPrestamos.addAll(QueryFacade.getInstance().getAllPrestamosAsGarante(objSocio, false));
        List<SolicitudPrestamo> lstSolicitudesPrestamo = QueryFacade.getInstance().getAllSolicitudesPrestamoPorAprobarAsGarante(objSocio);

        if (!lstPrestamos.isEmpty()) {
            String message = "El Socio se encuentra como garante de " + lstPrestamos.size() + " Prestamos activos. ";
            if (lstPrestamos.size() >= 3) {
                message = message.concat("El Socio ha alcanzado el tope de Prestamos permitidos.");
            } else if (QueryFacade.getInstance().getMontoDisponibleAsGarante(objSocio).compareTo(BigDecimal.ZERO) > 0) {
                message = message.concat("El Socio se encuentra Disponible.");
            } else {
                message = message.concat("El Socio no cuenta con monto disponible para avalar.");
            }

            return message;
        }

        if (!lstSolicitudesPrestamo.isEmpty()) {
            String message = "El Socio se encuentra como garante de " + lstPrestamos.size() + " Solicitudes pendientes. ";
            if (lstSolicitudesPrestamo.size() >= 3) {
                message = message.concat("El Socio ha alcanzado el tope de Solicitudes permitidos.");
            } else if (QueryFacade.getInstance().getMontoDisponibleAsGarante(objSocio).compareTo(BigDecimal.ZERO) > 0) {
                message = message.concat("El Socio se encuentra Disponible.");
            } else {
                message = message.concat("El Socio no cuenta con monto disponible para avalar.");
            }

            return message;
        }

        return "El Socio se encuentra Disponible.";
    }

    public boolean verificarGarante(Socio objSocio, Socio objSocioGarante, List<Socio> lstGarantes) {
        if (objSocio.getId() != objSocioGarante.getId()) {
            for (Socio objGarante : lstGarantes) {
                if (objGarante.getId() == objSocioGarante.getId()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean verificarSocio(Socio objSocio, List<Socio> lstGarantes) {
        for (Socio objGarante : lstGarantes) {
            if (objGarante.getId() == objSocio.getId()) {
                return false;
            }
        }
        return true;
    }

    public DetalleSolicitudPrestamoSocio getDetalleSolicitudPrestamoSocio(Socio objSocio, BigDecimal nivelIngresos, BigDecimal otrosIngresos, BigDecimal nivelGastos) {
        return new DetalleSolicitudPrestamoSocio(0,
                false,
                nivelIngresos,
                nivelGastos,
                otrosIngresos,
                BigDecimal.ZERO,
                objSocio,
                null);
    }

    public BigDecimal calcularMontoDisponiblePrestamo(Socio objSocio, TableModel tableModel) {
        BigDecimal montoDisponible = objSocio == null ? BigDecimal.ZERO : calcularSaldoAportes(objSocio);

        int size = tableModel.getRowCount();
        for (int i = 0; i < size; i++) {
            montoDisponible = montoDisponible.add(new BigDecimal(tableModel.getValueAt(i, 5).toString()));
        }

        montoDisponible = montoDisponible.divide(BigDecimal.valueOf(3), 4, RoundingMode.HALF_UP);

        return montoDisponible;
    }

    public List<DetalleSolicitudPrestamoSocio> getAllDetalleSolicitudPrestamoSociosFiadores(List<Socio> lstFiadores, TableModel tableModel) {
        List<DetalleSolicitudPrestamoSocio> lstDetalleSolicitudPrestamoSocio = new ArrayList<>();

        int size = lstFiadores.size();
        for (int i = 0; i < size; i++) {
            DetalleSolicitudPrestamoSocio objDetalleSolicitudPrestamoSocio = new DetalleSolicitudPrestamoSocio(
                    0,
                    true,
                    new BigDecimal(tableModel.getValueAt(i, 7).toString()),
                    new BigDecimal(tableModel.getValueAt(i, 8).toString()),
                    BigDecimal.ZERO,
                    new BigDecimal(tableModel.getValueAt(i, 9).toString()),
                    lstFiadores.get(i),
                    null);
            lstDetalleSolicitudPrestamoSocio.add(objDetalleSolicitudPrestamoSocio);
        }

        return lstDetalleSolicitudPrestamoSocio;
    }

    public boolean ajustarCuotas(Prestamo objPrestamo) {
        boolean result = true;
        ConnectionManager.beginTransaction();
        try {
            if (objPrestamo.getId() > 0) {
                for (Cuota objCuota : objPrestamo.getLstCuotas()) {
                    result = UpdateFacade.getInstance().updateCuota(objCuota);
                    if (!result) {
                        break;
                    }
                }
            }

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }

    public boolean registrarHabilitacionCuotas(HabilitacionCuotas objHabilitacionCuotas) {
        boolean result = true;
        ConnectionManager.beginTransaction();
        try {
            result = InsertFacade.getInstance().insertHabilitacionCuotas(objHabilitacionCuotas);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }
}
