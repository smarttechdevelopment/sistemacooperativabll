package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAOCaja;
import com.smartech.sistemacooperativa.dal.DAOCuenta;
import com.smartech.sistemacooperativa.dal.DAODeposito;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.Moneda;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Permiso;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.TipoUsuario;
import com.smartech.sistemacooperativa.dominio.Usuario;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class DepositosProcessLogic {

    private Permiso objCurrentPermiso;
    private static final List<Moneda> lstMonedasAceptadas = new ArrayList<>();

    public Permiso getObjCurrentPermiso() {
        return objCurrentPermiso;
    }

    public void setObjCurrentPermiso(Permiso objCurrentPermiso) {
        this.objCurrentPermiso = objCurrentPermiso;
    }

    private static DepositosProcessLogic instance = null;

    private DepositosProcessLogic() {
    }

    public static DepositosProcessLogic getInstance() {
        if (instance == null) {
            instance = new DepositosProcessLogic();
            DepositosProcessLogic.lstMonedasAceptadas.add(QueryFacade.getInstance().getMonedaByAbreviatura("PEN"));
            Deposito.objMonedaBase = QueryFacade.getInstance().getMonedaBase();
        }

        return instance;
    }

    // Cuando una persona la busqueda de cuentas de socio
    public List<Cuenta> getAllCuentasByCheckUsuario(Socio objSocio, TipoUsuario objTipoUsuario) {

        setObjCurrentPermiso(QueryFacade.getInstance().getPermiso(Permiso.PERMISO_DEPOSITOS_ID));

        boolean result = true;
        List<Cuenta> lstCuentas = new ArrayList<>();

        // 1. Verificar ESTADO de SOCIO
        result = result && objSocio.isEstado();
        //result = result && objSocio.isActivo();

        // 2. Verificar PERMISO de USUARIO por TIPOUSUARIO
        // Nivel minimo: LECTURA
        result = result && objTipoUsuario.verificarPermiso(getObjCurrentPermiso().getNombre(), Permiso.NIVEL_LECTURA);

        // 3. Verificar por tipo de cuenta
        if (result) {
            lstCuentas = DAOCuenta.getInstance().getAllCuentas(objSocio);
        }

        return lstCuentas;
    }

    // Cuando una persona inicia el proceso de realizar deposito
    public boolean permiteRealizarDeposito(Cuenta objCuenta, Usuario objUsuario) {
        boolean result = true;

        setObjCurrentPermiso(QueryFacade.getInstance().getPermiso(Permiso.PERMISO_DEPOSITOS_ID));

        // 1. Verificar permiso de usuario para 
        // Nivel minimo: ESCRITURA
        result &= objUsuario.getObjTipoUsuario().verificarPermiso(getObjCurrentPermiso().getNombre(), Permiso.NIVEL_ESCRITURA);

        return result;
    }

    // Cuando realiza un pago
    public boolean permiteRealizarPagoDeposito(Usuario objUsuario, Deposito objDeposito) {

        boolean result;
        //Deposito.establecerMontoAdvertencia(objUsuario);
        //Deposito.advertirMontoDeposito(objDeposito.getMonto(), objDeposito.getLstPagos().get(0).getObjMoneda());

        Deposito objDepositoComprobar = DAODeposito.getInstance().getDeposito(objDeposito.getId());
        result = objDepositoComprobar.isEstado() && objDepositoComprobar.isHabilitado() && !objDepositoComprobar.isPagado();
        // provisionalmente
        if (result) {
            result = objDepositoComprobar.getObjCuenta().isEstado() && objDepositoComprobar.getObjCuenta().isActivo();
            if (result) {
                for (Pago objPago : objDeposito.getLstPagos()) {
                    for (Moneda objMoneda : DepositosProcessLogic.lstMonedasAceptadas) {
                        result &= objMoneda.equalsTo(objPago.getObjMoneda());
                    }
                }
                result &= objDeposito.verificarMontoDepositoYPagos();
            }

        }

        return result;
    }

    public boolean eliminarDeposito(Deposito objDeposito, Empleado objEmpleado) {
        boolean result = false;
        this.setObjCurrentPermiso(QueryFacade.getInstance().getPermiso(Permiso.PERMISO_DEPOSITOS_ID));
        if (objEmpleado.getObjUsuario().getObjTipoUsuario().verificarPermiso(this.getObjCurrentPermiso().getNombre(), Permiso.NIVEL_ELIMINACION)) {
            result = DAODeposito.getInstance().logicalDeleteDeposito(objDeposito);
        }
        return result;
    }

    // El depósito tiene los datos de la búsqueda
    public String registrarDeposito(Deposito objDeposito, Empleado objEmpleado, Establecimiento objEstablecimiento, Caja objCaja) {
        String mensaje = "Error";
        boolean result = false;

        if (objCaja.isAbierto()) {

            objCaja = DAOCaja.getInstance().getCaja(objCaja.getId());
            if (objCaja.isAbierto()) {

                try {
                    ConnectionManager.beginTransaction();

                    setObjCurrentPermiso(QueryFacade.getInstance().getPermiso(Permiso.PERMISO_DEPOSITOS_ID));

                    result = CuentasProcessLogic.getInstance().verificarSociosCuenta(objDeposito.getObjCuenta());

                    if (result) {

                        Cuenta objCuentaVerificada = DAOCuenta.getInstance().getCuenta(objDeposito.getObjCuenta().getId());
                        objDeposito.setObjCuenta(objCuentaVerificada);

                        if (objDeposito.getObjCuenta().isEstado()) {
                            if (objDeposito.getObjCuenta().isActivo()) {
                                if (objDeposito.isHabilitado()) {
                                    if (!objDeposito.isPagado()) {
                                        result = true;
//                            result = !objDeposito.getLstPagos().isEmpty();
//
//                            for(Pago objPago : objDeposito.getLstPagos()){
//                                for(Moneda objMoneda : DepositosProcessLogic.lstMonedasAceptadas){
//                                    result &= objMoneda.equalsTo(objPago.getObjMoneda());
//                                }
//                            }

                                        // 1. Verificar permiso de usuario para 
                                        // Nivel minimo: ESCRITURA
                                        result &= objEmpleado.getObjUsuario().getObjTipoUsuario().verificarPermiso(getObjCurrentPermiso().getNombre(), Permiso.NIVEL_ESCRITURA);

                                        if (result) {
                                            // 2. Verificar acreedor ?
                                            //                            objDeposito.getAcreedor();

                                            // 3. Verificar TIPO PAGO ?
                                            //                            lstPagos = objDeposito.getLstPagos();
                                            //                            for(Pago objPago : lstPagos) {
                                            //                                objPago.getTipopago();
                                            //                            }
                                            // 4. verificar monto
                                            //                            if(objDeposito.advertirMontoDeposito(objDeposito.getMonto())){
                                            //                                mensaje = "Debe realizar el procedimiento de 'Lavado de activos'";
                                            //                            }
                                            // 5. Registrar DEPOSITO
                                            result = DAODeposito.getInstance().insert(objDeposito);

                                            if (result) {
                                                mensaje = "Depósito registrado correctamente";
                                            } else {
                                                mensaje = "Error en registro";
                                            }

                                        }
                                    } else {
                                        mensaje = "Error: Ya se ha realizado el pago";
                                    }
                                } else {
                                    mensaje = "Error: No está habilitado para realizar el pago";
                                }
                            } else {
                                mensaje = "Error: La cuenta no está habilitada";
                            }
                        } else {
                            mensaje = "Error: La cuenta ya no existe";
                        }
                    } else {
                        mensaje = "Error: Los socios no han realizado aportes";
                    }
                } catch (Exception e) {
                    result = false;
                    e.printStackTrace();
                }

                if (result) {
                    ConnectionManager.commitTransaction();
                } else {
                    ConnectionManager.rollbackTransaction();
                }
            } else {
                mensaje = "Error: La caja ha sido bloqueada";
            }
        } else {
            mensaje = "Error: La caja está cerrada";
        }

        return mensaje;
    }
}
