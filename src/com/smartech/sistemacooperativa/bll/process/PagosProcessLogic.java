package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAOCuenta;
import com.smartech.sistemacooperativa.dal.DAODeposito;
import com.smartech.sistemacooperativa.dal.DAOPago;
import com.smartech.sistemacooperativa.dal.DAORenuncia;
import com.smartech.sistemacooperativa.dal.DAORetiro;
import com.smartech.sistemacooperativa.dal.DAOTasaInteres;
import com.smartech.sistemacooperativa.dal.DAOTipoCuenta;
import com.smartech.sistemacooperativa.dal.DAOTipoInteres;
import com.smartech.sistemacooperativa.dal.DAOTransferenciaCuenta;
import com.smartech.sistemacooperativa.dal.util.SequenceUtil;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Cuota;
import com.smartech.sistemacooperativa.dominio.Deposito;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.HabilitacionCuotas;
import com.smartech.sistemacooperativa.dominio.HistoricoCuenta;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Prestamo;
import com.smartech.sistemacooperativa.dominio.Renuncia;
import com.smartech.sistemacooperativa.dominio.Retiro;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoCuenta;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.dominio.TipoPago;
import com.smartech.sistemacooperativa.dominio.ITransaccion;
import com.smartech.sistemacooperativa.dominio.SolicitudPrestamo;
import com.smartech.sistemacooperativa.dominio.Transferencia;
import com.smartech.sistemacooperativa.dominio.TransferenciaCuenta;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class PagosProcessLogic {

    private static PagosProcessLogic instance = null;

    private PagosProcessLogic() {
    }

    public static PagosProcessLogic getInstance() {
        if (instance == null) {
            instance = new PagosProcessLogic();
        }

        return instance;
    }

    public boolean permiteSocioPagoTransaccion(ITransaccion objTransaccion, Socio objSocioVerificado) {
        // reglas para realizar pago
        if (objTransaccion instanceof Aporte) {
            Aporte objAporte = (Aporte) objTransaccion;
            // Tambien se verifica en generarPago
            if (objAporte.getObjTarjeta().getObjSocio() != null && objAporte.getObjTarjeta().getObjSocio().isEstado()) {
                if (objSocioVerificado.isEstado() && objSocioVerificado.isActivo()) {
                    if (!objAporte.isRetiro()) {
                        return true;
                    } else if (objAporte.getObjTarjeta().getObjSocio().getId() == objSocioVerificado.getId()) {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {

            if (!objSocioVerificado.isEstado() || !objSocioVerificado.isActivo()) {
                return false;
            }
            List<Socio> lstSocios = new ArrayList<>();
            String estadoActividad = RenunciaSocioProcessLogic.getInstance().getEstadoActividadSocio(objSocioVerificado);
            if (estadoActividad.equals(Socio.ESTADO_ACTIVIDAD_ACTIVO)) {
                if (objTransaccion instanceof Retiro) {
                    Retiro objRetiro = ((Retiro) objTransaccion);
                    if (objRetiro.getObjCuenta().isEstado() && objRetiro.getObjCuenta().isActivo()) {
                        lstSocios = objRetiro.getObjCuenta().getLstSocios();
                    }
                } else if (objTransaccion instanceof Deposito) {
                    Deposito objDeposito = ((Deposito) objTransaccion);
                    if (objDeposito.getObjCuenta().isEstado() && objDeposito.getObjCuenta().isActivo()) {
                        lstSocios = objDeposito.getObjCuenta().getLstSocios();
                    }
                } else if (objTransaccion instanceof HabilitacionCuotas) {
                    //lstSocios.add(((Cuota) objTransaccion).getObjPrestamo().getObjSolicitudPrestamo().getObjSocioPrestatario());
                    return true;
                } else if (objTransaccion instanceof Transferencia) {
                    if (objTransaccion instanceof TransferenciaCuenta) {
                        TransferenciaCuenta objTransferenciaCuenta = ((TransferenciaCuenta) objTransaccion);
                        if (objTransferenciaCuenta.evaluarMontoCuentas()) {
                            lstSocios = objTransferenciaCuenta.getObjCuentaDestino().getLstSocios();
                        }
                    }
                }
                for (Socio objSocio : lstSocios) {
                    if (objSocio.getId() == objSocioVerificado.getId()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public String generarPago(ITransaccion objTransaccion, TipoPago objTipoPago, Usuario objUsuario, Caja objCaja, Empleado objEmpleado, Establecimiento objEstablecimiento) {
        boolean result = false;
        List<Pago> lstPagos = new ArrayList<>();
        String mensaje = "Pago registrado correctamente";

        ConnectionManager.beginTransaction();
        Concepto objConcepto = null;
        List<Cuenta> lstCuentas = new ArrayList<>();
        try {
            //<editor-fold desc="Aporte" defaultstate="collapsed">
            if (objTransaccion instanceof Aporte) {
                Aporte objAporte = (Aporte) objTransaccion;
                if (objAporte.getObjTarjeta().getObjSocio() != null && objAporte.getObjTarjeta().getObjSocio().isEstado()) {
                    result = true;
                } else {
                    mensaje = "El Socio se encuentra Inactivo.";
                }
                if (result) {
                    if (!objAporte.isRetiro()) {
                        objConcepto = QueryFacade.getInstance().getConcepto("Aportes");
                    } else {
                        objConcepto = QueryFacade.getInstance().getConcepto("Retiros de renuncia");
                    }
                    Pago objPago = new Pago(
                            0,
                            DateUtil.currentTimestamp(),
                            objAporte.getMonto(),
                            DateUtil.currentTimestamp(),
                            null,
                            true,
                            objTipoPago,
                            objConcepto,
                            QueryFacade.getInstance().getMonedaByAbreviatura("PEN"));
                    result = InsertFacade.getInstance().insertPago(objPago);

                    if (result) {
                        lstPagos.add(objPago);
                        result = InsertFacade.getInstance().insertDetallePagoAporte(objAporte, objPago);
                        if (result) {
                            objAporte.setPagado(true);
                            result = UpdateFacade.getInstance().updateAporte(objAporte);
                            if (result) {
                                if (!objAporte.isRetiro()) {
                                    if (objAporte.isIngreso()) {
                                        objAporte.getObjTarjeta().getObjSocio().setActivo(true);
                                        objAporte.getObjTarjeta().getObjSocio().setRenunciado(false);
                                        result = UpdateFacade.getInstance().updateSocio(objAporte.getObjTarjeta().getObjSocio());
                                        if (result) {
                                            List<Socio> lstSocios = new ArrayList<>();
                                            lstSocios.add(objAporte.getObjTarjeta().getObjSocio());

                                            TipoCuenta objTipoCuenta = DAOTipoCuenta.getInstance().getTipoCuenta(TipoCuenta.TIPO_FONDO_MORTUORIO);
                                            TipoInteres objTipoInteres = DAOTipoInteres.getInstance().getAllTiposInteres(objTipoCuenta).get(0);
                                            List<TasaInteres> lstTasasInteres = DAOTasaInteres.getInstance().getAllTasasIntereses(objTipoInteres);
                                            TasaInteres objTasaInteres = lstTasasInteres.get(lstTasasInteres.size() - 1);
                                            mensaje = CuentasProcessLogic.getInstance().generarCuenta(objEmpleado, objTasaInteres, objTipoCuenta, lstSocios, objUsuario);
                                            result = !mensaje.contains("Error");

                                            if (result) {
                                                SolicitudIngreso objSolicitudIngreso = QueryFacade.getInstance().getSolicitudIngresoVigente(objAporte.getObjTarjeta().getObjSocio().getDocumentoIdentidad());
                                                objSolicitudIngreso.setPagado(true);
                                                result = UpdateFacade.getInstance().updateSolicitudIngreso(objSolicitudIngreso);
                                            }
                                        }
                                    } else if (PrestamosProcessLogic.getInstance().debeAportes(objAporte.getObjTarjeta().getObjSocio()) || !objAporte.getObjTarjeta().getObjSocio().isActivo()) {
                                        objAporte.getObjTarjeta().getObjSocio().setActivo(true);
                                        result = UpdateFacade.getInstance().updateSocio(objAporte.getObjTarjeta().getObjSocio());
                                    }
                                } else {
                                    Renuncia objRenuncia = DAORenuncia.getInstance().getRenuncia(objAporte.getObjTarjeta().getObjSocio());
                                    objRenuncia.setFechaModificacion(DateUtil.currentTimestamp());
                                    objRenuncia.setObjPago(objPago);
                                    objRenuncia.setPagado(true);
                                    result = DAORenuncia.getInstance().update(objRenuncia);
                                    if (result) {
                                        objAporte.getObjTarjeta().getObjSocio().setEstado(false);
                                        objAporte.getObjTarjeta().getObjSocio().setActivo(false);
                                        objAporte.getObjTarjeta().getObjSocio().setRenunciado(true);
                                        result = UpdateFacade.getInstance().updateSocio(objAporte.getObjTarjeta().getObjSocio());
                                    }

                                }
                                if (result) {
                                    mensaje = "Aporte realizado correctamente";
                                } else {
                                    mensaje = "Error en registro de Aporte";
                                }
                            }
                        }
                    }
                }
            }
            //</editor-fold>

            //<editor-fold desc="Deposito" defaultstate="collapsed">
            if (objTransaccion instanceof Deposito) {
                Date fechaModificacion = new Date();
                Deposito objDeposito = (Deposito) objTransaccion;
                lstCuentas.clear();
                lstCuentas.add(objDeposito.getObjCuenta());

                objConcepto = QueryFacade.getInstance().getConcepto("Depositos");
                lstPagos.add(new Pago(0, DateUtil.currentTimestamp(), objDeposito.getMonto(), DateUtil.currentTimestamp(), null, true, objTipoPago, objConcepto, objDeposito.getObjCuenta().getObjTipoCuenta().getObjMoneda()));
                objDeposito.setLstPagos(lstPagos);

                if (DepositosProcessLogic.getInstance().permiteRealizarPagoDeposito(objUsuario, objDeposito)) {
                    result = true;

                    if (!objDeposito.getLstPagos().isEmpty()) {
                        for (Pago objPago : objDeposito.getLstPagos()) {
                            result = DAOPago.getInstance().insert(objPago);
                            if (result) {
                                result = DAODeposito.getInstance().insertDetallePago(objDeposito, objPago);
                                if (!result) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    } else {
                        result = false;
                    }

                    if (result) {
                        objDeposito.setPagado(true);
                        objDeposito.setFechaModificacion(DateUtil.getTimestamp(fechaModificacion));

                        result = DAODeposito.getInstance().update(objDeposito);
                        if (result) {
                            result = objDeposito.actualizarCuenta();
                            if (result) {
                                result = DAOCuenta.getInstance().update(objDeposito.getObjCuenta());
                                if (result) {
                                    mensaje = "Depósito realizado correctamente";
                                } else {
                                    mensaje = "Error en registro de Depósito";
                                }
                            }
                        }
                    }
                } else {
                    mensaje = "Error en el Depósito que intenta realizar";
                }
            }
            //</editor-fold>

            //<editor-fold desc="Retiro" defaultstate="collapsed">
            if (objTransaccion instanceof Retiro) {
                Date fechaModificacion = new Date();

                Retiro objRetiro = (Retiro) objTransaccion;
                lstCuentas.clear();
                lstCuentas.add(objRetiro.getObjCuenta());

                objConcepto = QueryFacade.getInstance().getConcepto("Retiros de cuenta");
                lstPagos.add(new Pago(0, DateUtil.currentTimestamp(), objRetiro.getMonto(), DateUtil.currentTimestamp(), null, true, objTipoPago, objConcepto, objRetiro.getObjCuenta().getObjTipoCuenta().getObjMoneda()));
                objRetiro.setLstPagos(lstPagos);

                result = RetirosProcessLogic.getInstance().permiteRealizarPagoRetiro(objUsuario, objRetiro);
                if (result) {
                    if (!objRetiro.getLstPagos().isEmpty()) {
                        for (Pago objPago : objRetiro.getLstPagos()) {
                            result = DAOPago.getInstance().insert(objPago);
                            if (result) {
                                result = DAORetiro.getInstance().insertDetalleRetiro(objRetiro, objPago);
                                if (!result) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    } else {
                        result = false;
                    }

                    if (result) {
                        if (objRetiro.evaluarMontoRetiroYCuenta()) {
                            objRetiro.setPagado(true);
                            objRetiro.setFechaModificacion(DateUtil.getTimestamp(fechaModificacion));

                            result = DAORetiro.getInstance().update(objRetiro);

                            if (result) {
                                result = objRetiro.actualizarCuenta();
                                if (result) {
                                    result = DAOCuenta.getInstance().update(objRetiro.getObjCuenta());
                                    if (result) {
                                        mensaje = "Retiro realizado correctamente";
                                    } else {
                                        mensaje = "Error en registro de Retiro";
                                    }
                                }
                            }
                        } else {
                            mensaje = "Error: saldo insuficiente";
                        }
                    }
                } else {
                    mensaje = "Error en el Retiro que intenta realizar";
                }
            }
            //</editor-fold>

            //<editor-fold desc="Transferencia" defaultstate="collapsed">
            if (objTransaccion instanceof Transferencia) {
                Transferencia objTransferencia = (Transferencia) objTransaccion;
                objConcepto = QueryFacade.getInstance().getConcepto("Transferencias");

                lstCuentas.clear();

                Pago objPago = new Pago(
                        0,
                        DateUtil.currentTimestamp(),
                        objTransferencia.getMonto(),
                        objTransferencia.getFechaRegistro(),
                        null,
                        true,
                        objTipoPago,
                        objConcepto,
                        objTransferencia.getObjMoneda());
                result = InsertFacade.getInstance().insertPago(objPago);
                if (result) {
                    lstPagos.add(objPago);
                    if (objTransferencia instanceof TransferenciaCuenta) {
                        TransferenciaCuenta objTransferenciaCuenta = (TransferenciaCuenta) objTransferencia;

                        lstCuentas.add(objTransferenciaCuenta.getObjCuentaOrigen());
                        lstCuentas.add(objTransferenciaCuenta.getObjCuentaDestino());

                        result = DAOTransferenciaCuenta.getInstance().insertDetallePagoTransferenciaCuenta(objPago, objTransferenciaCuenta);
                        if (result) {
                            // Actualizar cuentas
                            if (objTransferenciaCuenta.evaluarMontoCuentas()) {
                                objTransferenciaCuenta.actualizarCuentas();
                                result = DAOCuenta.getInstance().update(objTransferenciaCuenta.getObjCuentaOrigen());
                                if (result) {
                                    result = DAOCuenta.getInstance().update(objTransferenciaCuenta.getObjCuentaDestino());
                                }
                            }
                            if (result) {
                                mensaje = "Pago realizado correctamente. Cuentas actualizadas";
                            } else {
                                mensaje = "Error en actualización de cuentas";
                            }
                        } else {
                            mensaje = "Error en el pago de la TransferenciaCuenta";
                        }
                    }
                } else {
                    mensaje = "Error en el pago de la Transferencia";
                }
            }
            //</editor-fold>

            //<editor-fold desc="HabilitacionCuotas" defaultstate="collapsed">
            if (objTransaccion instanceof HabilitacionCuotas) {
                objConcepto = QueryFacade.getInstance().getConcepto("Cuotas");
                HabilitacionCuotas objHabilitacionCuotas = (HabilitacionCuotas) objTransaccion;
                result = objHabilitacionCuotas.getLstCuotas().get(0).getObjPrestamo().getObjSolicitudPrestamo().getObjSocioPrestatario().isEstado() && objHabilitacionCuotas.getLstCuotas().get(0).getObjPrestamo().getObjSolicitudPrestamo().getObjSocioPrestatario().isActivo();
                if (result) {
                    Pago objPago = new Pago(
                            0,
                            DateUtil.currentTimestamp(),
                            objHabilitacionCuotas.getMonto(),
                            DateUtil.currentTimestamp(),
                            null,
                            true,
                            objTipoPago,
                            objConcepto,
                            QueryFacade.getInstance().getMonedaByAbreviatura("PEN"));
                    result = InsertFacade.getInstance().insertPago(objPago);

                    if (result) {
                        lstPagos.add(objPago);
                        result = InsertFacade.getInstance().insertDetalleHabilitacionCuotaPago(objHabilitacionCuotas, objPago);
                        if (result) {
                            objHabilitacionCuotas.setPagado(true);
                            result = UpdateFacade.getInstance().updateHabilitacionCuotas(objHabilitacionCuotas);
                            if (result) {
                                for (Cuota objCuota : objHabilitacionCuotas.getLstCuotas()) {
                                    objCuota.setPagado(true);
                                    objCuota.setFechaPago(DateUtil.currentTimestamp());
                                }
                                result = UpdateFacade.getInstance().updateCuotas(objHabilitacionCuotas.getLstCuotas());
                                if (result) {
                                    Prestamo objPrestamo = QueryFacade.getInstance().getPrestamo(objHabilitacionCuotas.getLstCuotas().get(0).getObjPrestamo().getId());
                                    objPrestamo.setPagado();
                                    if (objPrestamo.isPagado()) {
                                        if (objPrestamo.isPagado()) {
                                            result = UpdateFacade.getInstance().updatePrestamo(objPrestamo);
                                        }
                                    }
                                }
                            }
                            if (result) {
                                mensaje = "Cuotas pagadas correctamente";
                            } else {
                                mensaje = "Error en el pago de las Cuotas";
                            }
                        }
                    }
                }
            }
            //</editor-fold>

            //<editor-fold desc="Movimiento Administrativo" defaultstate="collapsed">
            if (objTransaccion instanceof MovimientoAdministrativo) {
                MovimientoAdministrativo objMovimientoAdministrativo = (MovimientoAdministrativo) objTransaccion;

                objConcepto = objMovimientoAdministrativo.isHabilitado() ? QueryFacade.getInstance().getConcepto("Ingreso caja administrativo") : QueryFacade.getInstance().getConcepto("Egreso caja administrativo");
                Pago objPago = new Pago(
                        0,
                        DateUtil.currentTimestamp(),
                        objMovimientoAdministrativo.getMonto(),
                        DateUtil.currentTimestamp(),
                        null,
                        true,
                        objTipoPago,
                        objConcepto,
                        QueryFacade.getInstance().getMonedaByAbreviatura("PEN"));
                result = InsertFacade.getInstance().insertPago(objPago);

                if (result) {
                    lstPagos.add(objPago);
                    result = InsertFacade.getInstance().insertDetallePagoMovimientoAdministrativo(objPago, objMovimientoAdministrativo);
                    if (result) {
                        objMovimientoAdministrativo.setPagado(true);
                        result = UpdateFacade.getInstance().updateMovimientoAdministrativo(objMovimientoAdministrativo);
                    }
                }

                if (result) {
                    mensaje = "Movimiento pagado correctamente";
                } else {
                    mensaje = "Error en el pago del Movimiento";
                }
            }
            //</editor-fold>

            //<editor-fold desc="Solicitud Prestamo" defaultstate="collapsed">
            if (objTransaccion instanceof SolicitudPrestamo) {
                SolicitudPrestamo objSolicitudPrestamo = (SolicitudPrestamo) objTransaccion;
                objConcepto = QueryFacade.getInstance().getConcepto("Solicitud de Prestamo");
                Pago objPago = new Pago(
                        0,
                        DateUtil.currentTimestamp(),
                        objSolicitudPrestamo.getMontoAprobado(),
                        DateUtil.currentTimestamp(),
                        null,
                        true,
                        objTipoPago,
                        objConcepto,
                        QueryFacade.getInstance().getMonedaByAbreviatura("PEN"));
                result = InsertFacade.getInstance().insertPago(objPago);
                if (result) {
                    lstPagos.add(objPago);
                    result = InsertFacade.getInstance().insertDetallePagoSolicitudPrestamo(objPago, objSolicitudPrestamo);
                    if (result) {
                        objSolicitudPrestamo.setPagado(true);
                        result = UpdateFacade.getInstance().updateSolicitudPrestamo(objSolicitudPrestamo);
                    }
                }

                if (result) {
                    mensaje = "Solicitud pagada correctamente";
                } else {
                    mensaje = "Error en el pago de la Solicitud";
                }
            }
            //</editor-fold>
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();

            if (result) {
                List<Pago> lstPagosAdicionales = this.generarPagosAdicionales(objConcepto, objTransaccion);

                mensaje = this.registrarMovimientosPagos(objCaja, objEmpleado, lstPagos, lstPagosAdicionales);

                if (!mensaje.contains("Error")) {
                    lstPagos.addAll(lstPagosAdicionales);
                    if (result) {
                        result = ComprobantesProcessLogic.getInstance().generarComprobante(objTransaccion, lstPagos, objEstablecimiento, lstCuentas);
                        if (!result) {
                            mensaje = "Error en la generacion del Comprobante";
                        }
                    }
                }
            }
        } else {
            if (!mensaje.isEmpty()) {
                mensaje = mensaje.concat("\n");
            }
            mensaje = mensaje.concat("Error al registrar el pago.");
            ConnectionManager.rollbackTransaction();
        }

        return mensaje;
    }

    public String registrarMovimientosPagos(Caja objCaja, Empleado objEmpleado, List<Pago> lstPagos, List<Pago> lstPagosAdicionales) {
        boolean result = false;
        String mensaje = "Movimiento registrado correctamente";

        try {
            if (objCaja != null && objEmpleado != null) {
                for (Pago objPago : lstPagos) {
                    result = CajaProcessLogic.getInstance().registrarMovimiento(objPago, objCaja, objEmpleado);
                    if (!result) {
                        break;
                    }
                }
                if (result) {
                    for (Pago objPagoRegistro : lstPagosAdicionales) {
                        result = InsertFacade.getInstance().insertPago(objPagoRegistro);
                        if (result) {
                            result = CajaProcessLogic.getInstance().registrarMovimiento(objPagoRegistro, objCaja, objEmpleado);
                            if (!result) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }

                if (!result) {
                    mensaje = "Error en la generacion de la Transaccion.";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
            mensaje = "Error en la generacion de la Transaccion.";
        }

        return mensaje;
    }

    public List<Pago> generarPagosAdicionales(Concepto objConcepto, ITransaccion objTransaccion) {
        List<Pago> lstPagos = new ArrayList<>();

        return lstPagos;
    }

    public static void sortByDate(List<ITransaccion> lstTransacciones, boolean ascendant) {
        ITransaccion objTransaccionTemporal;
        if (ascendant) {
            for (int i = 0; i < lstTransacciones.size() - 1; i++) {
                if (lstTransacciones.get(i).getFechaModificacion().compareTo(lstTransacciones.get(i + 1).getFechaModificacion()) > 0) {
                    objTransaccionTemporal = lstTransacciones.get(i);
                    lstTransacciones.set(i, lstTransacciones.get(i + 1));
                    lstTransacciones.set(i + 1, objTransaccionTemporal);
                }
            }
        } else {
            for (int i = 0; i < lstTransacciones.size() - 1; i++) {
                if (lstTransacciones.get(i).getFechaModificacion().compareTo(lstTransacciones.get(i + 1).getFechaModificacion()) < 0) {
                    objTransaccionTemporal = lstTransacciones.get(i);
                    lstTransacciones.set(i, lstTransacciones.get(i + 1));
                    lstTransacciones.set(i + 1, objTransaccionTemporal);
                }
            }
        }
    }
}
