package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.DeleteFacade;
import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAODetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dal.util.SequenceUtil;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.DetalleFamiliarSocio;
import com.smartech.sistemacooperativa.dominio.DetalleTipoSolicitudDocumento;
import com.smartech.sistemacooperativa.dominio.Documento;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.Huella;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudIngreso;
import com.smartech.sistemacooperativa.dominio.Tarjeta;
import com.smartech.sistemacooperativa.dominio.TipoSolicitud;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.FileUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class AdmisionSociosProcessLogic {

    private static AdmisionSociosProcessLogic instance = null;

    private AdmisionSociosProcessLogic() {
    }

    public static AdmisionSociosProcessLogic getInstance() {
        if (instance == null) {
            instance = new AdmisionSociosProcessLogic();
        }

        return instance;
    }

    public List<DetalleDocumentoSolicitud> generarDetalleDocumentoSolicitud(TipoSolicitud objTipoSolicitud, Usuario objUsuario) {
        List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitud = new ArrayList<>();

        for (DetalleTipoSolicitudDocumento objDetalleTipoSolicitudDocumento : objTipoSolicitud.getLstDetalleTipoSolicitudDocumento()) {
            DetalleDocumentoSolicitud objDetalleDocumentoSolicitud = new DetalleDocumentoSolicitud(
                    0,
                    null, //fechaentrega
                    objDetalleTipoSolicitudDocumento.getObjDocumento(),
                    "", //ruta
                    null, //representacion
                    null, //objsolicitud
                    false, //aprobado
                    null, //fechamodificacion
                    null, //fechasolicitud
                    objUsuario
            );

            lstDetalleDocumentoSolicitud.add(objDetalleDocumentoSolicitud);
        }

        return lstDetalleDocumentoSolicitud;
    }

    public String registrarSolicitudIngreso(SolicitudIngreso objSolicitudIngreso, List<Huella> lstHuellas, Establecimiento objEstablecimiento, Map<Documento, File> mapArchivos, File fileFoto) {
        String resultMessage = "";
        boolean result = false;

        ConnectionManager.beginTransaction();
        FileUtil.connect();
        try {
            if (FileUtil.areNotNull(new ArrayList<>(mapArchivos.values()))) {
                SolicitudIngreso objSolicitudIngresoExistente = QueryFacade.getInstance().getSolicitudIngresoVigente(objSolicitudIngreso.getObjSocio().getDocumentoIdentidad());
                // Verificar si existe socio con el mismo DNI
                if (objSolicitudIngresoExistente == null) {
                    objSolicitudIngreso.getObjSocio().setObjUsuario(null);

                    Usuario objUsuario = new Usuario(
                            0,
                            objSolicitudIngreso.getObjSocio().getDocumentoIdentidad(),
                            StringUtil.encodeString(objSolicitudIngreso.getObjSocio().getDocumentoIdentidad()),
                            true,
                            false,
                            QueryFacade.getInstance().getTipoUsuario(6),
                            lstHuellas);
                    result = InsertFacade.getInstance().insertUsuario(objUsuario);

                    if (result) {
                        String destinationPath = "/SolicitudesIngreso/" + objSolicitudIngreso.getObjSocio().getDocumentoIdentidad() + "/" + objSolicitudIngreso.getCodigo();
                        objSolicitudIngreso.getObjSocio().setObjUsuario(objUsuario);
                        objSolicitudIngreso.getObjSocio().updateRutaFoto(fileFoto, destinationPath);
                        objSolicitudIngreso.updateRutas(mapArchivos, destinationPath);
                        result = InsertFacade.getInstance().insertSolicitudIngreso(objSolicitudIngreso);

                        if (result) {
                            result = FileUtil.createDirectoryFTP(destinationPath);
                            if (result) {
                                result = fileFoto == null ? true : FileUtil.saveFileFTP(fileFoto, destinationPath);
                                if (result) {
                                    List<File> lstArchivosDocumentos = new ArrayList<>(mapArchivos.values());
                                    result = FileUtil.saveFilesFTP(lstArchivosDocumentos, destinationPath);
                                    if (result) {
                                        objSolicitudIngreso.updateAllDocumentosAprobados();
                                        if (objSolicitudIngreso.isAprobado()) {
                                            if (result) {
                                                result = this.generarTarjeta(objSolicitudIngreso, objEstablecimiento);
                                            }
                                        }
                                    }
                                } else {
                                    resultMessage = "Error al cargar los archivos, revise los campos.";
                                }
                            } else {
                                resultMessage = "Error al cargar los archivos, revise los campos.";
                            }
                        }
                    }
                } else {
                    resultMessage = "Error. Ya existe una Solicitud vigente.";
                }
            } else {
                resultMessage = "Error al cargar los archivos, revise los campos.";
            }
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
            resultMessage = "Solicitud registrada con exito.";
        } else {
            //DeleteFacade.getInstance().rollbackInsertSolicitud(objSolicitudIngreso);
            ConnectionManager.rollbackTransaction();
            objSolicitudIngreso.setId(0);
            if (resultMessage.isEmpty()) {
                resultMessage = "Error en el registro de la Solicitud.";
            }
        }

        FileUtil.disconnect();
        return resultMessage;
    }

    public String actualizarSolicitudIngreso(SolicitudIngreso objSolicitudIngreso, Establecimiento objEstablecimiento, Map<Documento, File> mapArchivos, File fileFoto) {
        String resultMessage = "";

        boolean result = false;

        ConnectionManager.beginTransaction();
        FileUtil.connect();
        try {
            objSolicitudIngreso.updateAllDocumentosAprobados();
            String destinationPath = "/SolicitudesIngreso/" + objSolicitudIngreso.getObjSocio().getDocumentoIdentidad() + "/" + objSolicitudIngreso.getCodigo();
            objSolicitudIngreso.getObjSocio().updateRutaFoto(fileFoto, destinationPath);
            objSolicitudIngreso.updateRutas(mapArchivos, destinationPath);
            result = UpdateFacade.getInstance().updateSolicitudIngreso(objSolicitudIngreso);
            if (result) {
                result = UpdateFacade.getInstance().updateSocio(objSolicitudIngreso.getObjSocio());
                if (result) {
                    result = FileUtil.createDirectoryFTP(destinationPath);
                    if (result) {
                        result = fileFoto == null ? true : FileUtil.saveFileFTP(fileFoto, destinationPath);
                        if (result) {
                            List<File> lstArchivosDocumentos = new ArrayList<>(mapArchivos.values());
                            result = FileUtil.saveFilesFTP(lstArchivosDocumentos, destinationPath);
                            if (result) {
                                if (objSolicitudIngreso.isAprobado()) {
                                    if (result) {
                                        result = this.generarTarjeta(objSolicitudIngreso, objEstablecimiento);
                                    }
                                }
                            } else {
                                resultMessage = "Error al cargar los archivos, revise los campos.";
                            }
                        } else {
                            resultMessage = "Error al cargar los archivos, revise los campos.";
                        }
                    } else {
                        resultMessage = "Error al cargar los archivos, revise los campos.";
                    }
                }
            }
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
            resultMessage = "Solicitud actualizada correctamente.";
        } else {
            ConnectionManager.rollbackTransaction();
            resultMessage = "Error en la actualizacion de la Solicitud.";
        }

        FileUtil.disconnect();
        return resultMessage;
    }

    public boolean actualizarSocioOnly(Socio objSocio, File fileFoto) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        FileUtil.connect();
        try {
            SolicitudIngreso objSolicitudIngreso = QueryFacade.getInstance().getSolicitudIngresoVigente(objSocio);
            String destinationPath = "/SolicitudesIngreso/" + objSolicitudIngreso.getObjSocio().getDocumentoIdentidad() + "/" + objSolicitudIngreso.getCodigo();
            objSocio.updateRutaFoto(fileFoto, destinationPath);
            result = UpdateFacade.getInstance().updateSocio(objSocio);
            if (result) {
                result = DAODetalleFamiliarSocio.getInstance().physicalDelete(objSocio);
                if (result) {
                    result = FileUtil.createDirectoryFTP(destinationPath);
                    if (result) {
                        result = fileFoto == null ? true : FileUtil.saveFileFTP(fileFoto, destinationPath);
                        if (result) {
                            result = InsertFacade.getInstance().insertFamiliaresSocio(objSocio.getLstFamiliaresSocio());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        FileUtil.disconnect();
        return result;
    }

    public String generarCodigoTarjeta(Establecimiento objEstablecimiento) {
        String codigoOficina = objEstablecimiento.getCodigo();
        String codigoRegistro = DateUtil.getDate(new Date(), "MMyy");
        String codigoSecuencial = StringUtil.getStringCode(Integer.parseInt(QueryFacade.getInstance().getUltimoCodigoTarjeta()) + 1, 3);

        String codigoTarjeta = codigoOficina + codigoRegistro + codigoSecuencial;

        String digitoVerificacion = Tarjeta.generarDigitoVerificacion(codigoTarjeta);
        int codigoSecuencialInt = Integer.parseInt(codigoSecuencial);

        while (digitoVerificacion.length() == 0 || digitoVerificacion.length() >= 2) {
            codigoSecuencialInt++;
            codigoSecuencial = StringUtil.getStringCode(codigoSecuencialInt, 3);
            codigoTarjeta = codigoOficina + codigoRegistro + codigoSecuencial;
            digitoVerificacion = Tarjeta.generarDigitoVerificacion(codigoTarjeta);
        }

        codigoTarjeta = codigoOficina + codigoRegistro + codigoSecuencial + digitoVerificacion;

        return codigoTarjeta;
    }

    public String generarCodigoSocio() {
//        return StringUtil.getStringCode(SequenceUtil.getTableSequenceNextValue("socios"), 4);
        return StringUtil.getStringCode(QueryFacade.getInstance().getSiguienteCodigoSocio(), 4);
    }

    public String generarCodigoSolicitudIngreso() {
        int year = new GregorianCalendar().get(Calendar.YEAR);
        return StringUtil.getStringCode(QueryFacade.getInstance().getSiguienteCodigoSolicitudIngreso(year), 4) + "-" + String.valueOf(year);
    }

    public boolean generarTarjeta(SolicitudIngreso objSolicitudIngreso, Establecimiento objEstablecimiento) {
        boolean result = false;

        try {
            Tarjeta objTarjeta = new Tarjeta(
                    0,
                    this.generarCodigoTarjeta(objEstablecimiento),
                    StringUtil.encodeString("0000"),
                    DateUtil.currentTimestamp(),
                    false,
                    0,
                    DateUtil.currentTimestamp(),
                    null,
                    null,
                    true,
                    QueryFacade.getInstance().getTipoTarjeta(1),
                    objSolicitudIngreso.getObjSocio());

            result = InsertFacade.getInstance().insertTarjeta(objTarjeta);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean generarPagoInscripcion(SolicitudIngreso objSolicitudIngreso, Usuario objUsuario) {
        boolean result = false;

        try {
            ConnectionManager.beginTransaction();
            Aporte objAporte = new Aporte(
                    0,
                    Aporte.PAGO_APORTE_SOLES,
                    DateUtil.currentTimestamp(),
                    null,
                    true,
                    objUsuario,
                    true,
                    false,
                    true,
                    false,
                    objSolicitudIngreso.getObjSocio().getObjTarjeta(),
                    new ArrayList<>());

            result = InsertFacade.getInstance().insertAporte(objAporte);
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }

    public Aporte generarPagoReingreso(Tarjeta objTarjeta, Usuario objUsuario) {

        Aporte objAporte = new Aporte(
                0,
                PrestamosProcessLogic.getInstance().calcularDeudaAportes(objTarjeta.getObjSocio()),
                DateUtil.currentTimestamp(),
                null,
                true,
                objUsuario,
                true,
                false,
                true,
                false,
                objTarjeta,
                new ArrayList<>());

        return objAporte;
    }

    public boolean desaprobarDocumentosSolicitudIngresoPorPagar(SolicitudIngreso objSolicitudIngreso) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            if (QueryFacade.getInstance().getSolicitudIngresoVigente(objSolicitudIngreso.getObjSocio().getDocumentoIdentidad()) != null) {
                result = UpdateFacade.getInstance().discardAllDetalleDocumentoSolicitudIngreso(objSolicitudIngreso);
                if (result) {
                    Aporte objAporte = QueryFacade.getInstance().getAporteIngresoImpago(objSolicitudIngreso.getObjSocio());
                    result = objAporte == null ? true : DeleteFacade.getInstance().deleteAportePhysical(objAporte);
                    if (result) {
                        Tarjeta objTarjeta = QueryFacade.getInstance().getTarjeta(objSolicitudIngreso.getObjSocio());
                        result = objTarjeta == null ? true : DeleteFacade.getInstance().deleteTarjeta(objTarjeta);
                    }
                }
            }
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }

    public boolean existePagoAporteIngreso(Socio objSocio) {
        boolean result = false;

        try {
            result = QueryFacade.getInstance().getAporteIngresoImpago(objSocio) != null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean actualizarTarjeta(Tarjeta objTarjeta) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            result = UpdateFacade.getInstance().updateTarjeta(objTarjeta);
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }

    public void ajustarPorcentajeBeneficio(List<DetalleFamiliarSocio> lstDetalleFamiliarSocio) {
        if (!lstDetalleFamiliarSocio.isEmpty()) {
            BigDecimal porcentajeBeneficio = BigDecimal.valueOf(100).divide(BigDecimal.valueOf(lstDetalleFamiliarSocio.size()), 4, RoundingMode.HALF_UP);

            for (DetalleFamiliarSocio objDetalleFamiliarSocio : lstDetalleFamiliarSocio) {
                objDetalleFamiliarSocio.setPorcentajeBeneficio(porcentajeBeneficio);
            }
        }
    }

    public void ajustarPorcentajeBeneficio(List<DetalleFamiliarSocio> lstDetalleFamiliarSocio, DetalleFamiliarSocio objDetalleFamiliarSocio) {
        BigDecimal diferenciaPorcentajeBeneficio = BigDecimal.valueOf(100).subtract(objDetalleFamiliarSocio.getPorcentajeBeneficio());

        BigDecimal porcentajeBeneficio = diferenciaPorcentajeBeneficio.divide(BigDecimal.valueOf(lstDetalleFamiliarSocio.size() - 1), 4, RoundingMode.HALF_UP);

        for (DetalleFamiliarSocio objDetalleFamiliarSocioCambio : lstDetalleFamiliarSocio) {
            if (!objDetalleFamiliarSocioCambio.getObjFamiliar().getDocumentoIdentidad().equals(objDetalleFamiliarSocio.getObjFamiliar().getDocumentoIdentidad())) {
                objDetalleFamiliarSocioCambio.setPorcentajeBeneficio(porcentajeBeneficio);
            }
        }
    }
}
