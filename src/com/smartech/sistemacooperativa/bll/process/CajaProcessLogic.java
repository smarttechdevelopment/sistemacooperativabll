package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.DeleteFacade;
import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAOCaja;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.HistoricoCaja;
import com.smartech.sistemacooperativa.dominio.Movimiento;
import com.smartech.sistemacooperativa.dominio.MovimientoAdministrativo;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.math.BigDecimal;

/**
 *
 * @author Smartech
 */
public class CajaProcessLogic {

    private static CajaProcessLogic instance = null;

    private CajaProcessLogic() {
    }

    public static CajaProcessLogic getInstance() {
        if (instance == null) {
            instance = new CajaProcessLogic();
        }

        return instance;
    }

    public boolean esPosibleRealizarPago(Pago objPago, Caja objCaja) {
        BigDecimal montoAPagar = this.getMontoByConcepto(objPago.getMonto(), objPago.getObjConcepto());
        // No verifica cuando el monto es cero
        return montoAPagar.compareTo(BigDecimal.ZERO) > 0 ? true : objCaja.getMonto().compareTo(montoAPagar) >= 0;
    }

    public boolean registrarMovimiento(Pago objPago, Caja objCaja, Empleado objEmpleado) {
        boolean result = false;

        if (this.esPosibleRealizarPago(objPago, objCaja)) {

            BigDecimal montoSaldoCaja = this.getMontoByConcepto(objPago.getMonto(), objPago.getObjConcepto()).compareTo(BigDecimal.ZERO) > 0 ? objCaja.getMonto().add(objPago.getMonto()): objCaja.getMonto().subtract(objPago.getMonto());
            ConnectionManager.beginTransaction();
            try {
                if (objCaja.isAbierto()) {
                    Movimiento objMovimiento = new Movimiento(
                            0,
                            objPago.getFecha(),
                            DateUtil.currentTimestamp(),
                            null,
                            true,
                            objPago,
                            objCaja,
                            objEmpleado);

                    HistoricoCaja objHistoricoCaja = QueryFacade.getInstance().getHistoricoCaja(objCaja);
                    if (objHistoricoCaja == null) {
                            objHistoricoCaja = new HistoricoCaja(
                                    0,
                                    DateUtil.currentTimestamp(),
                                    null,
                                    objCaja.getMonto(),
                                    montoSaldoCaja,
                                    objCaja,
                                    objEmpleado);
                            result = InsertFacade.getInstance().insertHistoricoCaja(objHistoricoCaja);
                            System.out.println("Apertura de caja - " + objHistoricoCaja.getFechaInicio() + " : " + objMovimiento.getObjPago().getMonto().toPlainString());
                    } else {
                        if (DateUtil.currentTimestamp().after(DateUtil.lastTime(objHistoricoCaja.getFechaInicio()))) {

                            objHistoricoCaja.setFechaFin(DateUtil.lastTime(objHistoricoCaja.getFechaInicio()));
                            // objHistoricoCaja.setMontoFin(montoSaldoCaja); El monto de la caja se mantiene con el último valor al término del día
                            if(objHistoricoCaja.getMontoFin() == null){
                                objHistoricoCaja.setMontoFin(objHistoricoCaja.getMontoInicio());
                            }
                            // else{} El monto se debe manetener porque es movimiento de otro día
                            result = UpdateFacade.getInstance().updateHistoricoCaja(objHistoricoCaja);
                            
                            if (result) {
                                System.out.println("Cierre de caja - " + objHistoricoCaja.getFechaFin() + " : " + objHistoricoCaja.getMontoFin());
                                objHistoricoCaja = new HistoricoCaja(
                                        0,
                                        DateUtil.currentTimestamp(),
                                        null,
                                        objHistoricoCaja.getMontoFin(),
                                        montoSaldoCaja,
                                        objCaja,
                                        objEmpleado);
                                result = InsertFacade.getInstance().insertHistoricoCaja(objHistoricoCaja);

                            } else {
                                System.out.println("No hubo cierre de caja - " + objHistoricoCaja.getFechaInicio() + " : " + objHistoricoCaja.getMontoFin());
                            }

                        } else {
                            objHistoricoCaja.setMontoFin(montoSaldoCaja);
                            // No actualiza fecha fin porque no fue el último movimiento del día.
                            result = UpdateFacade.getInstance().updateHistoricoCaja(objHistoricoCaja);
                        }

                        if (result) {
                            System.out.println("Apertura de caja - " + objHistoricoCaja.getFechaInicio() + " : " + objHistoricoCaja.getMontoFin());
                            result = InsertFacade.getInstance().insertMovimiento(objMovimiento);
                            if (result) {
                                System.out.println("Movimiento registrado correctamente");
                                objCaja.setMonto(montoSaldoCaja);
                                result = UpdateFacade.getInstance().update(objCaja);
                            }else{
                                System.out.println("Error en registro de Movimiento");
                            }
                        }else{
                            System.out.println("No registró histórico de caja");
                        }
                    }
                }
            } catch (Exception e) {
                result = false;
                e.printStackTrace();
            }

            if (result) {
                System.out.println("Caja actualizada correctamente");
                ConnectionManager.commitTransaction();
            } else {
                System.out.println("Caja no se actualizó");
                ConnectionManager.rollbackTransaction();
            }
            
        } else {
            System.out.println("No hay saldo suficiente en caja - " + objPago.getObjConcepto().getNombre() + " : " + objPago.getMonto().toPlainString());
        }

        return result;
    }

    private BigDecimal getMontoByConcepto(BigDecimal monto, Concepto objConcepto) {
        BigDecimal montoObtenido = BigDecimal.ZERO;

        switch (objConcepto.getId()) {
            case Concepto.CONCEPTO_APORTES:
                montoObtenido = montoObtenido.add(monto);
                break;
            case Concepto.CONCEPTO_DEPOSITOS:
                montoObtenido = montoObtenido.add(monto);
                break;
            case Concepto.CONCEPTO_TRANSFERENCIAS:
                montoObtenido = montoObtenido.add(monto);
                break;
            case Concepto.CONCEPTO_RETIROS_CUENTA:
                montoObtenido = montoObtenido.subtract(monto);
                break;
            case Concepto.CONCEPTO_CUOTAS:
                montoObtenido = montoObtenido.add(monto);
                break;
            case Concepto.CONCEPTO_RETIROS_SOCIO:
                montoObtenido = montoObtenido.subtract(monto);
                break;
            case Concepto.CONCEPTO_INGRESO_CAJA_ADMINISTRATIVO:
                montoObtenido = montoObtenido.add(monto);
                break;
            case Concepto.CONCEPTO_EGRESO_CAJA_ADMINISTRATIVO:
                montoObtenido = montoObtenido.subtract(monto);
                break;
        }
        return montoObtenido;
    }

    public boolean abrirCaja(Caja objCaja, Empleado objEmpleado) {
        boolean result = false;
        HistoricoCaja objHistoricoCaja;

        ConnectionManager.beginTransaction();
        try {
            if (objCaja != null) {
                objHistoricoCaja = QueryFacade.getInstance().getHistoricoCaja(objCaja);
                // Nota: verificar empleado si es necesario
                Caja objCajaVerificar = DAOCaja.getInstance().getCaja(objCaja.getId());
                if (!objCajaVerificar.isAbierto()) {
                    objCaja.setAbierto(true); // mover abajo 2. Si es necesario. 
                    result = UpdateFacade.getInstance().update(objCaja); // mover abajo 2. Si es necesario.
                    if (result) {  // ubicar despues de if...else if(objHistoricoCaja != null). Si es necesario.
                        if (objHistoricoCaja != null) {
                            if (objHistoricoCaja.getFechaFin() == null || objHistoricoCaja.getMontoFin() == null || objHistoricoCaja.getMontoFin().compareTo(BigDecimal.ZERO) == 0) {
                                result = false;
                                objCaja.setAbierto(false);
                                System.out.println("La caja ha sido bloqueda. El histórico de la caja '" + objCaja.getCodigo() + "' no ha sido validada");
                            } else {
                                objHistoricoCaja = new HistoricoCaja(
                                        0,
                                        DateUtil.currentTimestamp(),
                                        null,
                                        objHistoricoCaja.getMontoFin(),
                                        null,
                                        objCaja,
                                        objEmpleado);
                                result = InsertFacade.getInstance().insertHistoricoCaja(objHistoricoCaja);
                            }
                        } else {
                            objHistoricoCaja = new HistoricoCaja(
                                    0,
                                    DateUtil.currentTimestamp(),
                                    null,
                                    objHistoricoCaja == null ? objCaja.getMonto() : objHistoricoCaja.getMontoFin(),
                                    null,
                                    objCaja,
                                    objEmpleado);
                            result = InsertFacade.getInstance().insertHistoricoCaja(objHistoricoCaja);
                        }
                    }
                } else if (objHistoricoCaja != null) {
                    if (DateUtil.currentTimestamp().after(DateUtil.lastTime(objHistoricoCaja.getFechaInicio()))) {
                        System.out.println("Caja aperturada recientemente. Cierre caja primero.");
                    } else {
                        System.out.println("Caja aperturada recientemente. Cierre caja primero.");
                        // Evaluar histórico de caja
                    }
                } else {
                    System.out.println("Caja aperturada recientemente. Solicite información.");
                }
            } else {
                System.out.println("Error: Caja no existe.");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            System.out.println("Caja aperturada correctamente");
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }

    public boolean cerrarCaja(Caja objCaja, Empleado objEmpleado) {
        boolean result = false;
        boolean abierto = true;
        ConnectionManager.beginTransaction();
        try {
            if (objCaja != null) {
                Caja objCajaVerificada = DAOCaja.getInstance().getCaja(objCaja.getId());
                abierto = objCajaVerificada.isAbierto();
                if (abierto) {
                    HistoricoCaja objHistoricoCaja = QueryFacade.getInstance().getHistoricoCaja(objCaja);
                    if (objHistoricoCaja != null) {

                        if (DateUtil.currentTimestamp().before(DateUtil.lastTime(objHistoricoCaja.getFechaInicio()))) {
                            objHistoricoCaja.setFechaFin(DateUtil.currentTimestamp());
                            if (objHistoricoCaja.getMontoFin() == null) {
                                objHistoricoCaja.setMontoFin(objCaja.getMonto());
                            }
                        } else {
                            objHistoricoCaja.setFechaFin(DateUtil.lastTime(objHistoricoCaja.getFechaInicio()));
                            // El monto se establece al momento de realizar movimientos
                        }

                        result = UpdateFacade.getInstance().updateHistoricoCaja(objHistoricoCaja);
                        if (result) {
                            System.out.println("Cierre de caja - " + objHistoricoCaja.getFechaFin() + " : " + objHistoricoCaja.getMontoFin());
                            objCaja.setAbierto(false);
                            result = UpdateFacade.getInstance().update(objCaja);
                        }

                    } else {
                        System.out.println("Error: no existe histórico de caja. Aperture caja.");
                    }
                } else {
                    System.out.println("Advertencia: caja no aperturada. Aperture caja.");
//                    objCaja.setAbierto(abierto);
                    result = true;
                }
            } else {
                System.out.println("Error: Caja no existe.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result && abierto) {
            System.out.println("Caja actualizada correctamente");
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }
    
    public boolean registrarMovimientoAdministrativo(MovimientoAdministrativo objMovimientoAdministrativo){
        boolean result = false;
        ConnectionManager.beginTransaction();
        
        try{
            result = InsertFacade.getInstance().insertMovimientoAdministrativo(objMovimientoAdministrativo);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }
        
        return result;
    }
    
    public boolean eliminarMovimientoAdministrativo(MovimientoAdministrativo objMovimientoAdministrativo) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            result = DeleteFacade.getInstance().deleteMovimientoAdministrativo(objMovimientoAdministrativo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }
}
