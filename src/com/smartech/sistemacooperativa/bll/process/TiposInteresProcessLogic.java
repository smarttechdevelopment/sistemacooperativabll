package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.DeleteFacade;
import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.TasaInteres;
import com.smartech.sistemacooperativa.dominio.TipoInteres;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class TiposInteresProcessLogic {

    private static TiposInteresProcessLogic instance = null;

    private TiposInteresProcessLogic() {
    }

    public static TiposInteresProcessLogic getInstance() {
        if (instance == null) {
            instance = new TiposInteresProcessLogic();
        }

        return instance;
    }

    public String registrarTipoInteres(TipoInteres objTipoInteres) {
        String message = "";
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            result = InsertFacade.getInstance().insertTipoInteres(objTipoInteres);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            message = "Tipo de Interes registrado correctamente.";
            ConnectionManager.commitTransaction();
        } else {
            message = "Error en el registro.";
            ConnectionManager.rollbackTransaction();
        }

        return message;
    }

    public String actualizarTipoInteres(TipoInteres objTipoInteres, List<TasaInteres> lstTasasInteresBackup) {
        String message = "";
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            for (TasaInteres objTasaInteres : objTipoInteres.getLstTasasInteres()) {
                objTasaInteres.setFechaModificacion(DateUtil.currentTimestamp());
            }
            result = UpdateFacade.getInstance().updateTipoInteres(objTipoInteres);
            if (result) {
                this.updateLstTasasInteres(objTipoInteres, lstTasasInteresBackup);
                List<TasaInteres> lstTasasInteresToUpdate = this.getToUpdateTasasInteres(objTipoInteres.getLstTasasInteres(), lstTasasInteresBackup);
                List<TasaInteres> lstTasasInteresToInsert = this.getToInsertTasasInteres(objTipoInteres.getLstTasasInteres());
                List<TasaInteres> lstTasasInteresToDelete = this.getToDeleteTasasInteres(objTipoInteres.getLstTasasInteres());

                result = UpdateFacade.getInstance().updateTasasInteres(lstTasasInteresToUpdate);
                if (result) {
                    result = InsertFacade.getInstance().insertTasasInteres(lstTasasInteresToInsert);
                    if (result) {
                        result = DeleteFacade.getInstance().deleteTasasInteres(lstTasasInteresToDelete);
                        if (!result) {
                            message = "Error en la eliminacion de las Tasas de Interes.";
                        }
                    } else {
                        message = "Error en el registro de nuevas Tasas de Interes.";
                    }
                } else {
                    message = "Error en la actualizacion de las Tasas de Interes.";
                }
            } else {
                message = "Error en la actualizacion del Tipo de Interes.";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            message = "Tipo de Interes actualizado correctamente";
            ConnectionManager.commitTransaction();
        } else {
            if (message.isEmpty()) {
                message = "Error en la actualizacion.";
            }
            ConnectionManager.rollbackTransaction();
        }

        return message;
    }

    public String eliminarTipoInteres(TipoInteres objTipoInteres) {
        String message = "";
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            result = DeleteFacade.getInstance().deleteTipoInteres(objTipoInteres);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
            message = "Tipo de Interes eliminado correctamente.";
        } else {
            ConnectionManager.rollbackTransaction();
            message = "Error en la eliminacion";
        }

        return message;
    }

    private List<TasaInteres> getToInsertTasasInteres(List<TasaInteres> lstTasasInteres) {
        List<TasaInteres> lstTasasInteresToInsert = new ArrayList<>();

        for (TasaInteres objTasaInteres : lstTasasInteres) {
            if (objTasaInteres.isEstado() && objTasaInteres.getId() == 0) {
                lstTasasInteresToInsert.add(objTasaInteres);
            }
        }

        return lstTasasInteresToInsert;
    }

    private List<TasaInteres> getToUpdateTasasInteres(List<TasaInteres> lstTasasInteres, List<TasaInteres> lstTasasInteresBackup) {
        List<TasaInteres> lstTasasInteresToUpdate = new ArrayList<>();

        for (TasaInteres objTasaInteres : lstTasasInteres) {
            if (objTasaInteres.isEstado() && objTasaInteres.getId() != 0) {
                for (TasaInteres objTasaInteresBackup : lstTasasInteresBackup) {
                    if (objTasaInteres.getId() == objTasaInteresBackup.getId()) {
                        if (!objTasaInteres.compareTo(objTasaInteresBackup)) {
                            lstTasasInteresToUpdate.add(objTasaInteres);
                        }
                        break;
                    }
                }
            }
        }

        return lstTasasInteresToUpdate;
    }

    private List<TasaInteres> getToDeleteTasasInteres(List<TasaInteres> lstTasasInteres) {
        List<TasaInteres> lstTasasInteresToDelete = new ArrayList<>();

        for (TasaInteres objTasaInteres : lstTasasInteres) {
            if (!objTasaInteres.isEstado()) {
                lstTasasInteresToDelete.add(objTasaInteres);
            }
        }

        return lstTasasInteresToDelete;
    }

    private void updateLstTasasInteres(TipoInteres objTipoInteres, List<TasaInteres> lstTasasInteresBackup) {
        for (TasaInteres objTasaInteres : objTipoInteres.getLstTasasInteres()) {
            TasaInteres objTasaInteresEncontrado = this.existsTasaInteres(objTasaInteres, lstTasasInteresBackup);
            if (objTasaInteresEncontrado != null) {
                objTasaInteres = objTasaInteresEncontrado;
            }
        }
    }

    public TasaInteres existsTasaInteres(TasaInteres objTasaInteres, List<TasaInteres> lstTasasInteres) {
        for (TasaInteres objTasaInteresBuscar : lstTasasInteres) {
            if (objTasaInteres.getId() == objTasaInteresBuscar.getId() || objTasaInteres.getNombre().trim().equalsIgnoreCase(objTasaInteresBuscar.getNombre().trim())) {
                return objTasaInteresBuscar;
            }
        }

        return null;
    }
}
