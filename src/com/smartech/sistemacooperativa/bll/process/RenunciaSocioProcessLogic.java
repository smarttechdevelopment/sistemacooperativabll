/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.QueryFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAOAporte;
import com.smartech.sistemacooperativa.dal.DAOCuenta;
import com.smartech.sistemacooperativa.dal.DAORenuncia;
import com.smartech.sistemacooperativa.dal.DAOSocio;
import com.smartech.sistemacooperativa.dal.util.SequenceUtil;
import com.smartech.sistemacooperativa.dominio.Aporte;
import com.smartech.sistemacooperativa.dominio.Caja;
import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Concepto;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.DetalleDocumentoSolicitud;
import com.smartech.sistemacooperativa.dominio.DetalleSolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.Documento;
import com.smartech.sistemacooperativa.dominio.Empleado;
import com.smartech.sistemacooperativa.dominio.HistoricoCuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.Renuncia;
import com.smartech.sistemacooperativa.dominio.Socio;
import com.smartech.sistemacooperativa.dominio.SolicitudRetiroSocio;
import com.smartech.sistemacooperativa.dominio.TipoPago;
import com.smartech.sistemacooperativa.dominio.Usuario;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.FileUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Smartech
 */
public class RenunciaSocioProcessLogic {

    private static RenunciaSocioProcessLogic instance = null;

    public static RenunciaSocioProcessLogic getInstance() {
        if (instance == null) {
            instance = new RenunciaSocioProcessLogic();
        }
        return instance;
    }

    public List<DetalleSolicitudRetiroSocio> generarDetalleSociosSolicitudRetiro(Socio objSocioPrincipal, Usuario objUsuario) {
        List<DetalleSolicitudRetiroSocio> lstDetalleSolicitudRetiroSocio = new ArrayList<>();

        lstDetalleSolicitudRetiroSocio.add(new DetalleSolicitudRetiroSocio(0, objSocioPrincipal, objUsuario, false));

        List<Socio> lstSociosRelacionados;
        for (Cuenta objCuenta : objSocioPrincipal.getLstCuentas()) {
            if (objCuenta.getObjTipoCuenta().isMancomunada()) {
                lstSociosRelacionados = QueryFacade.getInstance().getAllSocios(objCuenta, false);
                for (Socio objSocio : lstSociosRelacionados) {
                    if (objSocio.getId() != objSocioPrincipal.getId()) {
                        lstDetalleSolicitudRetiroSocio.add(new DetalleSolicitudRetiroSocio(0, objSocio, objUsuario, false));
                    }
                }
            }
        }

        return lstDetalleSolicitudRetiroSocio;
    }

    public String getEstadoActividadSocio(Socio objSocio) {
        boolean estado = false;
        String estadoActividad;
        // [falta configuracion]
//        Aporte objAporte = QueryFacade.getInstance().getLastAporte(objSocio);
//        if (objAporte != null) {
//            if (objAporte.isEstado() && objAporte.isHabilitado() && objAporte.isPagado()) {
//                if (DateUtil.getMonthsDifference(objAporte.getFechaRegistro(), DateUtil.currentTimestamp()) < Socio.MESES_LIMITE_INACTIVIDAD) {
        BigDecimal deuda = PrestamosProcessLogic.getInstance().calcularDeudaAportes(objSocio);
        estado = deuda.compareTo(BigDecimal.ZERO) == 0;
//                }
//            }
//        }

        if (estado) {
            estadoActividad = Socio.ESTADO_ACTIVIDAD_ACTIVO;
        } else {
            estadoActividad = Socio.ESTADO_ACTIVIDAD_PASIVO;
        }
        return estadoActividad;
    }

    public boolean aprobaronSocios(SolicitudRetiroSocio objSolicitudRetiroSocio) {
        boolean result = false;
        List<DetalleSolicitudRetiroSocio> lstDetalleSolicitudRetiroSocios = objSolicitudRetiroSocio.getLstDetalleSolicitudRetiroSocios();
        for (DetalleSolicitudRetiroSocio objDetalleSolicitudRetiroSocio : lstDetalleSolicitudRetiroSocios) {
            result = objDetalleSolicitudRetiroSocio.isAprueba();
            if (objDetalleSolicitudRetiroSocio.getObjSocio().isEstado()) {
                if (!result) {
                    break;
                }
            }
        }
        return result;
    }

    public boolean verificarAprobacionDocumentos(SolicitudRetiroSocio objSolicitudRetiroSocio) {
        boolean result = false;
        List<DetalleDocumentoSolicitud> lstDetalleDocumentoSolicitudes = objSolicitudRetiroSocio.getLstDetalleDocumentoSolicitud();
        for (DetalleDocumentoSolicitud objDetalleDocumentoSolicitud : lstDetalleDocumentoSolicitudes) {
            result = objDetalleDocumentoSolicitud.isAprobado();
            if (!result) {
                break;
            }
        }
        return result;
    }

    public boolean tienePrestamosPendientes(Socio objSocio) {
        // Préstamos que no han sido pagados
        return !QueryFacade.getInstance().getAllPrestamos(objSocio, false).isEmpty();
    }

    public boolean tienecuentasActivas(Socio objSocio) {
        for (Cuenta objCuenta : objSocio.getLstCuentas()) {
            if (objCuenta.isActivo() || objCuenta.isEstado()) {
                return true;
            }
        }
        return false;
    }

    public boolean puedeRealizarSolicitudRetiro(Socio objSocio) {
        boolean result;
        for (Cuenta objCuenta : objSocio.getLstCuentas()) {
            if (objCuenta.isActivo() || objCuenta.isEstado()) {
                if (objCuenta.getObjTipoCuenta().isMancomunada()) {
                    return false;
                }
            }
        }

        result = !RenunciaSocioProcessLogic.getInstance().tienePrestamosPendientes(objSocio);

        return result;
    }

    public boolean guardarSolicitudRetiroSocio(SolicitudRetiroSocio objSolicitudRetiroSocio, Map<Documento, File> mapArchivos, Caja objCaja, Empleado objEmpleado) {
        boolean result = false;
        ConnectionManager.beginTransaction();
        FileUtil.connect();
        List<HistoricoCuenta> lstHistoricoCuentas = new ArrayList<>();
        String disponibilidad = PrestamosProcessLogic.getInstance().verificarDisponibilidadGarante(objSolicitudRetiroSocio.getObjSocio());
        if (disponibilidad.contains("Disponible")) {

            try {
                String destinationPath = "/SolicitudesRenuncia/" + objSolicitudRetiroSocio.getObjSocio().getDocumentoIdentidad() + "/" + objSolicitudRetiroSocio.getCodigo();
                objSolicitudRetiroSocio.updateRutas(mapArchivos, destinationPath);
                if (objSolicitudRetiroSocio.getId() == 0) {
                    if (verificarAprobacionDocumentos(objSolicitudRetiroSocio)) {
                        objSolicitudRetiroSocio.setAprobado(true);
                    }
                    result = InsertFacade.getInstance().insertSolicitudRetiroSocio(objSolicitudRetiroSocio);
                    if (result) {
                        result = FileUtil.createDirectoryFTP(destinationPath);
                        if (result) {
                            List<File> lstArchivosDocumentos = new ArrayList<>(mapArchivos.values());
                            result = FileUtil.saveFilesFTP(lstArchivosDocumentos, destinationPath);
                        }
                    }
                } else {
//                if (aprobaronSocios(objSolicitudRetiroSocio)) {
                    if (verificarAprobacionDocumentos(objSolicitudRetiroSocio)) {
                        objSolicitudRetiroSocio.setAprobado(true);
                    }
//                }
                    objSolicitudRetiroSocio.setFechaModificacion(DateUtil.currentTimestamp());
                    result = UpdateFacade.getInstance().updateSolicituRetiroSocio(objSolicitudRetiroSocio);
                    if (result) {
                        result = FileUtil.createDirectoryFTP(destinationPath);
                        if (result) {
                            List<File> lstArchivosDocumentos = new ArrayList<>(mapArchivos.values());
                            result = FileUtil.saveFilesFTP(lstArchivosDocumentos, destinationPath);
                        }
                    }
                }

                if (result) {

                    if (objSolicitudRetiroSocio.isAprobado()) {
                        BigDecimal montoPago;

                        BigDecimal saldoAportes, saldoTotal, montoCuentas = BigDecimal.ZERO;

                        BigDecimal deudaTotal;
                        BigDecimal deudaAportes = PrestamosProcessLogic.getInstance().calcularDeudaAportes(objSolicitudRetiroSocio.getObjSocio());
                        BigDecimal deudaPrestamos = PrestamosProcessLogic.getInstance().calcularDeudaSocio(objSolicitudRetiroSocio.getObjSocio());

                        deudaTotal = deudaAportes.add(deudaPrestamos);

                        if (objSolicitudRetiroSocio.getObjSocio().getLstCuentas().isEmpty()) {
                            List<Cuenta> lstCuentas = DAOCuenta.getInstance().getAllCuentas(objSolicitudRetiroSocio.getObjSocio());
                            for (Cuenta objCuenta : lstCuentas) { // sólo las cuetas individuales
                                if (!objCuenta.getObjTipoCuenta().isMancomunada()) {
                                    montoCuentas = montoCuentas.add(objCuenta.getSaldo());
                                } else if (objCuenta.isEstado() || objCuenta.isActivo()) {
                                    result = false;
                                    break;
                                }

                            }
                        }

                        if (result) {
                            saldoAportes = DAOAporte.getInstance().getSaldoAportes(objSolicitudRetiroSocio.getObjSocio());
                            saldoTotal = montoCuentas.add(saldoAportes);
                            // Verificacion de ahorro de socio y su deuda
                            if (deudaTotal.compareTo(saldoTotal) <= 0) {
                                montoPago = saldoAportes.subtract(deudaTotal);
                                Renuncia objRenuncia = new Renuncia(0, montoPago, DateUtil.currentTimestamp(), null, true, true, false, null, objSolicitudRetiroSocio.getObjUsuario(), objSolicitudRetiroSocio);
                                result = DAORenuncia.getInstance().insert(objRenuncia);
                                if (result) {
                                    Aporte objAporte = new Aporte(
                                            0,
                                            montoPago,
                                            DateUtil.currentTimestamp(),
                                            null,
                                            true,
                                            objSolicitudRetiroSocio.getObjUsuario(),
                                            true,
                                            false,
                                            false,
                                            true,
                                            objSolicitudRetiroSocio.getObjSocio().getObjTarjeta(),
                                            null);
                                    result = DAOAporte.getInstance().insert(objAporte);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }
        FileUtil.disconnect();

        return result;
    }

    public Comprobante registrarPagoSaldoGeneral(SolicitudRetiroSocio objSolicitudRetiroSocio, BigDecimal monto, Caja objCaja, Empleado objEmpleado) {

        boolean result = false;
        List<Pago> lstPagos = new ArrayList<>();
        Comprobante objComprobante = null;
        Pago objPago = null;
        if (objCaja != null && objEmpleado != null) {
            TipoPago objTipoPago = QueryFacade.getInstance().getTipoPago(1);
            Concepto objConcepto = QueryFacade.getInstance().getConcepto("Retiros de renuncia");
            objPago = new Pago(
                    0,
                    DateUtil.currentTimestamp(),
                    monto,
                    DateUtil.currentTimestamp(),
                    null,
                    true,
                    objTipoPago,
                    objConcepto,
                    QueryFacade.getInstance().getMonedaByAbreviatura("PEN"));
            lstPagos.add(objPago);
            result = InsertFacade.getInstance().insertPago(objPago);

            if (result) {
                result = CajaProcessLogic.getInstance().registrarMovimiento(objPago, objCaja, objEmpleado);
            }

        }

        if (result) {
            if (objCaja.getObjEstablecimiento() != null) {
                objComprobante = new Comprobante(
                        0,
                        StringUtil.getStringCode(ComprobantesProcessLogic.getInstance().getNextCodigo(false), 10),
                        "Renuncia de Socio",
                        monto,
                        false,
                        DateUtil.currentTimestamp(),
                        false,
                        objCaja.getObjEstablecimiento(),
                        lstPagos);
                result = InsertFacade.getInstance().insertComprobante(objComprobante);
                if (!result) {
                    objComprobante = null;
                }
            }
        }

        return objComprobante;
    }

    public boolean inhabilitarSocios(List<Socio> lstSociosPasivos) {
        boolean result = false;
        ConnectionManager.beginTransaction();
        try {
            for (Socio objSocioPasivo : lstSociosPasivos) {
                String estadoActividad = this.getEstadoActividadSocio(objSocioPasivo);
                if (estadoActividad.equals(Socio.ESTADO_ACTIVIDAD_PASIVO)) {
                    objSocioPasivo.setActivo(false);
                    result = DAOSocio.getInstance().update(objSocioPasivo);
                    if (!result) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result) {
            ConnectionManager.commitTransaction();
        } else {
            ConnectionManager.rollbackTransaction();
        }
        return result;
    }
}
