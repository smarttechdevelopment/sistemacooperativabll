package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.DeleteFacade;
import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.bll.facade.UpdateFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dominio.Empleado;

/**
 *
 * @author Smartech
 */
public class EmpleadosProcessLogic {
    
    private static EmpleadosProcessLogic instance = null;
    
    private EmpleadosProcessLogic(){
    }
    
    public static EmpleadosProcessLogic getInstance(){
        if(instance == null){
            instance = new EmpleadosProcessLogic();
        }
        
        return instance;
    }
    
    public String registrarEmpleado(Empleado objEmpleado){
        String message = "";
        boolean result = false;
        
        ConnectionManager.beginTransaction();
        try{
            result = InsertFacade.getInstance().insertEmpleado(objEmpleado);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if (result) {
            message = "Empleado registrado correctamente";
            ConnectionManager.commitTransaction();
        } else {
            message = "Error en el registro";
            ConnectionManager.rollbackTransaction();
        }
        
        return message;
    }
    
    public String actualizarEmpleado(Empleado objEmpleado){
        String message = "";
        boolean result = false;
        
        ConnectionManager.beginTransaction();
        try{
            result = UpdateFacade.getInstance().updateEmpleado(objEmpleado);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if (result) {
            message = "Empleado actualizado correctamente";
            ConnectionManager.commitTransaction();
        } else {
            message = "Error en la actualizacion";
            ConnectionManager.rollbackTransaction();
        }
        
        return message;
    }
    
    public String eliminarEmpleado(Empleado objEmpleado){
        String message = "";
        boolean result = false;
        
        ConnectionManager.beginTransaction();
        try{
            result = DeleteFacade.getInstance().deleteEmpleado(objEmpleado);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if (result) {
            message = "Empleado eliminado correctamente";
            ConnectionManager.commitTransaction();
        } else {
            message = "Error en la eliminacion";
            ConnectionManager.rollbackTransaction();
        }
        
        return message;
    }
}
