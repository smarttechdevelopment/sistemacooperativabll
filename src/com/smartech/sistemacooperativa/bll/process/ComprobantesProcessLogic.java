package com.smartech.sistemacooperativa.bll.process;

import com.smartech.sistemacooperativa.bll.facade.InsertFacade;
import com.smartech.sistemacooperativa.connection.ConnectionManager;
import com.smartech.sistemacooperativa.dal.DAOComprobante;
import com.smartech.sistemacooperativa.dal.util.SequenceUtil;
import com.smartech.sistemacooperativa.dominio.Comprobante;
import com.smartech.sistemacooperativa.dominio.Cuenta;
import com.smartech.sistemacooperativa.dominio.Establecimiento;
import com.smartech.sistemacooperativa.dominio.HistoricoCuenta;
import com.smartech.sistemacooperativa.dominio.Pago;
import com.smartech.sistemacooperativa.dominio.ITransaccion;
import com.smartech.sistemacooperativa.util.generics.DateUtil;
import com.smartech.sistemacooperativa.util.generics.StringUtil;
import java.util.List;

/**
 *
 * @author Smartech
 */
public class ComprobantesProcessLogic {

    private static ComprobantesProcessLogic instance = null;

    private ComprobantesProcessLogic() {
    }

    public static ComprobantesProcessLogic getInstance() {
        if (instance == null) {
            instance = new ComprobantesProcessLogic();
        }

        return instance;
    }
    
    public long getNextCodigo(boolean ingreso){
        return DAOComprobante.getInstance().getUltimoCodigo(true);
    }

    public boolean generarComprobante(ITransaccion objTransaccion, List<Pago> lstPagos, Establecimiento objEstablecimiento, List<Cuenta> lstCuentas) {
        boolean result = false;

        ConnectionManager.beginTransaction();
        try {
            boolean ingreso = objTransaccion.isIngreso();
            Comprobante objComprobante = new Comprobante(
                    0,
                    StringUtil.getStringCode(this.getNextCodigo(ingreso), 10),
                    objTransaccion.getDescripcionTransaccion(),
                    Pago.getMontoTotal(lstPagos),
                    ingreso,
                    DateUtil.currentTimestamp(),
                    false,
                    objEstablecimiento,
                    lstPagos);
            result = InsertFacade.getInstance().insertComprobante(objComprobante);

            if (result && lstCuentas.size() > 0) {
                for (Cuenta objCuenta : lstCuentas) {
                    HistoricoCuenta objHistoricoCuenta = new HistoricoCuenta(
                            0,
                            objCuenta.getSaldo(),
                            DateUtil.currentTimestamp(),
                            objCuenta,
                            objComprobante);
                    result = InsertFacade.getInstance().insertHistoricoCuenta(objHistoricoCuenta);
                    if (!result) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if(result){
            ConnectionManager.commitTransaction();
        }else{
            ConnectionManager.rollbackTransaction();
        }

        return result;
    }
}
